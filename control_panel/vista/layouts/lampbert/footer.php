 <!-- ***** Footer Area Start ***** -->
    <footer class="footer-social-icon text-center section_padding_70 clearfix">
        <!-- footer logo -->
        <div class="footer-text">
            <h2>La.</h2>
        </div>
        <!-- social icon-->
        <div class="footer-social-icon">
            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="#"><i class="active fa fa-twitter" aria-hidden="true"></i></a>
            <a href="#"> <i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
        </div>
        <div class="footer-menu">
            <nav>
                <ul>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Terms &amp; Conditions</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </nav>
        </div>
        <!-- Foooter Text-->
        <div class="copyright-text">
            <!-- ***** Removing this text is now allowed! This template is licensed under CC BY 3.0 ***** -->
            <p>Copyright ©2017 Ca.  <a href="# target="_blank">www</a></p>
        </div>
    </footer>

    <!-- ***** Footer Area Start ***** -->

    <!-- Jquery-2.2.4 JS -->
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap-4 Beta JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins JS -->
    <script src="js/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="js/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="js/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="js/active.js"></script>
    <!-- Active JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    
    <?php if ($_SESSION['session_usuario']['id_perfil']==2) { ?>
        <script>
            /*$(function(){
                notificacion();
                setInterval(function(){
                    notificacion();
                }, 10000)

            })*/
            function notificacion(){
                $.ajax({
                    type: "POST", //The type of HTTP verb (post, get, etc)
                    url: "control_panel/controlador/tbl_cursos.php", //The URL from the form element where the AJAX request will be sent
                    data: {"accion": "notificacion"}, //All the data from the form serialized
                    dataType: "json", //The type of response to expect from the server
                    success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
                        if (data.contador>0){
                           $('#notificacion').html(data.contador).show();
                        }
                    }
                });
            }
        </script>
    <?php } ?>



    <!-- Active JS -->
    <script src="js/usuario.js"></script>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cursos disponibles</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
                <div class="panel panel-default">
                
                <ul class="list-group" id="list_cursos">
                    
                    
                </ul>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>