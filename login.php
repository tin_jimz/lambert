<?php session_start();
include("control_panel/inc/config.sistema.php");
require_once("control_panel/modelo/config.modelo.php"); # configuracion del modelo      
require_once("control_panel/modelo/class_tbl_estatus_usuarios.php"); # clase del modelo
$Obj_tbl_estatus_usuarios = new tbl_estatus_usuarios;   
require_once("control_panel/modelo/class_tbl_perfiles.php"); # clase del modelo
$Obj_tbl_perfiles = new tbl_perfiles;   
require_once("control_panel/modelo/class_tbl_regiones.php"); # clase del modelo
$Obj_tbl_regiones = new tbl_regiones;
$_SESSION["where"]="";  
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
   <?php include("control_panel/vista/layouts/lampbert/header.php");?>
    <?php include("control_panel/vista/layouts/lampbert/header_js.php");?>

</head>

<body>
    <!-- ***** Header Area  ***** -->
    <?php include("control_panel/vista/layouts/lampbert/header_menu.php");?>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
    <section class="wellcome_area clearfix" id="home">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="card" style="width: 30%; margin-left: 35%; top: -10%;">
                  <div class="card-block">
                    <h4 class="card-title">INICIAR SESIÓN</h4>
                    <hr>
                    <form id="form_inicio">
                      <div class="form-group">
                        <label for="formGroupExampleInput">Usuario o Email</label>
                        <input type="text" name="usuario" required class="form-control" id="formGroupExampleInput" placeholder="Usuario o Email" autofocus>
                      </div>
                      <div class="form-group">
                        <label for="formGroupExampleInput2">Contraseña</label>
                        <input type="password" name="clave" required class="form-control" id="formGroupExampleInput2" placeholder="Contraseña">
                      </div>
                      <hr>
                    <button type="submit" class="btn btn-primary btn-md btn-block" style="border-radius: 0px;">Entrar</button>
                    <a href="register.php" class="btn btn-secondary btn-md btn-block" style="border-radius: 0px;">Registro</a>
                    <input type="hidden" name="accion" value="autenticar">
                    </form>
                    <div id="respuesta"></div>
                  </div>
                </div>
            </div>
        </div>
        
    </section>
    <!-- ***** Wellcome Area End ***** -->

    
    <!-- ***** Footer Area Start ***** -->
    <?php include("control_panel/vista/layouts/lampbert/footer.php");?>
    <script>
        $(function(){login();});
    </script>

</html>
