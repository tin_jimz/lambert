$(function(){
    $('#inicio_link').click(function(){ location='dashboard.php' })
    $('#menu_bar').css('background-color', '#6654e7')
    var dd = new DropDown( $('#dd') );
    $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
    $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
    $(this).addClass('selected');
    $(this).find('input[type="radio"]').prop("checked", true); 
    });

    $('#product1').on( "click",function(){
        //alert($('#product1').attr('alt'))
        $('#btn1').attr('disabled', 'disabled')
        $('#btn2').removeAttr('disabled')
        $('#grado').html('<option value="">Seleccione...</option>'+
                        '<option value="1">1</option>'+
                        '<option value="2">2</option>'+
                        '<option value="3">3</option>'+
                        '<option value="4">4</option>'+
                        '<option value="5">5</option>'+
                        '<option value="6">6</option>'+
                        '<option value="7">7</option>');
        $('#grado').change(function(){
            listar_materias($('#grado').val());

        });
        $('#boton_urg').hide()
        $('#notice').html('<strong><a href="#" title="Ver Cursos">Realice una petición &nbsp;</a></strong><span>para obtener resultados.</span>')
        $('#grado').val('').focus();
    })
    $('#product2').on( "click",function(){
        //alert($('#product2').attr('alt'))
        $('#btn2').attr('disabled', 'disabled')
        $('#btn1').removeAttr('disabled')

        $('#grado').html('<option value="">Seleccione...</option>'+
                        '<option value="8">1</option>'+
                        '<option value="9">2</option>'+
                        '<option value="10">3</option>'+
                        '<option value="11">4</option>'+
                        '<option value="12">5</option>'+
                        '<option value="13">6</option>');
        $('#grado').change(function(){
            listar_materias($('#grado').val());

        });

        $('#boton_urg').hide()
        $('#notice').html('<strong><a href="#" title="Ver Cursos">Realice una petición &nbsp;</a></strong><span>para obtener resultados.</span>')
        $('#grado').val('').focus();
    })

    $('#link1').click(function(){
        $('#link1').addClass('active');
        $('#link2').removeAttr('class');
        $('#link3').removeAttr('class');
        $('#link4').removeAttr('class');
    });
    $('#link2').click(function(){
        $('#link2').addClass('active');
        $('#link1').removeAttr('class');
        $('#link3').removeAttr('class');
        $('#link4').removeAttr('class');
    });
    $('#link3').click(function(){
        $('#link3').addClass('active');
        $('#link2').removeAttr('class');
        $('#link1').removeAttr('class');
        $('#link4').removeAttr('class');
    });
    $('#link4').click(function(){
        $('#link4').addClass('active');
        $('#link2').removeAttr('class');
        $('#link3').removeAttr('class');
        $('#link1').removeAttr('class');
    });
    
});

function Colapse_Switcher() {
    // Switch theme color
    var $styleSwitcher = $('#style_switcher');
    if (!$styleSwitcher.hasClass('switcher_open')) {
        $styleSwitcher.addClass('switcher_open')
        $('#icono').removeAttr('class').addClass('fa fa-chevron-right')
    } else {
        $styleSwitcher.removeClass('switcher_open')
        $('#icono').removeAttr('class').addClass('fa fa-comments')
    }
}
function DropDown(el) {
        this.dd = el;
        this.placeholder = this.dd.children('span');
        this.opts = this.dd.find('.dropdown a');
        this.val = '';
        this.index = -1;
        this.initEvents();
    }
    DropDown.prototype = {
        initEvents : function() {
            var obj = this;

            obj.dd.on('click', function(event){
                $(this).toggleClass('active');
                return false;
            });

            obj.opts.on('click',function(){
                var opt = $(this);
                obj.val = opt.text();
                obj.index = opt.index();
                obj.placeholder.text(obj.val);
            });
        },
        getValue : function() {
            return this.val;
        },
        getIndex : function() {
            return this.index;
        }
    }

function listar_materias(grado){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_cursos.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "listar_materias", "grado": grado}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data){
                $('#boton_urg').hide()
                contador=0;
                $('#notice').html('<div class="col-md-3"><select id="select_materia" class="form-control"><option value="">Elegí Matería...</option></select></div><div class="col-md-3" id="span_sel"></div><div class="col-md-6" id="area_text"></div>');
                $.each(data,function(index_data, registros){
                    contador++;
                    $('#select_materia').append(new Option(registros.materia, registros.id_materia))
                });
                if (contador==0) {
                    $('#notice').html('<strong><a href="#" title="Ver Cursos">No hay materias&nbsp;</a></strong><span>registradas para este grado.</span>')
                }
            }

            $('#select_materia').change(function(){
                selec_prof($(this).val(), grado);
            })
        }
    });
}

function selec_prof(id_materia, grado){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_cursos.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "selec_prof2", "grado": grado, "id_materia": id_materia}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data){
                contador=0;
                $('#boton_urg').show()
                $('#span_sel').html('<select id="selec_pro" class="form-control"><option value="">Elegí Profesor...</option></select>');
                $('#area_text').html('<textarea id="descripcion" class="form-control" placeholder="Escriba una breve descripcion" maxlength="100"></textarea>');
                $.each(data,function(index_data, registros){
                    contador++;
                    $('#selec_pro').append(new Option(registros.nombres+' '+registros.apellidos, registros.id_curso))
                });
                if (contador==0) {
                    $('#span_sel').html('<strong><a href="#" title="Ver Cursos">No hay profesores</a></strong> <span>&nbsp;disponibles ahora.</span>')
                    $('#boton_urg').hide()
                }
            }
        }
    });
}

function completar(){
    var hora = $('#time').val()
    var fecha = $('#date').val()
    var id_curso = $('#selec_pro').val()
    var descripcion = $('#descripcion').val()
    var duracion = $('#duracion').val()
    if(hora!="" && fecha!="" && id_curso!="" && descripcion!="" && duracion!=""){
        $.ajax({
            type: "POST", //The type of HTTP verb (post, get, etc)
            url: "control_panel/controlador/tbl_clases.php", //The URL from the form element where the AJAX request will be sent
            data: {"accion": "reservar", "hora": hora, "fecha": fecha, "id_curso": id_curso, "descripcion": descripcion, "duracion": duracion}, //All the data from the form serialized
            dataType: "json", //The type of response to expect from the server
            success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
                if (data.estado=="insertado"){
                    $.confirm({
                            title: 'Exitoso!',
                            content: 'Su solicitud ha sido enviada, Por favor espere su respuesta!',
                            theme: 'supervan', // 'material', 'bootstrap'
                            buttons: {
                                somethingElse: {
                                    text: 'Aceptar',
                                    btnClass: 'btn-blue',
                                    action: function(){
                                        location ='dashboard.php';
                                    }
                                }
                            }
                        });
                }
            }
        });
    }else{
        $.confirm({
            title: 'Aviso!',
            content: 'Todos los campos deben estar llenos.',
            theme: 'light', // 'material', 'bootstrap'
            buttons: {
                somethingElse: {
                    text: 'Aceptar',
                    btnClass: 'btn-blue',
                    action: function(){
                        
                    }
                }
            }
        });
    }
}

function mis_reservas(){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_asistencias.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "mis_reservas"}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data){
                contador=0; contador2=0; contador3=0; contador4= 0;
                $('#mis_reservas').html('');
                $.each(data,function(index_data, registros){

                if(registros.status==1){   
                contador++;
                        $('#res1').append('<li>'+
                                            '<time datetime="'+registros.fecha_hora+'" style="background-color: #007bff;">'+
                                                '<span class="day" style="margin-top: 10px;">'+registros.fecha_sola.substr(8, 2)+'</span>'+
                                                '<span class="month">'+registros.mes+'</span>'+
                                            '</time>'+
                                            '<div class="info">'+
                                                '<h2 class="title">'+registros.materia+'</h2>'+
                                                '<p class="desc"><span><img width="32" style="border-radius: 50px;" src="'+registros.imagen_perfil+'"/></span><span> Prof.: '+registros.nombres+' '+registros.apellidos+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-clock-o fa-lg" ></i></span><span> De'+registros.hora_inicio+' a '+registros.hora_fin+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-info-circle fa-lg" ></i></span><span> '+registros.descripcion+'</span></p>'+
                                                '<ul>'+
                                                    '<li style="width:50%;" id="acceso_aula"></li>'+
                                                '</ul>'+
                                            '</div>'+
                                        '</li>');
                        setTimeout(function(){
                            if(registros.flag==1){
                                $('#acceso_aula').html('<a href="aula_virtual.php?id_clase='+registros.id_clase+'" style="color: #546BF9;">Ir al Aula <i class="fa fa-arrow-right"></i></a>')
                            }
                        }, 200)
                        
                    }else if(registros.status==2){ 
                        contador2++;
                        $('#res2').append('<li>'+
                                            '<time datetime="'+registros.fecha_hora+'" style="background-color: orange;">'+
                                                '<span class="day" style="margin-top: 10px;">'+registros.fecha_sola.substr(8, 2)+'</span>'+
                                                '<span class="month">'+registros.mes+'</span>'+
                                            '</time>'+
                                            '<div class="info">'+
                                                '<h2 class="title">'+registros.materia+'</h2>'+
                                                '<p class="desc"><span><img width="32" style="border-radius: 50px;" src="'+registros.imagen_perfil+'"/></span><span> Prof.: '+registros.nombres+' '+registros.apellidos+'</span></p>'+
                                               '<p class="desc"><span><i class="fa fa-clock-o fa-lg" ></i></span><span> De'+registros.hora_inicio+' a '+registros.hora_fin+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-info-circle fa-lg" ></i></span><span> '+registros.descripcion+'</span></p>'+
                                                
                                            '</div>'+
                                        '</li>');
                    }else if(registros.status==3){ 
                        contador4++;
                        $('#res4').append('<li>'+
                                            '<time datetime="'+registros.fecha_hora+'" style="background-color: red;">'+
                                                '<span class="day" style="margin-top: 10px;">'+registros.fecha_sola.substr(8, 2)+'</span>'+
                                                '<span class="month">'+registros.mes+'</span>'+
                                            '</time>'+
                                            '<div class="info">'+
                                                '<h2 class="title">'+registros.materia+'</h2>'+
                                                '<p class="desc"><span><img width="32" style="border-radius: 50px;" src="'+registros.imagen_perfil+'"/></span><span> Prof.: '+registros.nombres+' '+registros.apellidos+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-clock-o fa-lg" ></i></span><span> De'+registros.hora_inicio+' a '+registros.hora_fin+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-info-circle fa-lg" ></i></span><span> '+registros.descripcion+'</span></p>'+
                                                
                                            '</div>'+
                                        '</li>');
                    }else if(registros.status==4){ 
                        contador3++;
                        $('#res3').append('<li>'+
                                            '<time datetime="'+registros.fecha_hora+'" style="background-color: gray;">'+
                                                '<span class="day" style="margin-top: 10px;">'+registros.fecha_sola.substr(8, 2)+'</span>'+
                                                '<span class="month">'+registros.mes+'</span>'+
                                            '</time>'+
                                            '<div class="info">'+
                                                '<h2 class="title">'+registros.materia+'</h2>'+
                                                '<p class="desc"><span><img width="32" style="border-radius: 50px;" src="'+registros.imagen_perfil+'"/></span><span> Prof.: '+registros.nombres+' '+registros.apellidos+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-clock-o fa-lg" ></i></span><span> De'+registros.hora_inicio+' a '+registros.hora_fin+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-info-circle fa-lg" ></i></span><span> '+registros.descripcion+'</span></p>'+
                                                
                                            '</div>'+
                                        '</li>');
                    }
                

                
                });

                if (contador==0) {
                    $('#res1').html('<div class="col-md-12"><hr /><i class="fa fa-info-circle"></i> No existen reservas aceptadas.</div>')
                }
                if (contador2==0) {
                    $('#res2').html('<div class="col-md-12"><hr /><i class="fa fa-info-circle"></i> No existen reservas pendientes.</div>')
                }
                if (contador3==0) {
                    $('#res3').html('<div class="col-md-12"><hr /><i class="fa fa-info-circle"></i> No existen reservas caducadas.</div>')
                }
                if (contador4==0) {
                    $('#res4').html('<div class="col-md-12"><hr /><i class="fa fa-info-circle"></i> No existen reservas rechazadas.</div>')
                }
            }
        }
    });
}

//////////////////////////////

function mis_reservas_prof(){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_cursos.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "mis_reservas_prof"}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data){
                $('#res1').html("");
                $('#res2').html("");
                $('#res3').html("");
                $('#res4').html("");
                $.each(data,function(index_data, registros){
                    listar_reservas(registros.id_curso, registros.materia)
                });
            }
        }
    });
}

function listar_reservas(id_curso, materia,){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_clases.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "listar_reservas", "id_curso" : id_curso, "materia" : materia}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data){
                $.each(data,function(index_data, registros){
                    if(registros.status==1){ 
                        $('#res1').append('<li>'+
                                            '<time datetime="'+registros.fecha_hora+'" style="background-color: #007bff;">'+
                                                '<span class="day" style="margin-top: 10px;">'+registros.fecha_sola.substr(8, 2)+'</span>'+
                                                '<span class="month">'+registros.mes+'</span>'+
                                            '</time>'+
                                            '<div class="info">'+
                                                '<h2 class="title">'+materia+'</h2>'+
                                                '<p class="desc"><span><img width="32" style="border-radius: 50px;" src="'+registros.imagen_perfil+'"/></span><span> '+registros.nombres+' '+registros.apellidos+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-clock-o fa-lg" ></i></span><span> De'+registros.hora_inicio+' a '+registros.hora_fin+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-info-circle fa-lg" ></i></span><span> '+registros.descripcion+'</span></p>'+
                                                '<ul>'+
                                                    '<li style="width:50%;" id="acceso_aula"></li>'+
                                                '</ul>'+
                                            '</div>'+
                                        '</li>');
                        setTimeout(function(){
                            if(registros.flag==1){
                                $('#acceso_aula').html('<a href="aula_virtual.php?id_clase='+registros.id_clase+'" style="color: #546BF9;">Ir al Aula <i class="fa fa-arrow-right"></i></a>')
                            }
                        }, 200)
                        
                    }else if(registros.status==2){ 
                        $('#res2').append('<li>'+
                                            '<time datetime="'+registros.fecha_hora+'" style="background-color: orange;">'+
                                                '<span class="day" style="margin-top: 10px;">'+registros.fecha_sola.substr(8, 2)+'</span>'+
                                                '<span class="month">'+registros.mes+'</span>'+
                                            '</time>'+
                                            '<div class="info">'+
                                                '<h2 class="title">'+materia+'</h2>'+
                                                '<p class="desc"><span><img width="32" style="border-radius: 50px;" src="'+registros.imagen_perfil+'"/></span><span> '+registros.nombres+' '+registros.apellidos+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-clock-o fa-lg" ></i></span><span> De'+registros.hora_inicio+' a '+registros.hora_fin+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-info-circle fa-lg" ></i></span><span> '+registros.descripcion+'</span></p>'+
                                                
                                            '</div>'+
                                            '<div class="social">'+
                                                '<ul>'+
                                                    '<li class="facebook" style="width:33%;"><a href="javascript:void(0);" onclick="aceptar('+registros.id_clase+')" ><span class="fa fa-check fa-lg"></span></a></li>'+
                                                    '<li class="google-plus" style="width:34%;" ><a href="javascript:void(0);" onclick="rechazar('+registros.id_clase+')" ><span class="fa fa-ban fa-lg"></span></a></li>'+
                                                '</ul>'+
                                            '</div>'+
                                        '</li>');
                    }else if(registros.status==3){ 
                        $('#res4').append('<li>'+
                                            '<time datetime="'+registros.fecha_hora+'" style="background-color: red;">'+
                                                '<span class="day" style="margin-top: 10px;">'+registros.fecha_sola.substr(8, 2)+'</span>'+
                                                '<span class="month">'+registros.mes+'</span>'+
                                            '</time>'+
                                            '<div class="info">'+
                                                '<h2 class="title">'+materia+'</h2>'+
                                                '<p class="desc"><span><img width="32" style="border-radius: 50px;" src="'+registros.imagen_perfil+'"/></span><span> '+registros.nombres+' '+registros.apellidos+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-clock-o fa-lg" ></i></span><span> De'+registros.hora_inicio+' a '+registros.hora_fin+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-info-circle fa-lg" ></i></span><span> '+registros.descripcion+'</span></p>'+
                                                
                                            '</div>'+
                                        '</li>');
                    }else if(registros.status==4){ 
                        $('#res3').append('<li>'+
                                            '<time datetime="'+registros.fecha_hora+'" style="background-color: gray;">'+
                                                '<span class="day" style="margin-top: 10px;">'+registros.fecha_sola.substr(8, 2)+'</span>'+
                                                '<span class="month">'+registros.mes+'</span>'+
                                            '</time>'+
                                            '<div class="info">'+
                                                '<h2 class="title">'+materia+'</h2>'+
                                                '<p class="desc"><span><img width="32" style="border-radius: 50px;" src="'+registros.imagen_perfil+'"/></span><span> '+registros.nombres+' '+registros.apellidos+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-clock-o fa-lg" ></i></span><span> De'+registros.hora_inicio+' a '+registros.hora_fin+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-info-circle fa-lg" ></i></span><span> '+registros.descripcion+'</span></p>'+
                                                
                                            '</div>'+
                                        '</li>');
                    }


                });
                
            }
        }
    });
}

function aceptar(id_clase){
    $.confirm({
        title: '¿Seguro?',
        content: 'Realmente desea aceptar esta solicitud?',
        theme: 'light', // 'material', 'bootstrap'
        buttons: {
            confirm: function () {
                confirmar(id_clase,1);
            },
            somethingElse: {
                text: 'Cancel',
                btnClass: 'btn-red',
                action: function(){
                    
                }
            }
        }
    });
}

function rechazar(id_clase){
    $.confirm({
        title: '¿Seguro?',
        content: 'Realmente desea rechazar esta solicitud?',
        theme: 'light', // 'material', 'bootstrap'
        buttons: {
            confirm: function () {
                confirmar(id_clase,2);
            },
            somethingElse: {
                text: 'Cancel',
                btnClass: 'btn-red',
                action: function(){
                    
                }
            }
        }
    });
}

function confirmar(id_clase, flag,){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_clases.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "confirmar", "id_clase" : id_clase, "flag" : flag}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data.estado=="actualizado"){
                $.alert({
                        title: 'Exitoso',
                        content: 'Su petición ha sido procesada exitosamente.',
                        theme: 'light', // 'material', 'bootstrap'
                    });
                mis_reservas_prof()
            }
        }
    });
}