<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Star Admin</title>
  <link rel="stylesheet" href="/<?php echo $sistema; ?>/vista/layouts/star/node_modules/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="/<?php echo $sistema; ?>/vista/layouts/star/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css" />
  <link rel="stylesheet" href="/<?php echo $sistema; ?>/vista/layouts/star/css/style.css" />
  <link rel="shortcut icon" href="/<?php echo $sistema; ?>/vista/layouts/star/images/favicon.png" />
  <?php include(RUTA_SISTEMA."/inc/head_sistema_autenticacion.php"); ?>
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid">
      <div class="row">

