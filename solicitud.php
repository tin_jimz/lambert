<?php session_start();
include("control_panel/inc/config.sistema.php");
require_once("control_panel/modelo/config.modelo.php"); # configuracion del modelo      
require_once("control_panel/modelo/class_tbl_estatus_usuarios.php"); # clase del modelo
$Obj_tbl_estatus_usuarios = new tbl_estatus_usuarios;   
require_once("control_panel/modelo/class_tbl_perfiles.php"); # clase del modelo
$Obj_tbl_perfiles = new tbl_perfiles;   
require_once("control_panel/modelo/class_tbl_regiones.php"); # clase del modelo
$Obj_tbl_regiones = new tbl_regiones;
$_SESSION["where"]="";  
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
   <?php include("control_panel/vista/layouts/lampbert/header.php");?>
    <?php include("control_panel/vista/layouts/lampbert/header_js.php");?>
</head>

<body>
    <!-- ***** Header Area  ***** -->
    <?php include("control_panel/vista/layouts/lampbert/header_menu.php");?>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading Area -->
                    <div class="section-heading text-left">
                        <h2 style="font-size: 22px;"><i class="fa fa-navicon"></i> Solicitudes de mis cursos</h2>
                    </div>
                    
                    <hr>
                </div>
            </div>
            
            <div class="row">
                <!-- Single Special Area -->
                <div class="col-md-12">
                    <div class="alert alert-info" role="alert">
                      <i class="fa fa-info-circle fa-lg"></i> En esta sección podra ver los cursos en que participa. 
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="span5">
            <table class="table table-striped table-condensed">
                  <thead>
                  <tr>
                      <th>Curso</th>
                      <th>Fecha Solicitud</th>
                      <th>Inicio / Final del Curso</th>  
                      <th>Acción</th>                                          
                  </tr>
              </thead>   
              <tbody id="mis_solicitudes">

              </tbody>
            </table>
            </div>
                </div>
                
            </div>
        </div>

    </section>

    

    
    <!-- ***** Footer Area Start ***** -->
    <?php include("control_panel/vista/layouts/lampbert/footer.php");?>
<script>
    $(function(){
            $('#inicio_link').click(function(){ location='dashboard.php' })
            $('#menu_bar').css('background-color', '#6654e7')
        });
    $(function(){
        solicitudes();
    })
</script>
<script src="js/cursos.js"></script>

</html>
