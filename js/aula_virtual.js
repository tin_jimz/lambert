$(function(){
    $('#inicio_link').click(function(){ location='dashboard.php' })
    $('#menu_bar').css('background-color', '#6654e7')
    
    $('#link1').click(function(){
        $('#link1').addClass('active');
        $('#link2').removeAttr('class');
    });
    $('#link2').click(function(){
        $('#link2').addClass('active');
        $('#link1').removeAttr('class');
    });

});

function Colapse_Switcher() {
    // Switch theme color
    var $styleSwitcher = $('#style_switcher');
    if (!$styleSwitcher.hasClass('switcher_open')) {
        $styleSwitcher.addClass('switcher_open')
        $('#icono').removeAttr('class').addClass('fa fa-chevron-right')
    } else {
        $styleSwitcher.removeClass('switcher_open')
        $('#icono').removeAttr('class').addClass('fa fa-comments')
    }
}
function DropDown(el) {
        this.dd = el;
        this.placeholder = this.dd.children('span');
        this.opts = this.dd.find('.dropdown a');
        this.val = '';
        this.index = -1;
        this.initEvents();
    }
    DropDown.prototype = {
        initEvents : function() {
            var obj = this;

            obj.dd.on('click', function(event){
                $(this).toggleClass('active');
                return false;
            });

            obj.opts.on('click',function(){
                var opt = $(this);
                obj.val = opt.text();
                obj.index = opt.index();
                obj.placeholder.text(obj.val);
            });
        },
        getValue : function() {
            return this.val;
        },
        getIndex : function() {
            return this.index;
        }
    }

/////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////


function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}            

function chat(id_clase){
    var control = "";
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_chats.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "chat", "id_clase": id_clase}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data){
                $('#ul').html('');
                contador=0;
                $.each(data,function(index_data, registros){
                    contador++;
                    var date = formatAMPM(new Date(registros.fecha_hora));
                    if (registros.remitente == String($('#remitente').val()) ){
                        control = '<li style="width:100%;" id="lista_'+contador+'">' +
                                        '<div class="msj macro">' +
                                        '<div class="avatar"><img class="img-circle" style="width:100%;" src="https://image.flaticon.com/icons/svg/194/194935.svg" /></div>' +
                                            '<div class="text text-l">' +
                                                '<p>'+ registros.mensaje +'</p>' +
                                                '<p><small>'+date+'</small></p>' +
                                            '</div>' +
                                        '</div>' +
                                    '</li>';                    
                    }else{
                        control = '<li style="width:100%;" id="lista_'+contador+'">' +
                                        '<div class="msj-rta macro">' +
                                            '<div class="text text-r">' +
                                                '<p>'+registros.mensaje+'</p>' +
                                                '<p><small>'+date+'</small></p>' +
                                            '</div>' +
                                        '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:100%;" src="https://image.flaticon.com/icons/png/512/206/206855.png" /></div>' +                                
                                  '</li>';
                    }
                    
                    $('#ul').append(control)
                });

                if (contador==0) {
                    $('#ul').html('<span style="color: gray; text-align: center;"><i class="fa fa-comments fa-4x"></i><br>Se el primero el escribir un mensaje...</span>')
                }
            }
            document.getElementById('ul').scrollTop=document.getElementById('ul').scrollHeight;
        }
    });


}

$(".mytext").on("keyup", function(e){
    var date = formatAMPM(new Date());
    if (e.which == 13){
        var text = $(this).val();
        if (text !== ""){
            $('#ul').append('<li style="width:100%;">' +
                                        '<div class="msj macro">' +
                                        '<div class="avatar"><img class="img-circle" style="width:100%;" src="https://image.flaticon.com/icons/svg/194/194935.svg" /></div>' +
                                            '<div class="text text-l">' +
                                                '<p>'+ text +'</p>' +
                                                '<p><small>'+date+'</small></p>' +
                                            '</div>' +
                                        '</div>' +
                                    '</li>')
            document.getElementById('ul').scrollTop=document.getElementById('ul').scrollHeight;
            var remitente = $('#remitente').val();
            var destinatario = $('#destinatario').val();
            var id_clase = $('#id_clase').val();
            $.ajax({
                type: "POST", //The type of HTTP verb (post, get, etc)
                url: "control_panel/controlador/tbl_chats.php", //The URL from the form element where the AJAX request will be sent
                data: {"accion": "insertar", "remitente": remitente, "destinatario": destinatario, "mensaje": text, "id_clase": id_clase}, //All the data from the form serialized
                dataType: "json", //The type of response to expect from the server
                success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
                    if (data.estado=="insertado"){
                        chat(id_clase)
                    }
                }
            });            
            $(this).val('');
        }
    }
});



//////////////////////////////////////////////////////


$(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );    
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Muestra</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $(".image-preview-send").hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Buscar archivo"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Cambiar");
            $(".image-preview-clear").show();
            $(".image-preview-send").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  
});


function cargar_archivo(){
      $('#form_archivo').submit(function (e){
       var data = new FormData(document.getElementById("form_archivo"));
        $.ajax({
            url: "control_panel/controlador/tbl_archivos_clases.php", //URL destino
            data: data,
            processData: false, //Evitamos que JQuery procese los datos, daría error
            contentType: false, //No especificamos ningún tipo de datos
            type: 'POST',
            dataType: "json",
            success: function (data) {                         
               if(data){
                    $('.image-preview').attr("data-content","").popover('hide');
                    $('.image-preview-filename').val("");
                    $('.image-preview-clear').hide();
                    $(".image-preview-send").hide();
                    $('.image-preview-input input:file').val("");
                    $(".image-preview-input-title").text("Buscar archivo"); 
                    list_img()
               }
            }
        });
        e.preventDefault(); //Evitamos que se mande del formulario de forma convencional
        return false;
     });
      return false;
}

function list_img(){
    $.post(
            "control_panel/controlador/tbl_archivos_clases.php",{ //URL destino
            accion: 'list',
            directorio: $('#id_clase').val()
        },function (data){                         
               if(data){
                $('#imagen-mult').html('')
                  $.each(data,function(index_data,registros){
                    
                        $('#imagen-mult').append('<div class="col-md-2" style="min-height: 120"><img onclick="cargar_img(\''+registros.value+'\');" style="margin: 1%; cursor:pointer; max-width: 120px;" src="'+encodeURI(registros.value)+'" class="img-thumbnail image"><br><a style="cursor: pointer;" href="javascript:void(0);" onclick="modal_remove(\''+registros.ruta_remove+'\');" title="Eliminar"><i class="fa fa-trash fa-lg"></i></a>    <a href="control_panel/'+registros.ruta_remove+'" download="'+registros.value+'" title="Descargar"><i class="fa fa-download fa-lg"></i></a><hr /></div>');
                       
                    });
                }
            },'json'
            );
}

function modal_remove(directorio){
    $.confirm({
        title: '¿Seguro?',
        content: 'Realmente desea eliminar el archivo?',
        theme: 'light', // 'material', 'bootstrap'
        buttons: {
            confirm: function () {
                remove_img(directorio);
            },
            somethingElse: {
                text: 'Cancel',
                btnClass: 'btn-red',
                action: function(){
                    
                }
            }
        }
    });
}

function remove_img(directorio){
  $.post("control_panel/controlador/tbl_archivos_clases.php",{ //URL destino
            accion: 'remove_img',
            directorio: directorio
        },function (data){                         
               if(data.mensaje="hecho"){
                    list_img();
                }
            },'json'
            );
}


