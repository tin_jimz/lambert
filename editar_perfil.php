<?php session_start();
include("control_panel/inc/config.sistema.php");
require_once("control_panel/modelo/config.modelo.php"); # configuracion del modelo      
require_once("control_panel/modelo/class_tbl_estatus_usuarios.php"); # clase del modelo
$Obj_tbl_estatus_usuarios = new tbl_estatus_usuarios;   
require_once("control_panel/modelo/class_tbl_perfiles.php"); # clase del modelo
$Obj_tbl_perfiles = new tbl_perfiles;   
require_once("control_panel/modelo/class_tbl_regiones.php"); # clase del modelo
$Obj_tbl_regiones = new tbl_regiones;
$_SESSION["where"]="";  
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <?php include("control_panel/vista/layouts/lampbert/header.php");?>
    <?php include("control_panel/vista/layouts/lampbert/header_js.php");?>
</head>

<body>
    <!-- ***** Header Area  ***** -->
    <?php include("control_panel/vista/layouts/lampbert/header_menu.php");?>
    <?php 
        if ($_SESSION['session_usuario']['id_perfil']==2) {
            $id = $_SESSION['session_usuario']['id_perfil_profesor'];
            $dni = $_SESSION['session_usuario']['dni_profesor'];
            $direccion = $_SESSION['session_usuario']['direccion_profesor'];
            $nac = $_SESSION['session_usuario']['nac_profesor'];
            $telefono = $_SESSION['session_usuario']['telefono_profesor'];
        }elseif ($_SESSION['session_usuario']['id_perfil']==3) {
            $id = $_SESSION['session_usuario']['id_perfil_alumno'];
            $dni = $_SESSION['session_usuario']['dni'];
            $direccion = $_SESSION['session_usuario']['direccion_alumno'];
            $nac = $_SESSION['session_usuario']['nac_alumno'];
            $telefono = $_SESSION['session_usuario']['telefono_alumno'];
        }
        
    ?>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
<section class="special-area bg-white section_padding_100">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <!-- Section Heading Area -->
            <div class="section-heading text-left">
                <h2 style="font-size: 22px;"><i class="fa fa-edit"></i> Editar Mi Perfil</h2>
            </div>
            <hr>
        </div>
        <div class="col-md-4">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-xs-12 col-md-12 text-center">
                        <form id="form_avatar" enctype="multipart/form-data" type="post">
                            <input type="file" name="input_avatar" accept="image/png, image/jpeg" id="input_avatar" style="display: none;">
                            <input type="hidden" name="accion" value="foto_perfil">
                            <img src="<?php echo $img_perfil; ?>" class="img-thumbnail" alt="Perfil" id="img_perfil" style="border-radius: 120px;"><br>
                            <br>
                            <span id="botones" style="display: none;">
                            <button type="sumbit" title="Guardar" class="btn btn-sm btn-primary"><i class="fa fa-check"></i></button>
                            <button type="button" title="Cancelar" id="cancelar_imagen" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                            </span>
                            <br>
                            <h5><?php echo $_SESSION['session_usuario']['nombres'].' '.$_SESSION['session_usuario']['apellidos']; ?>
                            </h5>
                        </form>
                    </div>
                    <div class="col-xs-12 col-md-6 text-center">
                        <hr>
                        <h1 class="rating-num">
                        4.0</h1>
                        <div class="rating">
                            <span class="fa fa-star"></span><span class="fa fa-star">
                            </span><span class="fa fa-star"></span><span class="fa fa-star">
                            </span><span class="fa fa-star-o"></span>
                        </div>
                        <div>
                            <span class="fa fa-user"></span> 1,050,008 total
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <hr>
                        <div class="row rating-desc">
                            <div class="col-xs-3 col-md-3 text-right">
                                <span class="fa fa-star"></span>5
                            </div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                    </div>
                                </div>
                            </div>
                            <!-- end 5 -->
                            <div class="col-xs-3 col-md-3 text-right">
                                <span class="fa fa-star"></span>4
                            </div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                    </div>
                                </div>
                            </div>
                            <!-- end 4 -->
                            <div class="col-xs-3 col-md-3 text-right">
                                <span class="fa fa-star"></span>3
                            </div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                    </div>
                                </div>
                            </div>
                            <!-- end 3 -->
                            <div class="col-xs-3 col-md-3 text-right">
                                <span class="fa fa-star"></span>2
                            </div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    </div>
                                </div>
                            </div>
                            <!-- end 2 -->
                            <div class="col-xs-3 col-md-3 text-right">
                                <span class="fa fa-star"></span>1
                            </div>
                            <div class="col-xs-8 col-md-9">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                    </div>
                                </div>
                            </div>
                            <!-- end 1 -->
                        </div>
                        <!-- end row -->
                    </div>
                </div>
            </div>
        </div>
        <!-- LO QUE SE MUESTRA AL ENTRAR -->
        <div class="col-md-8" id="card_info">
            <label>Información</label> | <a href="#" id="enlace" onclick="activar_edicion();"><i class="fa fa-edit" id="icono_card"></i> Editar Perfil</a>
            <hr style="margin-top: 0;">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-sm-6 col-md-8">
                            <h4><?php echo $_SESSION["session_usuario"]["nombres"] ; ?> <?php echo $_SESSION["session_usuario"]["apellidos"] ; ?>
                            </h4>
                            <small>
                            <cite title="San Francisco, USA"><?php echo $direccion; ; ?>
                            <i class="fa fa-map-marker"></i></cite>
                            </small>
                            <p>
                                <i class="fa fa-envelope"></i><?php echo $_SESSION["session_usuario"]["correo_electronico"] ; ?>
                                <br/>
                                <i class="fa fa-globe"></i><a href="http://www.jquery2dotnet.com">www.blogweb.com</a>
                                <br/>
                                <i class="fa fa-gift"></i><?php echo $nac; ; ?>
                            </p>
                            <a href="#" class="btn btn-sm btn-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-sm btn-info"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-sm btn-success"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <!-- Split button -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>



    
    <!-- ***** Footer Area Start ***** -->
    <?php include("control_panel/vista/layouts/lampbert/footer.php");?>
    <script>
        $(function(){
            $('#inicio_link').click(function(){ location='dashboard.php' })
            $('#menu_bar').css('background-color', '#6654e7')
            foto_perfil();
        });
    </script>

    <style>
        small {
            display: block;
            line-height: 1.428571429;
            color: #999;
            }

            .fa { margin-right:5px; }
            .rating .fa {font-size: 22px; color: #007bff; }
            .rating-num { margin-top:0px;font-size: 54px;}
            .progress { margin-bottom: 5px;}
            .progress-bar { text-align: left; }
            .rating-desc .col-md-3 {padding-right: 0px;}
            .sr-only { margin-left: 5px;overflow: visible;clip: auto; }
        
        #img_perfil{
            width: 40%;
        }
    </style>

</html>
