<?php session_start();
/*
Código necesario para la ejecución
*/

 	include("../../inc/config.sistema.php");
						require_once("../../modelo/config.modelo.php"); # configuracion del modelo
							require_once("../../modelo/class_tbl_perfil_profesor.php"); # clase del modelo
		$Obj_tbl_perfil_profesor = new tbl_perfil_profesor;	require_once("../../modelo/class_tbl_cursos.php"); # clase del modelo
		$Obj_tbl_cursos = new tbl_cursos;

					$_SESSION["where"]="";
			
?>
<?php	include(RUTA_SISTEMA."/vista/layouts/{$defaul_layouts}/header.php"); ?>
		<!--inicio viata de plantilla -->
	
	
		
		<script type="text/javascript" src="/<?php echo $sistema; ?>/js/validaciones/tbl_cursos_estudiantes.js"></script>

		<form id="frm_formulario" name="frm_formulario" class="form-horizontal" role="form" method="post"> <!--  method="post" action="javascript:void(null);" onSubmit="return validar_tbl_cursos_estudiantes(this);" -->
		
		<input name="accion" id="accion" type="hidden" value="<?php echo $_REQUEST["accion"]?>" />
		<input name="quien" id="quien" type="hidden" value="<?php echo $_REQUEST["quien"]?>" />
	<input name="id_curso_estudiante" type="hidden" value="<?php echo $_REQUEST["id_curso_estudiante"]?>" />
<fieldset>
		<!-- Nombre del Formulario -->
		<legend>
			<?php echo strtoupper(str_replace("_"," ",$_REQUEST["accion"])); ?> - Cursos Estudiantes
		</legend>
			<div class="form-group">
				 <label for="id_perfil_alumno" class="col-sm-3 control-label">
					Perfil Alumno
				 </label>
				<div class="col-sm-9">
					
						<select name="id_perfil_alumno" id="id_perfil_alumno" class="form-control" title="Seleccionar perfil alumno">
							<option value="">Seleccione...</option>
							<?php echo $Obj_tbl_perfil_profesor->ver_opciones($_REQUEST["id_perfil_alumno"]);?>
						</select>*
					
				</div>
			</div>
			<div class="form-group">
				 <label for="id_curso" class="col-sm-3 control-label">
					Curso
				 </label>
				<div class="col-sm-9">
					
						<select name="id_curso" id="id_curso" class="form-control" title="Seleccionar curso">
							<option value="">Seleccione...</option>
							<?php echo $Obj_tbl_cursos->ver_opciones($_REQUEST["id_curso"]);?>
						</select>*
					
				</div>
			</div>
			<div class="form-group">
				 <label for="fecha_registro" class="col-sm-3 control-label">
					Fecha Registro
				 </label>
				<div class="col-sm-9">
					
					<input name="fecha_registro" id="fecha_registro" type="text" class="form-control" maxlength="10" size="12" value="<?php echo $_REQUEST["fecha_registro"]; ?>" title="Insertar Fecha Registro" placeholder="Fecha Registro" />*
				</div>
			</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9 boton-grupo">
				 <button type="submit" class="btn btn-success">
					<?php echo strtoupper(str_replace("_"," ",$_REQUEST["accion"])); ?>
				 </button>

				 <button type="reset" class="btn btn-danger">
					Cancelar
				 </button>
			</div>
		</div>
		
		
		</fieldset>
	</form>
	<br />
	<div id="div_listar"></div>
	<div id="cargando"></div>
	
		
	
		<!--fin viata de plantilla -->
<?php include(RUTA_SISTEMA."/vista/layouts/{$defaul_layouts}/footer.php"); ?>