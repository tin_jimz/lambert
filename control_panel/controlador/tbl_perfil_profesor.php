<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_perfil_profesor.php"); # clase del modelo
	$Obj_tbl_perfil_profesor = new tbl_perfil_profesor($_REQUEST["id_perfil_profesor"],$_REQUEST["dni_profesor"],$_REQUEST["direccion_profesor"],$_REQUEST["nac_profesor"],$_REQUEST["telefono_profesor"],$_REQUEST["email_profesor"],$_REQUEST["usuario"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_perfil_profesor->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_perfil_profesor"]=$Obj_tbl_perfil_profesor->insertar();
			if (is_numeric($_REQUEST["id_perfil_profesor"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_perfil_profesor"]=$_REQUEST["id_perfil_profesor"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_perfil_profesor"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_perfil_profesor"]=$Obj_tbl_perfil_profesor->actualizar();
			if (is_numeric($_REQUEST["id_perfil_profesor"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_perfil_profesor"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_perfil_profesor->where=" id_perfil_profesor in (".$_REQUEST["id_perfil_profesor"].")";
		$_REQUEST["id_perfil_profesor"]=$Obj_tbl_perfil_profesor->eliminar();
			if (is_numeric($_REQUEST["id_perfil_profesor"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_perfil_profesor"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_perfil_profesor->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_perfil_profesor"];
				$options[$index+1]["text"] =$valor["perfil_profesor"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "combo_dependiente":
			
			$_SESSION["where"]="";	
			
			$Obj_tbl_perfil_profesor->where=$_REQUEST["where"];
			
			$combo=$Obj_tbl_perfil_profesor->listar(true);
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_perfil_profesor"];
				$options[$index+1]["text"] =$valor["perfil_profesor"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
	}	
	
	?>
	