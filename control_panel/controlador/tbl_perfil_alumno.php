<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_perfil_alumno.php"); # clase del modelo
	$Obj_tbl_perfil_alumno = new tbl_perfil_alumno($_REQUEST["id_perfil_alumno"],$_REQUEST["dni"],$_REQUEST["telefono_alumno"],$_REQUEST["direccion_alumno"],$_REQUEST["email_alumno"],$_REQUEST["nac_alumno"],$_REQUEST["usuario"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_perfil_alumno->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_perfil_alumno"]=$Obj_tbl_perfil_alumno->insertar();
			if (is_numeric($_REQUEST["id_perfil_alumno"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_perfil_alumno"]=$_REQUEST["id_perfil_alumno"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_perfil_alumno"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_perfil_alumno"]=$Obj_tbl_perfil_alumno->actualizar();
			if (is_numeric($_REQUEST["id_perfil_alumno"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_perfil_alumno"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_perfil_alumno->where=" id_perfil_alumno in (".$_REQUEST["id_perfil_alumno"].")";
		$_REQUEST["id_perfil_alumno"]=$Obj_tbl_perfil_alumno->eliminar();
			if (is_numeric($_REQUEST["id_perfil_alumno"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_perfil_alumno"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_perfil_alumno->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_perfil_alumno"];
				$options[$index+1]["text"] =$valor["perfil_alumno"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "combo_dependiente":
			
			$_SESSION["where"]="";	
			
			$Obj_tbl_perfil_alumno->where=$_REQUEST["where"];
			
			$combo=$Obj_tbl_perfil_alumno->listar(true);
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_perfil_alumno"];
				$options[$index+1]["text"] =$valor["perfil_alumno"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
	}	
	
	?>
	