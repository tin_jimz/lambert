$(function(){
            $('#inicio_link').click(function(){ location='dashboard.php' })
            $('#menu_bar').css('background-color', '#6654e7')
            var dd = new DropDown( $('#dd') );
            $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true); 
            });

            $('#product1').on( "click",function(){
                //alert($('#product1').attr('alt'))
                $('#btn1').attr('disabled', 'disabled')
                $('#btn2').removeAttr('disabled')
                $('#dd').html('<span id="selector">Seleccione...</span>'+
                                '<ul class="dropdown">'+
                                '<li><a class="ls" alt="1" href="#">1</a></li>'+
                                '<li><a class="ls" alt="2" href="#">2</a></li>'+
                                '<li><a class="ls" alt="3" href="#">3</a></li>'+
                                '<li><a class="ls" alt="4" href="#">4</a></li>'+
                                '<li><a class="ls" alt="5" href="#">5</a></li>'+
                                '<li><a class="ls" alt="6" href="#">6</a></li>'+
                                '<li><a class="ls" alt="7" href="#">7</a></li>'+
                                '</ul>');
                $('.ls').click(function(){
                    $('#selector').html($(this).html())
                    $('#grado').val($(this).attr('alt'))

                    contar($('#grado').val());

                });
                $('#notice').html('<strong><a href="#" title="Ver Cursos">Realice una petición</a></strong> <span>para obtener resultados.</span>'+
                                '<button disabled type="button" class="btn btn-circle btn-primary pull-right" style="border-radius: 30px; padding: 10px;" title="Ver Cursos"><i class="fa fa-list"></i></button>')
                $('#grado').val('')
            })
            $('#product2').on( "click",function(){
                //alert($('#product2').attr('alt'))
                $('#btn2').attr('disabled', 'disabled')
                $('#btn1').removeAttr('disabled')
                $('#dd').html('<span id="selector">Seleccione...</span>'+
                                '<ul class="dropdown">'+
                                '<li><a class="ls" alt="8" href="#">1</a></li>'+
                                '<li><a class="ls" alt="9" href="#">2</a></li>'+
                                '<li><a class="ls" alt="10" href="#">3</a></li>'+
                                '<li><a class="ls" alt="11" href="#">4</a></li>'+
                                '<li><a class="ls" alt="12" href="#">5</a></li>'+
                                '<li><a class="ls" alt="13" href="#">6</a></li>'+
                                '</ul>');
                $('.ls').click(function(){
                    $('#selector').html($(this).html())
                    $('#grado').val($(this).attr('alt'))
                    contar($('#grado').val());
                });
                $('#notice').html('<strong><a href="#" title="Ver Cursos">Realice una petición</a></strong> <span>para obtener resultados.</span>'+
                                '<button disabled type="button" class="btn btn-circle btn-primary pull-right" style="border-radius: 30px; padding: 10px;" title="Ver Cursos"><i class="fa fa-list"></i></button>')
                $('#grado').val('')
            })

            

        });

        function Colapse_Switcher() {
            // Switch theme color
            var $styleSwitcher = $('#style_switcher');
            if (!$styleSwitcher.hasClass('switcher_open')) {
                $styleSwitcher.addClass('switcher_open')
                $('#icono').removeAttr('class').addClass('fa fa-chevron-right')
            } else {
                $styleSwitcher.removeClass('switcher_open')
                $('#icono').removeAttr('class').addClass('fa fa-comments')
            }
        }
        function DropDown(el) {
                this.dd = el;
                this.placeholder = this.dd.children('span');
                this.opts = this.dd.find('.dropdown a');
                this.val = '';
                this.index = -1;
                this.initEvents();
            }
            DropDown.prototype = {
                initEvents : function() {
                    var obj = this;

                    obj.dd.on('click', function(event){
                        $(this).toggleClass('active');
                        return false;
                    });

                    obj.opts.on('click',function(){
                        var opt = $(this);
                        obj.val = opt.text();
                        obj.index = opt.index();
                        obj.placeholder.text(obj.val);
                    });
                },
                getValue : function() {
                    return this.val;
                },
                getIndex : function() {
                    return this.index;
                }
            }

function contar(grado){
	$.ajax({
		type: "POST", //The type of HTTP verb (post, get, etc)
		url: "control_panel/controlador/tbl_cursos.php", //The URL from the form element where the AJAX request will be sent
		data: {"accion": "contar", "grado": grado}, //All the data from the form serialized
		dataType: "json", //The type of response to expect from the server
		success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
			if (data.cuenta>0){
				$('#notice').html('<strong><a href="#" title="Ver Cursos">'+data.cuenta+' Cursos</a></strong>'+ 
								'<span> se encontraron para este Grado.</span>'+
                        		'<button type="button" onclick="modal_cursos('+grado+');" class="btn btn-circle btn-primary pull-right" style="border-radius: 30px; padding: 10px;" title="Ver Cursos">'+
                        		'<i class="fa fa-arrows-alt"></i></button>')
			}else{
				$('#notice').html('<strong><a href="#" title="Ver Cursos">No se obtuvo resultados</a></strong> <span> para el Grado seleccionado.</span>'+
                        		'<button disabled type="button" class="btn btn-circle btn-primary pull-right" style="border-radius: 30px; padding: 10px;" title="Ver Cursos"><i class="fa fa-arrows-alt"></i></button>')
			}
		}
	});
}

function modal_cursos(grado){
	$('#myModal').modal('show');
	$.ajax({
		type: "POST", //The type of HTTP verb (post, get, etc)
		url: "control_panel/controlador/tbl_cursos.php", //The URL from the form element where the AJAX request will be sent
		data: {"accion": "buscar_cursos", "grado": grado}, //All the data from the form serialized
		dataType: "json", //The type of response to expect from the server
		success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
			if (data){
                $('#list_cursos').html('');
				$.each(data,function(index_data, registros){
                $('#list_cursos').append('<li class="list-group-item" style="display: inherit;">'+
                        '<div class="col-lg-3 col-md-3">'+
                            '<img src="img/book.png" alt="Scott Stevens" class="img-responsive img-thumbnail" style="width: 70%; border-radius: 60px;" />'+
                        '</div>'+
                        '<div class="col-lg-7 col-md-7">'+
                            '<span class="name">'+registros.tema+'</span><br/>'+
                            '<span class="fa fa-calendar text-muted"></span>'+
                            '<span class="visible-xs"> <span class="text-muted">'+registros.fecha_inicio+' - '+registros.fecha_fin+'</span><br/></span>'+
                            '<span class="fa fa-user text-muted"></span>'+
                            '<span class="visible-xs"> <span class="text-muted">Prof. '+registros.nombres+' '+registros.apellidos+'</span><br/></span>'+
                            '<span class="fa fa-ellipsis-h text-muted"></span>'+
                            '<span class="visible-xs"> <span class="text-muted">'+registros.descripcion+'</span><br/></span>'+
                        '</div>'+
                        '<div class="col-lg-2 col-md-2">'+
                            '<button type="button" onclick="seleccionar('+registros.id_curso+',\''+registros.tema+'\',\''+registros.nombres+'\',\''+registros.apellidos+'\',\''+registros.fecha_inicio+'\',\''+registros.fecha_fin+'\');" class="btn btn-success" title="Seleccionar" style="border-radius: 50px; margin-top: 40%;"><i class="fa fa-check-square-o"></i></button>'+
                        '</div>'+
                        '<div class="clearfix"></div>'+
                    '</li>')
				});
			}
		}
	});
}

function seleccionar(id_curso, tema, nombre, apellido, inicio, fin){
    $('#myModal').modal('hide');
    $('#notice').html('<strong><a href="#" title="Ver Cursos"><i class="fa fa-pencil"></i> '+tema+'</a></strong>'+ 
                                '<br><span><i class="fa fa-user"></i> Prof. '+nombre+' '+apellido+'</span>'+
                                '<br><span class="text-muted" style="font-size: 13px;"><i class="fa fa-calendar"></i> '+inicio+' - '+fin+'</span>'+
                                '<button type="button" onclick="registrar_curso('+id_curso+');" class="btn btn-circle btn-primary pull-right" style="border-radius:0;" title="Completar">'+
                                '<i class="fa fa-check"></i> Completar</button>')
}

function registrar_curso(id_curso){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_cursos_estudiantes.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "registrar_curso", "id_curso": id_curso}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data.estado=='insertado'){
                $.confirm({
                        title: 'Exitoso!',
                        content: 'Espera a que el Profesor acepte su solicitud!',
                        theme: 'supervan', // 'material', 'bootstrap'
                        buttons: {
                            somethingElse: {
                                text: 'Aceptar',
                                btnClass: 'btn-blue',
                                action: function(){
                                    location ='cursos.php';
                                }
                            }
                        }
                    });
            }
        }
    });
}


$(function () {
    /* BOOTSNIPP FULLSCREEN FIX */
    if (window.location == window.parent.location) {
        $('#back-to-bootsnipp').removeClass('hide');
    }
    
    
    $('[data-toggle="tooltip"]').tooltip();
    
    $('#fullscreen').on('click', function(event) {
        event.preventDefault();
        window.parent.location = "http://bootsnipp.com/iframe/4l0k2";
    });
    $('a[href="#cant-do-all-the-work-for-you"]').on('click', function(event) {
        event.preventDefault();
        $('#cant-do-all-the-work-for-you').modal('show');
    })
    
    $('[data-command="toggle-search"]').on('click', function(event) {
        event.preventDefault();
        $(this).toggleClass('hide-search');
        
        if ($(this).hasClass('hide-search')) {        
            $('.c-search').closest('.row').slideUp(100);
        }else{   
            $('.c-search').closest('.row').slideDown(100);
        }
    })
    
    $('#contact-list').searchable({
        searchField: '#contact-list-search',
        selector: 'li',
        childSelector: '.col-xs-12',
        show: function( elem ) {
            elem.slideDown(100);
        },
        hide: function( elem ) {
            elem.slideUp( 100 );
        }
    })
});

/////////////////////////MUESTRA CURSOS DE LOS ALUMNOS//////////////////////////////////

function mis_cursos(){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_cursos_estudiantes.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "mis_cursos"}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data){
                contador=0;
                $('#mis_cursos').html('');
                $.each(data,function(index_data, registros){
                contador++;
                    if(registros.status==1){ clase = 'btn-primary'; label='Aceptado';}else if(registros.status==2){ clase = 'btn-warning'; label='Pendiente';}else if(registros.status==3){ clase = 'btn-danger'; label='Rechazado';}
                $('#mis_cursos').append('<tr>'+
                                            '<td>'+registros.tema+'</td>'+
                                            '<td>'+registros.descripcion+'</td>'+
                                            '<td>'+registros.nombres+' '+registros.apellidos+'</td>'+
                                            '<td>'+registros.fecha_inicio+' / '+registros.fecha_fin+'</td>'+
                                            '<td><span class="'+clase+'" style="border-radius: 15px; padding: 3px 10px;">'+label+'</span></td>'+
                                        '</tr> ')
                });

                if (contador==0) {
                    $('#mis_cursos').html('<tr><td colspan="5" style="text-align: center;">No hay registros...</td></tr>')
                }
            }
        }
    });
}

///////////////////MUESTRA LAS SOLICITRUDES DE ENTRADA A LOS CURSOS DE LOS PROF////////////////////
function solicitudes(){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_cursos.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "solicitudes"}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data){
                contador = 0;
                $('#mis_solicitudes').html('');
                $.each(data,function(index_data, registros){
                contador++;
                //if(registros.status==1){ clase = 'btn-primary'; label='Aceptado';}else if(registros.status==2){ clase = 'btn-warning'; label='Pendiente';}else if(registros.status==3){ clase = 'btn-danger'; label='Rechazado';}
                $('#mis_solicitudes').append('<tr>'+
                                            '<td>'+registros.tema+'</td>'+
                                            '<td>'+registros.fecha_registro+'</td>'+
                                            '<td>'+registros.fecha_inicio+' / '+registros.fecha_fin+'</td>'+
                                            '<td><button type="button" onclick="confirmar('+registros.id_curso_estudiante+');" class="btn btn-sm btn-primary"><i class="fa fa-thumbs-up"></i> Aceptar</button> <button type="button" onclick="rechazar('+registros.id_curso_estudiante+');" class="btn btn-sm btn-danger"><i class="fa fa-thumbs-down"></i> Rechazar</button></td>'+
                                        '</tr> ')
                });

                if (contador==0) {
                    $('#mis_solicitudes').html('<tr><td colspan="4" style="text-align: center;">No hay registros...</td></tr>')
                }
            }
        }
    });
}

function confirmar(id_solicitud){
    $.confirm({
        title: '¿Seguro?',
        content: 'Realmente desea aceptar esta solicitud?',
        theme: 'light', // 'material', 'bootstrap'
        buttons: {
            confirm: function () {
                aceptar_solicitud(id_solicitud)
            },
            somethingElse: {
                text: 'Cancel',
                btnClass: 'btn-red',
                action: function(){
                    
                }
            }
        }
    });
}

function aceptar_solicitud(id_solicitud){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_cursos_estudiantes.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "aceptar_solicitud", "id_solicitud": id_solicitud}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data.estado=='actualizado'){
                    $.confirm({
                        title: 'Exitoso!',
                        content: 'La solicitud fue aceptada satisfactoriamente!',
                        theme: 'supervan', // 'material', 'bootstrap'
                        buttons: {
                            somethingElse: {
                                text: 'Aceptar',
                                btnClass: 'btn-blue',
                                action: function(){
                                    solicitudes();
                                }
                            }
                        }
                    });
            }
        }
    });
}


function rechazar(id_solicitud){
    $.confirm({
        title: '¿Seguro?',
        content: 'Realmente desea rechazar esta solicitud?',
        theme: 'light', // 'material', 'bootstrap'
        buttons: {
            confirm: function () {
                rechazar_solicitud(id_solicitud)
            },
            somethingElse: {
                text: 'Cancel',
                btnClass: 'btn-red',
                action: function(){
                    
                }
            }
        }
    });
}

function rechazar_solicitud(id_solicitud){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_cursos_estudiantes.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "rechazar_solicitud", "id_solicitud": id_solicitud}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data.estado=='actualizado'){
                    $.confirm({
                        title: 'Exitoso!',
                        content: 'La solicitud fue rechazada satisfactoriamente!',
                        theme: 'supervan', // 'material', 'bootstrap'
                        buttons: {
                            somethingElse: {
                                text: 'Aceptar',
                                btnClass: 'btn-blue',
                                action: function(){
                                    solicitudes();
                                }
                            }
                        }
                    });
            }
        }
    });
}