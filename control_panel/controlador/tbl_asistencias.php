<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_clases.php"); # clase del modelo
	$Obj_tbl_clases = new tbl_clases();
	require_once("../modelo/class_tbl_cursos.php"); # clase del modelo
	$Obj_tbl_cursos = new tbl_cursos();
	require_once("../modelo/class_tbl_perfil_profesor.php"); # clase del modelo
	$Obj_tbl_perfil_profesor = new tbl_perfil_profesor();
	require_once("../modelo/class_tbl_asistencias.php"); # clase del modelo
	$Obj_tbl_asistencias = new tbl_asistencias($_REQUEST["id_asistencia"],$_REQUEST["id_clase"],$_REQUEST["id_perfil_alumno"],$_REQUEST["status"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_asistencias->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_asistencia"]=$Obj_tbl_asistencias->insertar();
			if (is_numeric($_REQUEST["id_asistencia"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_asistencia"]=$_REQUEST["id_asistencia"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_asistencia"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_asistencia"]=$Obj_tbl_asistencias->actualizar();
			if (is_numeric($_REQUEST["id_asistencia"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_asistencia"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_asistencias->where=" id_asistencia in (".$_REQUEST["id_asistencia"].")";
		$_REQUEST["id_asistencia"]=$Obj_tbl_asistencias->eliminar();
			if (is_numeric($_REQUEST["id_asistencia"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_asistencia"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_asistencias->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_asistencia"];
				$options[$index+1]["text"] =$valor["asistencia"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "mis_reservas":
			$Obj_tbl_asistencias->where=" tbl_asistencias.id_perfil_alumno=".$_SESSION['session_usuario']['id_perfil_alumno']." AND tbl_asistencias.status=2 ";
			
			$combo=$Obj_tbl_asistencias->listar(true);
			$options=array();

			$datetime = date('Y-m-d H:i:s');
				
			foreach($combo as $index => $valor)
			{
				

				$Obj_tbl_clases->where=" tbl_clases.id_clase=".$valor["id_clase"]." ";
				$clases=$Obj_tbl_clases->listar(true);
				
				$options[$index+1]["id_asistencia"]=$valor["id_asistencia"];
				$options[$index+1]["descripcion"]=$valor["descripcion"];

				$options[$index+1]["fecha_hora"]=$clases[0]["fecha_hora"];
				$options[$index+1]["status"]=$clases[0]["status"];
				$options[$index+1]["id_clase"]=$clases[0]["id_clase"];

				$Obj_tbl_cursos->where=" tbl_cursos.id_curso=".$clases[0]["id_curso"]." ";
				$cursos=$Obj_tbl_cursos->listar(true);

				$options[$index+1]["materia"]=$cursos[0]["materia"];

				$Obj_tbl_perfil_profesor->where=" tbl_perfil_profesor.id_perfil_profesor=".$cursos[0]["id_perfil_profesor"]." ";
				$profesor=$Obj_tbl_perfil_profesor->listar(true);

				$options[$index+1]["nombres"]=$profesor[0]["nombres"];
				$options[$index+1]["apellidos"]=$profesor[0]["apellidos"];

				$hora = strtotime(date(substr($clases[0]["fecha_hora_fin"], 11)));
				$options[$index+1]["hora_fin"] = date("g:i a", $hora);

				$hora2 = strtotime(date(substr($clases[0]["fecha_hora"], 11)));
				$options[$index+1]["hora_inicio"] = date("g:i a", $hora2);

				$options[$index+1]["fecha_sola"] = substr($clases[0]["fecha_hora"], 0, 10);

				if($datetime >= date($clases[0]["fecha_hora_fin"])){
					$Obj_tbl_clases->where=" id_clase=".$valor["id_clase"]." AND status<>3 ";
					$Obj_tbl_clases->actualizar_campo('status=4');
				}

				if(intval($clases[0]["status"])==1){
					if($datetime >= date($clases[0]["fecha_hora"]) AND $datetime <= date($clases[0]["fecha_hora_fin"])){
						$options[$index+1]["flag"] = 1;
					}else{
						$options[$index+1]["flag"] = 2;
					}
				}

				

			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		case "calendario":
			$Obj_tbl_asistencias->where=" tbl_asistencias.id_perfil_alumno=".$_SESSION['session_usuario']['id_perfil_alumno']." AND tbl_asistencias.status=2 ";
			
			$combo=$Obj_tbl_asistencias->listar(true);
			$options=array();

			$datetime = date('Y-m-d H:i:s');
				
			foreach($combo as $index => $valor)
			{
				

				$Obj_tbl_clases->where=" tbl_clases.id_clase=".$valor["id_clase"]." ";
				$clases=$Obj_tbl_clases->listar(true);

				if($clases[0]["status"]==1 OR $clases[0]["status"]==2 OR $clases[0]["status"]==4){
					$options[$index+1]["id_asistencia"]=$valor["id_asistencia"];

				$options[$index+1]["fecha_hora"]=$clases[0]["fecha_hora"];
				$options[$index+1]["fecha_hora_fin"]=$clases[0]["fecha_hora_fin"];
				$options[$index+1]["status"]=$clases[0]["status"];

				$Obj_tbl_cursos->where=" tbl_cursos.id_curso=".$clases[0]["id_curso"]." ";
				$cursos=$Obj_tbl_cursos->listar(true);

				$options[$index+1]["materia"]=$cursos[0]["materia"];

				$Obj_tbl_perfil_profesor->where=" tbl_perfil_profesor.id_perfil_profesor=".$cursos[0]["id_perfil_profesor"]." ";
				$profesor=$Obj_tbl_perfil_profesor->listar(true);

				$options[$index+1]["nombres"]=$profesor[0]["nombres"];
				$options[$index+1]["apellidos"]=$profesor[0]["apellidos"];

				    $hora = strtotime(date(substr($clases[0]["fecha_hora_fin"], 11)));
    				$options[$index+1]["hora_fin"] = date("g:i a", $hora);

    				$hora2 = strtotime(date(substr($clases[0]["fecha_hora"], 11)));
    				$options[$index+1]["hora_inicio"] = date("g:i a", $hora2);

    				$options[$index+1]["fecha_sola"] = substr($clases[0]["fecha_hora"], 0, 10);

				if($datetime >= date($clases[0]["fecha_hora_fin"])){
					$Obj_tbl_clases->where=" id_clase=".$valor["id_clase"]."  AND status<>3 ";
					$Obj_tbl_clases->actualizar_campo('status=4');
				}
			}

			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		###########################################################################################

		case "calendario_prof":
			$Obj_tbl_asistencias->where=" tbl_asistencias.status=2 ";
			
			$combo=$Obj_tbl_asistencias->listar(true);
			$options=array();

			$datetime = date('Y-m-d H:i:s');
				
			foreach($combo as $index => $valor)
			{
				

				$Obj_tbl_clases->where=" tbl_clases.id_clase=".$valor["id_clase"]." ";
				$clases=$Obj_tbl_clases->listar(true);

				if($clases[0]["status"]==1 OR $clases[0]["status"]==2 OR $clases[0]["status"]==4){
					

				$Obj_tbl_cursos->where=" tbl_cursos.id_curso=".$clases[0]["id_curso"]." ";
				$cursos=$Obj_tbl_cursos->listar(true);

				if($cursos[0]["id_perfil_profesor"]==$_SESSION['session_usuario']['id_perfil_profesor']){
					$options[$index+1]["id_asistencia"]=$valor["id_asistencia"];

					$options[$index+1]["fecha_hora"]=$clases[0]["fecha_hora"];
					$options[$index+1]["fecha_hora_fin"]=$clases[0]["fecha_hora_fin"];
					$options[$index+1]["status"]=$clases[0]["status"];

					$options[$index+1]["materia"]=$cursos[0]["materia"];

					    $hora = strtotime(date(substr($clases[0]["fecha_hora_fin"], 11)));
	    				$options[$index+1]["hora_fin"] = date("g:i a", $hora);

	    				$hora2 = strtotime(date(substr($clases[0]["fecha_hora"], 11)));
	    				$options[$index+1]["hora_inicio"] = date("g:i a", $hora2);

	    				$options[$index+1]["fecha_sola"] = substr($clases[0]["fecha_hora"], 0, 10);

					if($datetime >= date($clases[0]["fecha_hora_fin"])){
						$Obj_tbl_clases->where=" id_clase=".$valor["id_clase"]."  AND status<>3 ";
						$Obj_tbl_clases->actualizar_campo('status=4');
					}

				}
			}

			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
	}	
	
	?>
	