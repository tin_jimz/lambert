<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_perfil_pariente.php"); # clase del modelo
	$Obj_tbl_perfil_pariente = new tbl_perfil_pariente($_REQUEST["id_perfil_pariente"],$_REQUEST["dni"],$_REQUEST["telefono_pariente"],$_REQUEST["direccion_pariente"],$_REQUEST["email_pariente"],$_REQUEST["nac_pariente"],$_REQUEST["usuario"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_perfil_pariente->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_perfil_pariente"]=$Obj_tbl_perfil_pariente->insertar();
			if (is_numeric($_REQUEST["id_perfil_pariente"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_perfil_pariente"]=$_REQUEST["id_perfil_pariente"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_perfil_pariente"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_perfil_pariente"]=$Obj_tbl_perfil_pariente->actualizar();
			if (is_numeric($_REQUEST["id_perfil_pariente"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_perfil_pariente"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_perfil_pariente->where=" id_perfil_pariente in (".$_REQUEST["id_perfil_pariente"].")";
		$_REQUEST["id_perfil_pariente"]=$Obj_tbl_perfil_pariente->eliminar();
			if (is_numeric($_REQUEST["id_perfil_pariente"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_perfil_pariente"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_perfil_pariente->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_perfil_pariente"];
				$options[$index+1]["text"] =$valor["perfil_alumno"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "combo_dependiente":
			
			$_SESSION["where"]="";	
			
			$Obj_tbl_perfil_pariente->where=$_REQUEST["where"];
			
			$combo=$Obj_tbl_perfil_pariente->listar(true);
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_perfil_pariente"];
				$options[$index+1]["text"] =$valor["perfil_alumno"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
	}	
	
	?>
	