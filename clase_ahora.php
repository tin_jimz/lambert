<?php session_start();
include("control_panel/inc/config.sistema.php");
require_once("control_panel/modelo/config.modelo.php"); # configuracion del modelo      
require_once("control_panel/modelo/class_tbl_estatus_usuarios.php"); # clase del modelo
$Obj_tbl_estatus_usuarios = new tbl_estatus_usuarios;   
require_once("control_panel/modelo/class_tbl_perfiles.php"); # clase del modelo
$Obj_tbl_perfiles = new tbl_perfiles;   
require_once("control_panel/modelo/class_tbl_regiones.php"); # clase del modelo
$Obj_tbl_regiones = new tbl_regiones;
$_SESSION["where"]="";  
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <?php include("control_panel/vista/layouts/lampbert/header.php");?>
    <?php include("control_panel/vista/layouts/lampbert/header_js.php");?>
    <link rel="stylesheet" href="js/time/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="js/time/css/bootstrap-datetimepicker-standalone.min.css" />
</head>

<body>
    <!-- ***** Header Area  ***** -->
    <?php include("control_panel/vista/layouts/lampbert/header_menu.php");?>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading Area -->
                    <div class="section-heading text-left">
                        <h2 style="font-size: 22px;"><i class="fa fa-pencil"></i> Clase Urgente</h2>
                    </div>
                    
                    <hr>
                </div>
            </div>
            
            <div class="row">
                <!-- Single Special Area -->
                <div class="col-md-12">
                    <div class="alert alert-info alert-dismissable" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <i class="fa fa-info-circle fa-lg"></i> Recuerde que para las clases urgentes solo puede solicitar los profesores conectados. 
                    </div>
                </div>
                <div class="col-md-12">
                        
                        <div class="row form-group product-chooser">
                            <div class="col-md-8">
                            <h6>Elegí el Nivel</h6>
                            <hr>
                        </div>
                        <div class="col-md-4">
                            <h6>Elegí el Grado</h6>
                            <hr>
                        </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">

                                <div alt="1" id="product1" class="product-chooser-item" style="margin-left: 0; border-radius: 0;">
                                    <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                        <span class="title"><i class="fa fa-pencil"></i> PRIMARIA <button type="button" id="btn1" class="btn btn-primary btn-sm pull-right" style="border-radius: 0px;">Seleccionar <i class="fa fa-check"></i></button></span>
                                        <input type="radio" name="nivel" id="sel1">
                                        
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div alt="2" id="product2" class="product-chooser-item" style="margin-left: 0; border-radius: 0;">
                                    <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                        <span class="title"><i class="fa fa-book"></i> SECUNDARIA <button type="button" id="btn2" class="btn btn-primary btn-sm pull-right" style="border-radius: 0px;">Seleccionar <i class="fa fa-check"></i></button></span>
                                        <input type="radio" name="nivel" id="sel2">
                                        
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <select name="grado" id="grado" class="form-control">
                                    <option value="">Seleccione...</option>
                                </select>
                            </div>
                    </div>

                    <div class="col-md-12">
                        <h6>Elegí la Materia</h6><hr>
                        
                        <div class="notice notice-lg notice-info row" id="notice">
                            <strong><a href="#" title="Ver Cursos">Realice una petición&nbsp;</a></strong><span> para obtener resultados.</span>
                            
                        </div>
                        <hr>
                    </div>

                    <div class="col-md-12">
                    <h6 id="head6" style="display: none">Elegí la hora y duración de clases<hr></h6>
                        <div class="notice notice-md notice-success row" style="display: none" id="boton_urg">
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class='input-group' id='datetimepicker3'>
                                        <input type='text' class="form-control" id="timepick" placeholder="Seleccione la hora" />
                                        <span class="input-group-addon" style="background-color: white;">
                                            <span class="fa fa-clock-o"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <select name="duracion" id="duracion" class="form-control">
                                    <option value="">Seleccione la duración...</option>
                                    <option value="30">30 Minutos</option>
                                    <option value="45">45 Minutos</option>
                                    <option value="60">1 Hora</option>
                                    <option value=75"">1 Hora 15 Min.</option>
                                    <option value="90">1 Hora 30 Min.</option>
                                    <option value="105">1 Hora 45 Min.</option>
                                    <option value="120">2 Horas</option>
                                    <option value="135">2 Horas 15 Min.</option>
                                    <option value="150">2 Horas 30 Min.</option>
                                    <option value="165">2 Horas 45 Min.</option>
                                    <option value="180">3 Horas</option>
                                </select>       
                            </div>
                            <div class="col-md-4">
                                <button type="button" onclick="completar()" class="btn btn-primary pull-right" style="border-radius: 0;"><i class="fa fa-star"></i> Completar</button>
                            </div>
                        </div>

                    </div>
                
                </div>
                
            </div>
        </div>

    </section>

    
    <!-- ***** Footer Area Start ***** -->
    <?php include("control_panel/vista/layouts/lampbert/footer.php");?>
    <script type="text/javascript" src="js/time/js/moment.js"></script>
    <script type="text/javascript" src="js/time/js/bootstrap-datetimepicker.min.js"></script>
    
 <script src="js/urgente.js"></script>

 <script>
     $('#datetimepicker3').datetimepicker({
        format: 'HH:mm'
    });
 </script>

</html>
