<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_materias.php"); # clase del modelo
	$Obj_tbl_materias = new tbl_materias($_REQUEST["id_materia"],$_REQUEST["materia"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_materias->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_materia"]=$Obj_tbl_materias->insertar();
			if (is_numeric($_REQUEST["id_materia"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_materia"]=$_REQUEST["id_materia"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_materia"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_materia"]=$Obj_tbl_materias->actualizar();
			if (is_numeric($_REQUEST["id_materia"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_materia"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_materias->where=" id_materia in (".$_REQUEST["id_materia"].")";
		$_REQUEST["id_materia"]=$Obj_tbl_materias->eliminar();
			if (is_numeric($_REQUEST["id_materia"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_materia"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_materias->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_materia"];
				$options[$index+1]["text"] =$valor["materia"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "combo_dependiente":
			
			$_SESSION["where"]="";	
			
			$Obj_tbl_materias->where=$_REQUEST["where"];
			
			$combo=$Obj_tbl_materias->listar(true);
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_materia"];
				$options[$index+1]["text"] =$valor["materia"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
	}	
	
	?>
	