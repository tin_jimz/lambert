-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 26-01-2018 a las 21:06:30
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_lambert`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_archivos`
--

CREATE TABLE `tbl_archivos` (
  `id_archivo` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `ruta` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_archivos_clases`
--

CREATE TABLE `tbl_archivos_clases` (
  `id_archivo_clase` int(11) NOT NULL,
  `id_clase` int(11) NOT NULL,
  `ruta` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_archivos_clases`
--

INSERT INTO `tbl_archivos_clases` (`id_archivo_clase`, `id_clase`, `ruta`, `usuario`) VALUES
(23, 5, 'biblioteca/5/Imagen 004.jpg', 'martin'),
(24, 5, 'biblioteca/5/IMG_0037.JPG', 'martin'),
(25, 5, 'biblioteca/5/IMG_0045.JPG', 'martin'),
(26, 5, 'biblioteca/5/22641957_1634503126570987_1062062853_o.png', 'martin'),
(27, 5, 'biblioteca/5/24774924_1318241558279738_6460640289650109676_n.jpg', 'martin'),
(28, 5, 'biblioteca/5/26907426_1836677793029464_2914615885656177737_n.jpg', 'martin'),
(29, 5, 'biblioteca/5/235787ac7c9f049.jpg', 'martin'),
(30, 5, 'biblioteca/5/11890000_10208032150868114_2506780343229014629_o.jpg', 'martin'),
(31, 5, 'biblioteca/5/17457370_10213054072373013_3671901529335359599_n.jpg', 'martin'),
(32, 5, 'biblioteca/5/22641957_1634503126570987_1062062853_o.png', 'martin'),
(33, 5, 'biblioteca/5/IMG_0040.JPG', 'martin'),
(34, 5, 'biblioteca/5/Imagen 006.jpg', 'martin'),
(35, 5, 'biblioteca/5/firma.jpg', 'martin'),
(36, 5, 'biblioteca/5/Imagen 007.jpg', 'martin'),
(37, 5, 'biblioteca/5/IMG_0037.JPG', 'martin'),
(38, 5, 'biblioteca/5/IMG_0045.JPG', 'martin'),
(39, 5, 'biblioteca/5/11890000_10208032150868114_2506780343229014629_o.jpg', 'martin'),
(40, 5, 'biblioteca/5/23916761_1174412139360653_8853742017654154722_o.jpg', 'martin'),
(41, 5, 'biblioteca/5/26754692_10215929132327715_938204329_n.jpg', 'martin'),
(42, 5, 'biblioteca/5/Imagen 002.jpg', 'martin'),
(43, 5, 'biblioteca/5/Imagen 007.jpg', 'martin'),
(44, 5, 'biblioteca/5/Imagen 003.jpg', 'martin'),
(45, 5, 'biblioteca/5/Imagen 005.jpg', 'martin'),
(46, 5, 'biblioteca/5/26907426_1836677793029464_2914615885656177737_n.jpg', 'martin'),
(47, 5, 'biblioteca/5/Imagen 004.jpg', 'martin'),
(48, 5, 'biblioteca/5/firma.jpg', 'martin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_asistencias`
--

CREATE TABLE `tbl_asistencias` (
  `id_asistencia` int(11) NOT NULL,
  `id_clase` int(11) NOT NULL,
  `id_perfil_alumno` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_asistencias`
--

INSERT INTO `tbl_asistencias` (`id_asistencia`, `id_clase`, `id_perfil_alumno`, `status`) VALUES
(2, 1, 1, 2),
(3, 2, 1, 2),
(4, 3, 1, 2),
(5, 4, 1, 2),
(6, 5, 1, 2),
(7, 6, 1, 2),
(8, 7, 1, 2),
(9, 8, 1, 2),
(10, 9, 1, 2),
(11, 10, 1, 2),
(12, 11, 1, 2),
(13, 12, 1, 2),
(14, 13, 1, 2),
(16, 15, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_calendario`
--

CREATE TABLE `tbl_calendario` (
  `id_calendario` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_hora_fin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_calendario`
--

INSERT INTO `tbl_calendario` (`id_calendario`, `id_curso`, `fecha_hora`, `fecha_hora_fin`, `descripcion`) VALUES
(1, 1, '2018-01-17 14:00:00', '2018-01-17 15:00:00', '                                    \n                                '),
(2, 2, '2018-01-17 13:50:00', '2018-01-17 18:00:00', ''),
(3, 3, '2018-01-19 17:45:00', '2018-01-19 20:00:00', 'ahora si'),
(4, 3, '2018-01-21 14:30:00', '2018-01-21 16:00:00', 'Necesito explicacion urgente'),
(5, 1, '2018-01-24 14:00:00', '2018-01-24 16:00:00', 'Necesito lecciones de fracciones'),
(6, 2, '2018-01-27 17:30:00', '2018-01-27 18:30:00', 'Fuerzas de traccion y arrastre'),
(7, 4, '2018-01-28 14:00:00', '2018-01-28 15:00:00', 'Quimica de primer grado secundaria'),
(8, 4, '2018-01-30 12:30:00', '2018-01-30 13:15:00', 'Asesoria urgente'),
(9, 3, '2018-02-01 14:00:00', '2018-02-01 15:00:00', 'Masa atomita de las materias'),
(10, 1, '2018-02-02 16:10:00', '2018-02-02 18:10:00', 'Necesito aplicaciones de suma y resta'),
(13, 2, '2018-01-31 22:09:42', '2018-01-31 22:39:00', 'dsewewewe'),
(14, 1, '2018-01-26 03:00:00', '1970-01-01 02:00:00', '$_REQUEST[\'hora\']'),
(15, 2, '2018-01-26 03:00:00', '2018-01-26 05:00:00', '$_REQUEST[\'hora\']');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_chats`
--

CREATE TABLE `tbl_chats` (
  `id_mensaje` int(11) NOT NULL,
  `id_clase` int(11) NOT NULL,
  `remitente` varchar(100) NOT NULL,
  `destinatario` varchar(100) NOT NULL,
  `mensaje` varchar(100) NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_chats`
--

INSERT INTO `tbl_chats` (`id_mensaje`, `id_clase`, `remitente`, `destinatario`, `mensaje`, `fecha_hora`) VALUES
(1, 3, 'martin', 'prueba', 'Hola', '2018-01-18 21:00:11'),
(2, 3, 'prueba', 'martin', 'que tal?', '2018-01-18 21:00:11'),
(3, 3, 'prueba', 'martin', 'que mas que hay ?', '2018-01-18 21:34:13'),
(4, 3, 'prueba', 'martin', 'Holaaaa', '2018-01-18 21:35:37'),
(5, 3, 'prueba', 'martin', 'holaaa', '2018-01-18 21:35:44'),
(6, 3, 'prueba', 'martin', 'ww', '2018-01-18 21:36:40'),
(7, 3, 'prueba', 'martin', 'ajaa', '2018-01-18 21:44:00'),
(8, 3, 'prueba', 'martin', 'sdsd', '2018-01-18 21:44:27'),
(9, 3, 'prueba', 'martin', 'a', '2018-01-18 22:24:40'),
(10, 3, 'prueba', 'martin', 'aja', '2018-01-18 22:28:28'),
(11, 3, 'martin', 'prueba', 'ajaa', '2018-01-18 22:31:50'),
(12, 3, 'prueba', 'martin', 'aja', '2018-01-18 22:32:54'),
(13, 3, 'prueba', 'martin', 'sdsd', '2018-01-18 22:36:52'),
(14, 3, 'prueba', 'martin', 'sdsd', '2018-01-18 22:37:23'),
(15, 3, 'prueba', 'martin', 'sdsd', '2018-01-18 22:37:48'),
(16, 3, 'prueba', 'martin', 'asdasd', '2018-01-18 22:38:04'),
(17, 3, 'prueba', 'martin', 'sdsd', '2018-01-18 22:40:06'),
(18, 3, 'prueba', 'martin', 'sdsd', '2018-01-18 22:40:33'),
(19, 3, 'prueba', 'martin', 'sadasd', '2018-01-18 22:40:49'),
(20, 3, 'prueba', 'martin', 'ajaaaa', '2018-01-18 22:41:12'),
(21, 3, 'prueba', 'martin', 'sdsdsd', '2018-01-18 22:41:15'),
(22, 3, 'prueba', 'martin', 'sdsd', '2018-01-18 22:41:17'),
(23, 3, 'prueba', 'martin', 'sdasd', '2018-01-18 22:41:31'),
(24, 3, 'prueba', 'martin', 'siiiiiii', '2018-01-18 22:41:43'),
(25, 3, 'prueba', 'martin', 'asdasd', '2018-01-18 22:42:16'),
(26, 3, 'prueba', 'martin', 'werwer', '2018-01-18 22:42:40'),
(27, 3, 'prueba', 'martin', 'asdasdsd', '2018-01-18 22:42:57'),
(28, 3, 'prueba', 'martin', 'sdsd', '2018-01-18 22:42:59'),
(29, 3, 'prueba', 'martin', 'g', '2018-01-18 22:43:01'),
(30, 3, 'prueba', 'martin', '1', '2018-01-18 22:43:02'),
(31, 3, 'prueba', 'martin', 'sdsd', '2018-01-18 22:51:43'),
(32, 3, 'prueba', 'martin', 'dfdf', '2018-01-18 22:53:09'),
(33, 3, 'prueba', 'martin', 'sds', '2018-01-18 22:53:13'),
(34, 3, 'prueba', 'martin', 'sds', '2018-01-18 22:53:14'),
(35, 3, 'prueba', 'martin', 'sdsd', '2018-01-18 22:53:16'),
(36, 3, 'prueba', 'martin', 'ajaa', '2018-01-18 22:58:23'),
(37, 3, 'prueba', 'martin', 'sdsad', '2018-01-18 22:59:36'),
(38, 3, 'prueba', 'martin', 'jsjsjdsd', '2018-01-18 22:59:56'),
(39, 3, 'prueba', 'martin', 'jaajajajaja', '2018-01-18 23:00:35'),
(40, 3, 'prueba', 'martin', 'aksaks', '2018-01-18 23:00:37'),
(41, 3, 'prueba', 'martin', 'asas', '2018-01-18 23:00:37'),
(42, 3, 'prueba', 'martin', 'sdsd', '2018-01-18 23:00:39'),
(43, 3, 'prueba', 'martin', '343', '2018-01-18 23:00:40'),
(44, 3, 'prueba', 'martin', 'sdsdsd', '2018-01-18 23:03:16'),
(45, 3, 'prueba', 'martin', 'ajaaa', '2018-01-18 23:03:34'),
(46, 5, 'martin', 'prueba', 'ajaaa', '2018-01-22 21:55:26'),
(47, 5, 'martin', 'prueba', 'aja', '2018-01-23 00:07:35'),
(48, 5, 'martin', 'prueba', 'as', '2018-01-23 00:07:39'),
(49, 5, 'martin', 'prueba', 'hola', '2018-01-23 15:35:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_clases`
--

CREATE TABLE `tbl_clases` (
  `id_clase` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_hora_fin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descripcion` text NOT NULL,
  `status` int(11) NOT NULL,
  `pizarra` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_clases`
--

INSERT INTO `tbl_clases` (`id_clase`, `id_curso`, `fecha_hora`, `fecha_hora_fin`, `descripcion`, `status`, `pizarra`) VALUES
(1, 1, '2018-01-17 14:00:00', '2018-01-17 15:00:00', '              ', 4, NULL),
(2, 2, '2018-01-17 13:50:00', '2018-01-17 18:00:00', '', 4, NULL),
(3, 3, '2018-01-19 17:45:00', '2018-01-19 20:00:00', 'ahora si', 4, NULL),
(4, 3, '2018-01-21 14:30:00', '2018-01-21 16:00:00', 'Necesito explicacion urgente', 4, NULL),
(5, 1, '2018-01-24 14:00:00', '2018-01-24 16:00:00', 'Necesito lecciones de fracciones', 4, 'biblioteca/5/pizarra.png'),
(6, 2, '2018-01-27 17:30:00', '2018-01-27 18:30:00', 'Fuerzas de traccion y arrastre', 1, NULL),
(7, 4, '2018-01-28 14:00:00', '2018-01-28 15:00:00', 'Quimica de primer grado secundaria', 1, NULL),
(8, 4, '2018-01-30 12:30:00', '2018-01-30 13:15:00', 'Asesoria urgente', 1, NULL),
(9, 3, '2018-02-01 14:00:00', '2018-02-01 15:00:00', 'Masa atomita de las materias', 1, NULL),
(10, 1, '2018-02-02 16:10:00', '2018-02-02 18:10:00', 'Necesito aplicaciones de suma y resta', 3, NULL),
(13, 2, '2018-01-31 22:09:42', '2018-01-31 22:39:00', 'dsewewewe', 2, NULL),
(15, 2, '2018-01-26 03:00:00', '2018-01-26 05:00:00', '$_REQUEST[\'hora\']', 2, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_clases_urgentes`
--

CREATE TABLE `tbl_clases_urgentes` (
  `id_clase_urgente` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `id_perfil_alumno` int(11) NOT NULL,
  `fecha_hora_inicio` timestamp NULL DEFAULT NULL,
  `fecha_hora_fin` timestamp NULL DEFAULT NULL,
  `descripcion` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_clases_urgentes`
--

INSERT INTO `tbl_clases_urgentes` (`id_clase_urgente`, `id_curso`, `id_perfil_alumno`, `fecha_hora_inicio`, `fecha_hora_fin`, `descripcion`, `status`) VALUES
(2, 1, 1, '2018-01-12 18:13:19', '2018-01-12 01:30:00', '', 3),
(3, 1, 1, '2018-01-25 03:00:00', '2018-01-25 00:45:00', '', 3),
(4, 2, 1, '2018-01-25 03:00:00', '2018-01-25 00:30:00', 'Ahora es urgente', 3),
(8, 3, 1, '2018-01-25 02:35:00', '2018-01-25 05:35:00', 'sdsdsdsdfsdfsd sdfsdfsd sdfsdfsdf', 3),
(9, 2, 1, '2018-01-25 21:00:00', '2018-01-25 22:00:00', 'Arrastre', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_cursos`
--

CREATE TABLE `tbl_cursos` (
  `id_curso` int(11) NOT NULL,
  `id_perfil_profesor` int(11) NOT NULL,
  `id_materia` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `grado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_cursos`
--

INSERT INTO `tbl_cursos` (`id_curso`, `id_perfil_profesor`, `id_materia`, `descripcion`, `fecha_inicio`, `fecha_fin`, `grado`) VALUES
(1, 1, 1, 'Adicion, substraccion, division y multiplicacion.', '2018-01-09', '2018-02-28', 6),
(2, 1, 2, 'Fuerza de arrastre y magnetismo.', '2018-01-09', '2018-02-09', 6),
(3, 1, 3, 'Alcanos, Alquenos y alquinos.', '2018-01-07', '2018-01-31', 6),
(4, 4, 3, '', '2018-01-22', '2019-01-22', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_cursos_estudiantes`
--

CREATE TABLE `tbl_cursos_estudiantes` (
  `id_curso_estudiante` int(11) NOT NULL,
  `id_perfil_alumno` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL,
  `fecha_registro` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_cursos_estudiantes`
--

INSERT INTO `tbl_cursos_estudiantes` (`id_curso_estudiante`, `id_perfil_alumno`, `id_curso`, `fecha_registro`, `status`) VALUES
(1, 1, 1, '2018-01-10', 1),
(2, 1, 3, '2018-01-10', 3),
(3, 1, 2, '2018-01-10', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estatus_usuarios`
--

CREATE TABLE `tbl_estatus_usuarios` (
  `id_estatu_usuario` int(11) NOT NULL,
  `estatu_usuario` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_estatus_usuarios`
--

INSERT INTO `tbl_estatus_usuarios` (`id_estatu_usuario`, `estatu_usuario`) VALUES
(1, 'Activos'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_materias`
--

CREATE TABLE `tbl_materias` (
  `id_materia` int(11) NOT NULL,
  `materia` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_materias`
--

INSERT INTO `tbl_materias` (`id_materia`, `materia`) VALUES
(1, 'Matematica'),
(2, 'Fisica'),
(3, 'Quimica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_modulos`
--

CREATE TABLE `tbl_modulos` (
  `id_modulo` int(11) NOT NULL,
  `modulo` varchar(80) NOT NULL,
  `descripcion_modulo` text NOT NULL,
  `posicion_modulo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_modulos`
--

INSERT INTO `tbl_modulos` (`id_modulo`, `modulo`, `descripcion_modulo`, `posicion_modulo`) VALUES
(1, 'Configuracion del Sistema', 'Configuracion del Sistema', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_modulos_perfiles`
--

CREATE TABLE `tbl_modulos_perfiles` (
  `id_modulo_perfil` int(11) NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_modulos_perfiles`
--

INSERT INTO `tbl_modulos_perfiles` (`id_modulo_perfil`, `id_modulo`, `id_perfil`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_perfiles`
--

CREATE TABLE `tbl_perfiles` (
  `id_perfil` int(11) NOT NULL,
  `perfil` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_perfiles`
--

INSERT INTO `tbl_perfiles` (`id_perfil`, `perfil`) VALUES
(1, 'Super Administrador del sistema'),
(2, 'Profesor'),
(3, 'Alumno'),
(4, 'Pariente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_perfil_alumno`
--

CREATE TABLE `tbl_perfil_alumno` (
  `id_perfil_alumno` int(11) NOT NULL,
  `dni` varchar(100) NOT NULL,
  `telefono_alumno` varchar(100) NOT NULL,
  `direccion_alumno` varchar(100) NOT NULL,
  `email_alumno` varchar(100) NOT NULL,
  `nac_alumno` date NOT NULL,
  `usuario` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_perfil_alumno`
--

INSERT INTO `tbl_perfil_alumno` (`id_perfil_alumno`, `dni`, `telefono_alumno`, `direccion_alumno`, `email_alumno`, `nac_alumno`, `usuario`) VALUES
(1, '', '', '', '', '0000-00-00', 'martin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_perfil_pariente`
--

CREATE TABLE `tbl_perfil_pariente` (
  `id_perfil_pariente` int(11) NOT NULL,
  `dni` varchar(100) NOT NULL,
  `telefono_pariente` varchar(100) NOT NULL,
  `direccion_pariente` varchar(100) NOT NULL,
  `email_pariente` varchar(100) NOT NULL,
  `nac_pariente` date NOT NULL,
  `usuario` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_perfil_profesor`
--

CREATE TABLE `tbl_perfil_profesor` (
  `id_perfil_profesor` int(11) NOT NULL,
  `dni_profesor` varchar(100) NOT NULL,
  `direccion_profesor` varchar(100) NOT NULL,
  `nac_profesor` date NOT NULL,
  `telefono_profesor` varchar(100) NOT NULL,
  `email_profesor` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_perfil_profesor`
--

INSERT INTO `tbl_perfil_profesor` (`id_perfil_profesor`, `dni_profesor`, `direccion_profesor`, `nac_profesor`, `telefono_profesor`, `email_profesor`, `usuario`) VALUES
(1, '19978716', 'Boca de Sabana', '1992-07-03', '', '', 'prueba'),
(2, '', '', '0000-00-00', '', '', 'tinsystem'),
(4, '10078716', '', '0000-00-00', '', '', 'profesor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_planes`
--

CREATE TABLE `tbl_planes` (
  `id_plan` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `precio_mensual` float NOT NULL,
  `porcentaje_por_clase` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_planes_usuarios`
--

CREATE TABLE `tbl_planes_usuarios` (
  `id_plan_usuario` int(11) NOT NULL,
  `id_plan` int(11) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_regiones`
--

CREATE TABLE `tbl_regiones` (
  `id_region` int(11) NOT NULL,
  `region` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_regiones`
--

INSERT INTO `tbl_regiones` (`id_region`, `region`) VALUES
(1, 'Distrito Capital'),
(2, 'Maturin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_sub_modulos`
--

CREATE TABLE `tbl_sub_modulos` (
  `id_sub_modulo` int(11) NOT NULL,
  `sub_modulo` text NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `posicion_sub_modulo` int(11) NOT NULL,
  `descripcion_sub_modulo` text NOT NULL,
  `enlace` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_sub_modulos`
--

INSERT INTO `tbl_sub_modulos` (`id_sub_modulo`, `sub_modulo`, `id_modulo`, `posicion_sub_modulo`, `descripcion_sub_modulo`, `enlace`) VALUES
(1, 'Perfiles', 1, 1, 'Perfiles', '../tbl_perfiles/formulario.php?accion=insertar'),
(2, 'Modulos', 1, 2, 'Modulos', '../tbl_modulos/formulario.php?accion=insertar'),
(3, 'Modulos Perfiles', 1, 3, 'Modulos Perfiles', '../tbl_modulos_perfiles/formulario.php?accion=insertar'),
(4, 'Sub Modulos', 1, 4, 'sub_modulos', '../tbl_sub_modulos/formulario.php?accion=insertar'),
(5, 'Regiones', 1, 5, 'Regiones', '../tbl_regiones/formulario.php?accion=insertar'),
(6, 'Estatus Usuarios', 1, 6, 'Estatus Usuarios', '../tbl_estatus_usuarios/formulario.php?accion=insertar'),
(7, 'Usuarios', 1, 7, 'Usuarios', '../tbl_usuarios/formulario.php?accion=insertar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuarios`
--

CREATE TABLE `tbl_usuarios` (
  `cedula` varchar(12) DEFAULT NULL,
  `usuario` varchar(50) NOT NULL,
  `clave` varchar(15) NOT NULL,
  `nombres` varchar(80) DEFAULT NULL,
  `apellidos` varchar(80) DEFAULT NULL,
  `correo_electronico` varchar(80) NOT NULL,
  `id_estatu_usuario` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `online` int(11) NOT NULL,
  `imagen_perfil` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`cedula`, `usuario`, `clave`, `nombres`, `apellidos`, `correo_electronico`, `id_estatu_usuario`, `id_perfil`, `id_region`, `online`, `imagen_perfil`) VALUES
(NULL, 'martin', '123456', 'Martin', 'Jimenez', 'martin@hotmail.com', 1, 3, 1, 0, 'control_panel/perfil/martin/17457370_10213054072373013_3671901529335359599_n.jpg'),
(NULL, 'nia', '12345', NULL, NULL, 'nia@nia.com', 1, 3, 1, 0, 'img/user.png'),
(NULL, 'pariente', '123456', NULL, NULL, 'pariente@pariente.com', 1, 4, 1, 0, 'img/user.png'),
(NULL, 'profesor', '123456', 'Eligio', 'Jimenez', 'prof@prof.com', 1, 2, 1, 2, 'img/user.png'),
(NULL, 'prueba', '123456', 'Maria', 'Sanchez', 'prueba@prueba.com', 1, 2, 1, 1, 'control_panel/perfil/prueba/cropped.jpg'),
(NULL, 'super_admin', '123456', NULL, NULL, 'jimenito@hotmail.com', 1, 1, 1, 0, NULL),
(NULL, 'tinsystem', '123456', NULL, NULL, 'dsdsd@sdsdfsd.com', 1, 2, 1, 0, 'img/user.png');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_archivos`
--
ALTER TABLE `tbl_archivos`
  ADD PRIMARY KEY (`id_archivo`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `tbl_archivos_clases`
--
ALTER TABLE `tbl_archivos_clases`
  ADD PRIMARY KEY (`id_archivo_clase`),
  ADD KEY `id_clase` (`id_clase`),
  ADD KEY `usuario` (`usuario`);

--
-- Indices de la tabla `tbl_asistencias`
--
ALTER TABLE `tbl_asistencias`
  ADD PRIMARY KEY (`id_asistencia`),
  ADD KEY `id_clase` (`id_clase`),
  ADD KEY `id_perfil_alumno` (`id_perfil_alumno`);

--
-- Indices de la tabla `tbl_calendario`
--
ALTER TABLE `tbl_calendario`
  ADD PRIMARY KEY (`id_calendario`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `tbl_chats`
--
ALTER TABLE `tbl_chats`
  ADD PRIMARY KEY (`id_mensaje`),
  ADD KEY `id_clase` (`id_clase`);

--
-- Indices de la tabla `tbl_clases`
--
ALTER TABLE `tbl_clases`
  ADD PRIMARY KEY (`id_clase`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `tbl_clases_urgentes`
--
ALTER TABLE `tbl_clases_urgentes`
  ADD PRIMARY KEY (`id_clase_urgente`),
  ADD KEY `id_curso` (`id_curso`),
  ADD KEY `id_perfil_alumno` (`id_perfil_alumno`),
  ADD KEY `id_curso_2` (`id_curso`);

--
-- Indices de la tabla `tbl_cursos`
--
ALTER TABLE `tbl_cursos`
  ADD PRIMARY KEY (`id_curso`),
  ADD KEY `id_perfil_profesor` (`id_perfil_profesor`),
  ADD KEY `id_materia` (`id_materia`);

--
-- Indices de la tabla `tbl_cursos_estudiantes`
--
ALTER TABLE `tbl_cursos_estudiantes`
  ADD PRIMARY KEY (`id_curso_estudiante`),
  ADD KEY `id_curso` (`id_curso`),
  ADD KEY `id_perfil_alumno` (`id_perfil_alumno`);

--
-- Indices de la tabla `tbl_estatus_usuarios`
--
ALTER TABLE `tbl_estatus_usuarios`
  ADD PRIMARY KEY (`id_estatu_usuario`);

--
-- Indices de la tabla `tbl_materias`
--
ALTER TABLE `tbl_materias`
  ADD PRIMARY KEY (`id_materia`);

--
-- Indices de la tabla `tbl_modulos`
--
ALTER TABLE `tbl_modulos`
  ADD PRIMARY KEY (`id_modulo`);

--
-- Indices de la tabla `tbl_modulos_perfiles`
--
ALTER TABLE `tbl_modulos_perfiles`
  ADD PRIMARY KEY (`id_modulo_perfil`);

--
-- Indices de la tabla `tbl_perfiles`
--
ALTER TABLE `tbl_perfiles`
  ADD PRIMARY KEY (`id_perfil`);

--
-- Indices de la tabla `tbl_perfil_alumno`
--
ALTER TABLE `tbl_perfil_alumno`
  ADD PRIMARY KEY (`id_perfil_alumno`),
  ADD UNIQUE KEY `dni` (`dni`),
  ADD KEY `usuario` (`usuario`);

--
-- Indices de la tabla `tbl_perfil_pariente`
--
ALTER TABLE `tbl_perfil_pariente`
  ADD PRIMARY KEY (`id_perfil_pariente`),
  ADD UNIQUE KEY `id_perfil_pariente` (`id_perfil_pariente`),
  ADD UNIQUE KEY `dni` (`dni`),
  ADD KEY `usuario` (`usuario`);

--
-- Indices de la tabla `tbl_perfil_profesor`
--
ALTER TABLE `tbl_perfil_profesor`
  ADD PRIMARY KEY (`id_perfil_profesor`),
  ADD UNIQUE KEY `dni_profesor` (`dni_profesor`),
  ADD KEY `usuario` (`usuario`);

--
-- Indices de la tabla `tbl_planes`
--
ALTER TABLE `tbl_planes`
  ADD PRIMARY KEY (`id_plan`);

--
-- Indices de la tabla `tbl_planes_usuarios`
--
ALTER TABLE `tbl_planes_usuarios`
  ADD PRIMARY KEY (`id_plan_usuario`),
  ADD KEY `usuario` (`usuario`),
  ADD KEY `id_plan` (`id_plan`);

--
-- Indices de la tabla `tbl_regiones`
--
ALTER TABLE `tbl_regiones`
  ADD PRIMARY KEY (`id_region`);

--
-- Indices de la tabla `tbl_sub_modulos`
--
ALTER TABLE `tbl_sub_modulos`
  ADD PRIMARY KEY (`id_sub_modulo`);

--
-- Indices de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD PRIMARY KEY (`usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_archivos_clases`
--
ALTER TABLE `tbl_archivos_clases`
  MODIFY `id_archivo_clase` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT de la tabla `tbl_asistencias`
--
ALTER TABLE `tbl_asistencias`
  MODIFY `id_asistencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `tbl_calendario`
--
ALTER TABLE `tbl_calendario`
  MODIFY `id_calendario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `tbl_chats`
--
ALTER TABLE `tbl_chats`
  MODIFY `id_mensaje` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de la tabla `tbl_clases`
--
ALTER TABLE `tbl_clases`
  MODIFY `id_clase` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `tbl_clases_urgentes`
--
ALTER TABLE `tbl_clases_urgentes`
  MODIFY `id_clase_urgente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `tbl_cursos`
--
ALTER TABLE `tbl_cursos`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tbl_cursos_estudiantes`
--
ALTER TABLE `tbl_cursos_estudiantes`
  MODIFY `id_curso_estudiante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tbl_estatus_usuarios`
--
ALTER TABLE `tbl_estatus_usuarios`
  MODIFY `id_estatu_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tbl_materias`
--
ALTER TABLE `tbl_materias`
  MODIFY `id_materia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tbl_modulos`
--
ALTER TABLE `tbl_modulos`
  MODIFY `id_modulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tbl_modulos_perfiles`
--
ALTER TABLE `tbl_modulos_perfiles`
  MODIFY `id_modulo_perfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tbl_perfiles`
--
ALTER TABLE `tbl_perfiles`
  MODIFY `id_perfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tbl_perfil_alumno`
--
ALTER TABLE `tbl_perfil_alumno`
  MODIFY `id_perfil_alumno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tbl_perfil_pariente`
--
ALTER TABLE `tbl_perfil_pariente`
  MODIFY `id_perfil_pariente` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tbl_perfil_profesor`
--
ALTER TABLE `tbl_perfil_profesor`
  MODIFY `id_perfil_profesor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tbl_regiones`
--
ALTER TABLE `tbl_regiones`
  MODIFY `id_region` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tbl_sub_modulos`
--
ALTER TABLE `tbl_sub_modulos`
  MODIFY `id_sub_modulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_archivos`
--
ALTER TABLE `tbl_archivos`
  ADD CONSTRAINT `tbl_archivos_ibfk_1` FOREIGN KEY (`id_curso`) REFERENCES `tbl_cursos` (`id_curso`);

--
-- Filtros para la tabla `tbl_archivos_clases`
--
ALTER TABLE `tbl_archivos_clases`
  ADD CONSTRAINT `tbl_archivos_clases_ibfk_1` FOREIGN KEY (`id_clase`) REFERENCES `tbl_clases` (`id_clase`);

--
-- Filtros para la tabla `tbl_asistencias`
--
ALTER TABLE `tbl_asistencias`
  ADD CONSTRAINT `tbl_asistencias_ibfk_2` FOREIGN KEY (`id_perfil_alumno`) REFERENCES `tbl_perfil_alumno` (`id_perfil_alumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_asistencias_ibfk_3` FOREIGN KEY (`id_clase`) REFERENCES `tbl_clases` (`id_clase`);

--
-- Filtros para la tabla `tbl_calendario`
--
ALTER TABLE `tbl_calendario`
  ADD CONSTRAINT `tbl_calendario_ibfk_1` FOREIGN KEY (`id_curso`) REFERENCES `tbl_cursos` (`id_curso`);

--
-- Filtros para la tabla `tbl_chats`
--
ALTER TABLE `tbl_chats`
  ADD CONSTRAINT `tbl_chats_ibfk_1` FOREIGN KEY (`id_clase`) REFERENCES `tbl_clases` (`id_clase`);

--
-- Filtros para la tabla `tbl_clases`
--
ALTER TABLE `tbl_clases`
  ADD CONSTRAINT `tbl_clases_ibfk_1` FOREIGN KEY (`id_curso`) REFERENCES `tbl_cursos` (`id_curso`);

--
-- Filtros para la tabla `tbl_clases_urgentes`
--
ALTER TABLE `tbl_clases_urgentes`
  ADD CONSTRAINT `tbl_clases_urgentes_ibfk_1` FOREIGN KEY (`id_curso`) REFERENCES `tbl_cursos` (`id_curso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_clases_urgentes_ibfk_2` FOREIGN KEY (`id_perfil_alumno`) REFERENCES `tbl_perfil_alumno` (`id_perfil_alumno`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_cursos`
--
ALTER TABLE `tbl_cursos`
  ADD CONSTRAINT `tbl_cursos_ibfk_1` FOREIGN KEY (`id_perfil_profesor`) REFERENCES `tbl_perfil_profesor` (`id_perfil_profesor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_cursos_ibfk_2` FOREIGN KEY (`id_materia`) REFERENCES `tbl_materias` (`id_materia`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tbl_cursos_estudiantes`
--
ALTER TABLE `tbl_cursos_estudiantes`
  ADD CONSTRAINT `tbl_cursos_estudiantes_ibfk_2` FOREIGN KEY (`id_curso`) REFERENCES `tbl_cursos` (`id_curso`),
  ADD CONSTRAINT `tbl_cursos_estudiantes_ibfk_3` FOREIGN KEY (`id_perfil_alumno`) REFERENCES `tbl_perfil_alumno` (`id_perfil_alumno`);

--
-- Filtros para la tabla `tbl_planes_usuarios`
--
ALTER TABLE `tbl_planes_usuarios`
  ADD CONSTRAINT `tbl_planes_usuarios_ibfk_1` FOREIGN KEY (`id_plan`) REFERENCES `tbl_planes` (`id_plan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
