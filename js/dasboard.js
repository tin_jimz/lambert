$(function(){
	
});


function clases_mensual(){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_clases.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "clases_mensual"}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data){
                arreglo = [];

                $.each(data,function(index_data, registros){
                	if(registros.clases===null || registros.clases==="null"){ registros.clases = 0; }
                    arreglo.push({y: registros.mes, a: registros.clases})
                });

                var data = arreglo,
				            config = {
				              data: data,
				              xkey: 'y',
				              ykeys: ['a'],
				              labels: ['Clases', 'Total Outcome'],
				              fillOpacity: 0.6,
				              hideHover: 'auto',
				              behaveLikeLine: true,
				              resize: true,
				              pointStrokeColors: ['black'],
				              lineColors:['gray','red']
				          };
				        
				        
				        config.element = 'bar-chart';
				        Morris.Bar(config);
            }
        }
    });
}