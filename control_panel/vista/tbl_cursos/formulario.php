<?php session_start();
/*
Código necesario para la ejecución
*/

 	include("../../inc/config.sistema.php");
						require_once("../../modelo/config.modelo.php"); # configuracion del modelo
							require_once("../../modelo/class_tbl_perfil_profesor.php"); # clase del modelo
		$Obj_tbl_perfil_profesor = new tbl_perfil_profesor;

					$_SESSION["where"]="";
			
?>
<?php	include(RUTA_SISTEMA."/vista/layouts/{$defaul_layouts}/header.php"); ?>
		<!--inicio viata de plantilla -->
	
	
		
		<script type="text/javascript" src="/<?php echo $sistema; ?>/js/validaciones/tbl_cursos.js"></script>

		<form id="frm_formulario" name="frm_formulario" class="form-horizontal" role="form" method="post"> <!--  method="post" action="javascript:void(null);" onSubmit="return validar_tbl_cursos(this);" -->
		
		<input name="accion" id="accion" type="hidden" value="<?php echo $_REQUEST["accion"]?>" />
		<input name="quien" id="quien" type="hidden" value="<?php echo $_REQUEST["quien"]?>" />
	<input name="id_curso" type="hidden" value="<?php echo $_REQUEST["id_curso"]?>" />
<fieldset>
		<!-- Nombre del Formulario -->
		<legend>
			<?php echo strtoupper(str_replace("_"," ",$_REQUEST["accion"])); ?> - Cursos
		</legend>
			<div class="form-group">
				 <label for="id_perfil_profesor" class="col-sm-3 control-label">
					Perfil Profesor
				 </label>
				<div class="col-sm-9">
					
						<select name="id_perfil_profesor" id="id_perfil_profesor" class="form-control" title="Seleccionar perfil profesor">
							<option value="">Seleccione...</option>
							<?php echo $Obj_tbl_perfil_profesor->ver_opciones($_REQUEST["id_perfil_profesor"]);?>
						</select>*
					
				</div>
			</div>
			<div class="form-group">
				 <label for="tema" class="col-sm-3 control-label">
					Tema
				 </label>
				<div class="col-sm-9">
					
					<input name="tema" id="tema" type="text" class="form-control" maxlength="100" size="12" value="<?php echo $_REQUEST["tema"]; ?>" title="Insertar Tema" placeholder="Tema" />*
				</div>
			</div>
			<div class="form-group">
				 <label for="descripcion" class="col-sm-3 control-label">
					Descripcion
				 </label>
				<div class="col-sm-9">
					
					<textarea class="form-control" name="descripcion" id="descripcion" cols="50" rows="7" title="Insertar Descripcion"><?php echo $_REQUEST["descripcion"]; ?></textarea>*
					
				</div>
			</div>
			<div class="form-group">
				 <label for="fecha_inicio" class="col-sm-3 control-label">
					Fecha Inicio
				 </label>
				<div class="col-sm-9">
					
					<input name="fecha_inicio" id="fecha_inicio" type="text" class="form-control" maxlength="10" size="12" value="<?php echo $_REQUEST["fecha_inicio"]; ?>" title="Insertar Fecha Inicio" placeholder="Fecha Inicio" />*
				</div>
			</div>
			<div class="form-group">
				 <label for="fecha_fin" class="col-sm-3 control-label">
					Fecha Fin
				 </label>
				<div class="col-sm-9">
					
					<input name="fecha_fin" id="fecha_fin" type="text" class="form-control" maxlength="10" size="12" value="<?php echo $_REQUEST["fecha_fin"]; ?>" title="Insertar Fecha Fin" placeholder="Fecha Fin" />*
				</div>
			</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9 boton-grupo">
				 <button type="submit" class="btn btn-success">
					<?php echo strtoupper(str_replace("_"," ",$_REQUEST["accion"])); ?>
				 </button>

				 <button type="reset" class="btn btn-danger">
					Cancelar
				 </button>
			</div>
		</div>
		
		
		</fieldset>
	</form>
	<br />
	<div id="div_listar"></div>
	<div id="cargando"></div>
	
		
	
		<!--fin viata de plantilla -->
<?php include(RUTA_SISTEMA."/vista/layouts/{$defaul_layouts}/footer.php"); ?>