var canvas, ctx,
    brush = {
        x: 0,
        y: 0,
        color: '#000000',
        size: 10,
        down: false,
    },
    strokes = [],
    currentStroke = null;
    var circulo;
$('#tamanio').html(10).css('color', 'white');
$('#span').css('color', 'white');

function redraw () {
    ctx.clearRect(0, 0, canvas.width(), canvas.height());
    ctx.lineCap = 'round';
    for (var i = 0; i < strokes.length; i++) {
        var s = strokes[i];
        ctx.strokeStyle = s.color;
        ctx.lineWidth = s.size;
        ctx.beginPath();
        ctx.moveTo(s.points[0].x, s.points[0].y);
        for (var j = 0; j < s.points.length; j++) {
            var p = s.points[j];
            ctx.lineTo(p.x, p.y);
        }
        ctx.stroke();
    }
}

function init () {
    canvas = $('#draw');
    canvas.attr({
        width: window.innerWidth,
        height: window.innerHeight,
    });
    ctx = canvas[0].getContext('2d');

    function mouseEvent (e) {
        brush.x = e.pageX;
        brush.y = e.pageY;

        currentStroke.points.push({
            x: brush.x,
            y: brush.y,
        });

        redraw();
    }

    var id_clase = $('#id_clase').val();
    $.post("control_panel/controlador/tbl_clases.php",{ //URL destino
            accion: 'obtener_pizarra',
            id_clase: id_clase
        },function (data){                         
               if(data){
                    var img = new Image();
                    img.src = data.pizarra;

                    ctx.drawImage(img, 0, 0);
                    
                }
            },'json'
            );

    canvas.mousedown(function (e) {
        brush.down = true;

        currentStroke = {
            color: brush.color,
            size: brush.size,
            points: [],
        };

        strokes.push(currentStroke);

        mouseEvent(e);
    }).mouseup(function (e) {
        brush.down = false;

        mouseEvent(e);

        currentStroke = null;
    }).mousemove(function (e) {
        if (brush.down){
          mouseEvent(e);
        }
        x = e.clientX;
        y = e.clientY;

    });

    $('#save-btn').click(function () {
        var id_clase = $('#id_clase').val();
        var url = canvas[0].toDataURL();

        $.post("control_panel/controlador/tbl_clases.php",{ //URL destino
            accion: 'pizarra',
            url: url,
            id_clase: id_clase
            },function (data){                         
               if(data){
                    
                }
            },'json'
            );
    });

    $('#undo-btn').click(function () {
        strokes.pop();
        redraw();
    });

    $('#clear-btn').click(function () {
        strokes = [];
        redraw();
    });

    $('#color-picker').on('input', function () {
        brush.color = this.value;
    });

    $('#brush-size').on('input', function () {
        brush.size = this.value;
    }).on('change', function(){
        $('#tamanio').html($(this).val());
    });
}

$(init);
