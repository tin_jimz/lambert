$(function(){
	$('#lab1').click(function(){
		$('#titulo').html('Perfil');
		$('#id_perfil').attr('readonly','readonly').html('<option value="">...</option>'+
                            				  '<option value="2" selected>Profesor</option>');
	})

	$('#lab2').click(function(){
		$('#titulo').html('Seleccione Su Perfil');
		$('#id_perfil').removeAttr('readonly').html('<option value="">...</option>'+
                            '<option value="3">Alumno</option>'+
                            '<option value="4">Pariente</option>');
	})

	$('#img_perfil').mouseover(function(){
		$(this).css('cursor', 'pointer').css('opacity', '0.5').attr('title', 'Cambiar Imagen')
	}).mouseout(function() {
		$(this).css('opacity', '1')
	}).click(function(){
		$('#input_avatar').click();
	})

	$('#input_avatar').change(function(){
		$('#botones').show();
		if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img_perfil').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
	})

	$('#cancelar_imagen').click(function(){
		$('#img_perfil').attr('src', 'img/user.png');
		$('#botones').hide();
	})
});

function registrar_usuario(){
	$('#form_registro').submit(function (e) {
		dataString = $("#form_registro").serialize();
	        $.ajax({
	          type: "POST", //The type of HTTP verb (post, get, etc)
	          url: "control_panel/controlador/tbl_usuarios.php", //The URL from the form element where the AJAX request will be sent
	          data: dataString, //All the data from the form serialized
	          dataType: "json", //The type of response to expect from the server
	          success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
	            if (data.estado=="insertado"){
            	    $.confirm({
					    title: 'Exitoso!',
					    content: 'Usted ha sido registrado satisfactoriamente!',
					    theme: 'supervan', // 'material', 'bootstrap'
					    buttons: {
			                somethingElse: {
					            text: 'Aceptar',
					            btnClass: 'btn-blue',
					            action: function(){
					                location ='login.php';
					            }
					        }
					    }
					});
				    
	            }else if(data.estado=='ce'){
            	    $.alert({
				        title: 'Error!',
				        content: 'Este Email ya esta en uso.',
				        theme: 'modern'
				    });
	            }else if(data.estado=='ue'){
	            	$.alert({
				        title: 'Error!',
				        content: 'Este Usuario ya esta en uso.',
				        theme: 'modern'
				    });
	            }
	          }
	      });
	    e.preventDefault(); //Evitamos que se mande del formulario de forma convencional
      	return false;
	});
}

function login(){
	$('#form_inicio').submit(function (e) {
		$('#respuesta').html('<hr><div class="progress">'+
							  '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>'+
							'</div>');
		dataString = $("#form_inicio").serialize();
	        $.ajax({
	          type: "POST", //The type of HTTP verb (post, get, etc)
	          url: "control_panel/controlador/tbl_usuarios.php", //The URL from the form element where the AJAX request will be sent
	          data: dataString, //All the data from the form serialized
	          dataType: "json", //The type of response to expect from the server
	          success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
	            if (data.estado=="autenticado"){
	            	$('#respuesta').html('<hr><div class="alert alert-info" role="alert" style="border-radius: 0px;">'+
										  '<strong>¡Exitoso!</strong> Accediendo...'+
										'</div>'); 
	            	setTimeout(function(){
	            		location='dashboard.php';
	            	}, 1000)
            	         
	            }else{
	            	$('#respuesta').html('<hr><div class="alert alert-danger" role="alert" style="border-radius: 0px;">'+
										  '<strong>¡No Exitoso!</strong> Datos incorrectos.'+
										'</div>'); 	            }
	          }
	      });
	    e.preventDefault(); //Evitamos que se mande del formulario de forma convencional
      	return false;
	});
}

function salir(){
	$.ajax({
		type: "POST", //The type of HTTP verb (post, get, etc)
		url: "control_panel/controlador/tbl_usuarios.php", //The URL from the form element where the AJAX request will be sent
		data: {"accion": "salir"}, //All the data from the form serialized
		dataType: "json", //The type of response to expect from the server
		success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
			if (data.estado=="hecho"){
				location='index.php';
			}
		}
	});
}

function activar_edicion(){
	
}

function foto_perfil(){
	$('#form_avatar').submit(function (e){
       var data = new FormData(document.getElementById("form_avatar"));
        $.ajax({
            url: "control_panel/controlador/tbl_usuarios.php", //URL destino
            data: data,
            processData: false, //Evitamos que JQuery procese los datos, daría error
            contentType: false, //No especificamos ningún tipo de datos
            type: 'POST',
            dataType: "json",
            success: function (data) {                         
               if(data){
                    $('#botones').hide();
                    $('#img_menu').attr('src', data.imagen);
               }
            }
        });
        e.preventDefault(); //Evitamos que se mande del formulario de forma convencional
        return false;
     });
     return false;
}