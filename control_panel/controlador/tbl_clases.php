<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_calendario.php"); # clase del modelo
	$Obj_tbl_calendario = new tbl_calendario();
	require_once("../modelo/class_tbl_asistencias.php"); # clase del modelo
	$Obj_tbl_asistencias = new tbl_asistencias();
	require_once("../modelo/class_tbl_clases.php"); # clase del modelo
	$Obj_tbl_clases = new tbl_clases($_REQUEST["id_clase"],$_REQUEST["id_curso"],$_REQUEST["fecha_hora"],$_REQUEST["fecha_hora_fin"],$_REQUEST["descripcion"],$_REQUEST["status"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_clases->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_clase"]=$Obj_tbl_clases->insertar();
			if (is_numeric($_REQUEST["id_clase"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_clase"]=$_REQUEST["id_clase"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_clase"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_clase"]=$Obj_tbl_clases->actualizar();
			if (is_numeric($_REQUEST["id_clase"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_clase"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_clases->where=" id_clase in (".$_REQUEST["id_clase"].")";
		$_REQUEST["id_clase"]=$Obj_tbl_clases->eliminar();
			if (is_numeric($_REQUEST["id_clase"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_clase"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_clases->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_clase"];
				$options[$index+1]["text"] =$valor["clase"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "combo_dependiente":
			
			$_SESSION["where"]="";	
			
			$Obj_tbl_clases->where=$_REQUEST["where"];
			
			$combo=$Obj_tbl_clases->listar(true);
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_clase"];
				$options[$index+1]["text"] =$valor["clase"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		case "reservar":

			$hora = date($_REQUEST['hora']);
			$nuevafecha = strtotime ( '+'.intval($_REQUEST['duracion']).' minute' , strtotime ( $hora ) ) ;
			$nuevafecha2 = date ( 'H:i' , $nuevafecha );

			$Obj_tbl_clases = new tbl_clases('', $_REQUEST['id_curso'], $_REQUEST['fecha']." ".$_REQUEST['hora'], $_REQUEST['fecha']." ".$nuevafecha2, $_REQUEST['descripcion'], 2);
			$_REQUEST["id_clase"]=$Obj_tbl_clases->insertar();
			if (is_numeric($_REQUEST["id_clase"]))
			{  
				$Obj_tbl_asistencias = new tbl_asistencias('', $_REQUEST["id_clase"], $_SESSION["session_usuario"]['id_perfil_alumno'], 2);
				$_REQUEST["id_asistencia"]=$Obj_tbl_asistencias->insertar();

				if (is_numeric($_REQUEST["id_asistencia"]))
				{ 
					$Obj_tbl_calendario = new tbl_calendario('', $_REQUEST["id_curso"], $_REQUEST['fecha']." ".$_REQUEST['hora'], $_REQUEST['fecha']." ".$nuevafecha2, $_REQUEST["descripcion"]);
					$_REQUEST["id_calendario"]=$Obj_tbl_calendario->insertar();

					$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
					$retorna["datos"]="";
					$retorna["estado"]="insertado";
					$retorna["id_calendario"]=$_REQUEST["id_calendario"];
				}
				
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_clase"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}

			echo json_encode($retorna);

		break;

		case "listar_reservas":
			$Obj_tbl_clases->campos=" tbl_clases.*, tbl_clases.descripcion as ladescrip ";
			$Obj_tbl_clases->where=" tbl_clases.id_curso=".$_REQUEST['id_curso']." AND (status=1 OR status=2 OR status=3) ";
			
			$combo=$Obj_tbl_clases->listar(true);
			$options=array();
			$datetime = date('Y-m-d H:i:s');
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["materia"]=$_REQUEST["materia"];
				$options[$index+1]["id_clase"]=$valor["id_clase"];
				$options[$index+1]["descripcion"] =$valor["ladescrip"];
				$options[$index+1]["fecha_hora"] =$valor["fecha_hora"];
				$options[$index+1]["fecha_hora_fin"] =$valor["fecha_hora_fin"];
				$options[$index+1]["status"] =$valor["status"];

				$hora = strtotime(date(substr($valor["fecha_hora_fin"], 11)));
				$options[$index+1]["hora_fin"] = date("g:i a", $hora);

				$hora2 = strtotime(date(substr($valor["fecha_hora"], 11)));
				$options[$index+1]["hora_inicio"] = date("g:i a", $hora2);

				$options[$index+1]["fecha_sola"] = substr($valor["fecha_hora"], 0, 10);

				if(intval($valor["status"])==1){
					if($datetime >= date($valor["fecha_hora"]) AND $datetime <= date($valor["fecha_hora_fin"])){
						$options[$index+1]["flag"] = 1;
					}else{
						$options[$index+1]["flag"] = 2;
					}
				}

			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		case "confirmar":
		$Obj_tbl_clases->where=" id_clase=".$_REQUEST["id_clase"]." ";
		if ($_REQUEST['flag']==1) {
			$_REQUEST["id_clase"]=$Obj_tbl_clases->actualizar_campo('status=1');
		}elseif($_REQUEST['flag']==2){
			$_REQUEST["id_clase"]=$Obj_tbl_clases->actualizar_campo('status=3');
		}
			if (is_numeric($_REQUEST["id_clase"]))
			{  
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
	}	
	
	?>
	