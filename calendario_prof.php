<?php session_start();
include("control_panel/inc/config.sistema.php");
require_once("control_panel/modelo/config.modelo.php"); # configuracion del modelo      
require_once("control_panel/modelo/class_tbl_estatus_usuarios.php"); # clase del modelo
$Obj_tbl_estatus_usuarios = new tbl_estatus_usuarios;   
require_once("control_panel/modelo/class_tbl_perfiles.php"); # clase del modelo
$Obj_tbl_perfiles = new tbl_perfiles;   
require_once("control_panel/modelo/class_tbl_regiones.php"); # clase del modelo
$Obj_tbl_regiones = new tbl_regiones;
$_SESSION["where"]="";  
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
   <?php include("control_panel/vista/layouts/lampbert/header.php");?>
    <?php include("control_panel/vista/layouts/lampbert/header_js.php");?>
    <link href='js/calendar/fullcalendar.min.css' rel='stylesheet' />
    <link href='js/calendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    
    <script src='js/calendar/lib/moment.min.js'></script>
    <script src='js/calendar/fullcalendar.min.js'></script>
    <script src='js/calendar/locale/es-us.js'></script>
  
</head>

<body>
    <!-- ***** Header Area  ***** -->
    <?php include("control_panel/vista/layouts/lampbert/header_menu.php");?>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading Area -->
                    <a href="reservar_clase.php" class="pull-right"><i class="fa fa-plus"></i> Reservar</a>
                    <div class="section-heading text-left">
                        <h2 style="font-size: 26px;">Mi Calendario</h2><hr>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12" id="calendar">
                    
                </div>
            </div>
        </div>

    </section>

    
    <!-- ***** Footer Area Start ***** -->
    <?php include("control_panel/vista/layouts/lampbert/footer.php");?>
    <script src="js/reserva.js"></script>
    <script>
    $(document).ready(function() {

    $.ajax({
        type: "POST", 
        url: "control_panel/controlador/tbl_asistencias.php", 
        data: {"accion": "calendario_prof"},
        dataType: "json",
        success: function ( data, statusCode, xhr ) { 
            if (data){
                arreglo = [];
                $.each(data,function(index_data, registros){
                    if(registros.status==2){ fondo = "orange"; estado= 'Pendiente'}else if(registros.status==1){ fondo= '#007bff'; estado='Confirmado'}else if(registros.status==3){ fondo= 'red'; estado='Rechazado'}else if(registros.status==4){ fondo= 'gray'; estado='Caducado'}
                    arreglo.push({title:registros.materia, start: registros.fecha_hora, end: registros.fecha_hora_fin, textColor: '#fff', materia: registros.materia, datos: registros.fecha_hora, color: fondo, label: estado, fin: registros.hora_fin, inicio: registros.hora_inicio, sola: registros.fecha_sola});
                });
            }

            $('#calendar').fullCalendar({
            lang: 'es',
              header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
              },
              defaultDate: '<?php echo date('Y-m-d'); ?>',
              eventLimit: true, // allow "more" link when too many events
              events: arreglo, 
                eventMouseover: function(calEvent) {
                    $(this).css('opacity', '0.5').css('cursor', 'pointer');
                },
                eventMouseout: function(calEvent) {
                    $(this).css('opacity', '1');
                },
                eventClick: function(calEvent){
                    $.alert({
                        title: 'Información',
                        content: 'Materia: '+calEvent.materia+'<br>Datos: '+calEvent.sola+' de '+calEvent.inicio+' a '+calEvent.fin+'<br>Estado: '+calEvent.label,
                        theme: 'light', // 'material', 'bootstrap'
                    });
                }
            });
        }
    });
    

  });
    </script>

</html>
