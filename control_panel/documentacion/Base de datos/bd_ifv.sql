-- phpMyAdmin SQL Dump
-- version 4.2.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 30-06-2015 a las 12:31:29
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `bd_ifv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_clientes`
--

CREATE TABLE IF NOT EXISTS `tbl_clientes` (
`id_cliente` int(11) NOT NULL,
  `id_tipo_cliente` int(11) NOT NULL,
  `rif_cliente` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nombre_cliente` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `direccion_cliente` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telefono_cliente` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `contacto_cliente` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `cargo_cliente` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telefono_contacto` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `correo_cliente` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `codicion_pago` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `tbl_clientes`
--

INSERT INTO `tbl_clientes` (`id_cliente`, `id_tipo_cliente`, `rif_cliente`, `nombre_cliente`, `direccion_cliente`, `telefono_cliente`, `contacto_cliente`, `cargo_cliente`, `telefono_contacto`, `correo_cliente`, `codicion_pago`, `usuario`) VALUES
(1, 2, 'j-45345345', 'Inversiones sdx', 'Av san martin', '32656555959', 'Juan Tovar', 'Director', '65655959959', 'no_tiene@gmail.com', 'dfgfgfdg5345', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_comisiones`
--

CREATE TABLE IF NOT EXISTS `tbl_comisiones` (
`id_comision` int(11) NOT NULL,
  `comision` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `id_cliente` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `tbl_comisiones`
--

INSERT INTO `tbl_comisiones` (`id_comision`, `comision`, `id_cliente`) VALUES
(1, '0.25', 1),
(2, '0.32', 1),
(3, '0.45', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_cotizaciones`
--

CREATE TABLE IF NOT EXISTS `tbl_cotizaciones` (
`id_cotizacion` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_estatu_cotizacion` int(11) NOT NULL,
  `id_iva` int(11) NOT NULL,
  `numero_cotizacion` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_aprobacion` date DEFAULT NULL,
  `fecha_anulacion` date DEFAULT NULL,
  `fecha_emision` date NOT NULL,
  `fecha_vigente` date NOT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `sub_total_gravable` decimal(10,2) NOT NULL,
  `sub_total_exento` decimal(10,2) NOT NULL,
  `monto_iva_cotizado` decimal(10,2) NOT NULL,
  `monto_total` decimal(10,2) NOT NULL,
  `porcentaje_iva` decimal(5,2) NOT NULL,
  `usuario` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Volcado de datos para la tabla `tbl_cotizaciones`
--

INSERT INTO `tbl_cotizaciones` (`id_cotizacion`, `id_cliente`, `id_estatu_cotizacion`, `id_iva`, `numero_cotizacion`, `fecha_aprobacion`, `fecha_anulacion`, `fecha_emision`, `fecha_vigente`, `fecha_entrega`, `sub_total_gravable`, `sub_total_exento`, `monto_iva_cotizado`, `monto_total`, `porcentaje_iva`, `usuario`) VALUES
(10, 1, 3, 2, '201506001', NULL, NULL, '2015-06-14', '2015-06-30', NULL, '5612.00', '3279.00', '154.21', '9958.31', '0.12', 'admin'),
(11, 1, 1, 2, '201506002', '2015-06-18', NULL, '2015-06-14', '2015-05-20', NULL, '2000.97', '0.00', '0.00', '2241.09', '0.12', 'admin'),
(12, 1, 2, 2, '201506003', '2015-06-21', '2015-06-21', '2015-06-15', '2015-06-30', '2015-06-23', '7192.90', '0.00', '0.00', '8056.05', '0.12', 'admin'),
(13, 1, 3, 2, '201506005', NULL, NULL, '2015-06-21', '2015-06-30', NULL, '2629.75', '0.00', '315.57', '2945.32', '0.12', 'admin'),
(14, 1, 3, 2, '201506006', NULL, NULL, '2015-06-21', '2015-06-30', '2015-06-19', '528.90', '0.00', '63.47', '592.37', '0.12', 'admin'),
(22, 1, 3, 2, '201506007', NULL, NULL, '2015-06-21', '2015-06-30', '2015-06-18', '2103.80', '0.00', '252.46', '2356.26', '0.12', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_datos_basicos`
--

CREATE TABLE IF NOT EXISTS `tbl_datos_basicos` (
`id_dato_basico` int(11) NOT NULL,
  `nombre_dato` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `monto_dato` decimal(2,2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tbl_datos_basicos`
--

INSERT INTO `tbl_datos_basicos` (`id_dato_basico`, `nombre_dato`, `monto_dato`) VALUES
(2, 'GASTO VENTA', '0.00'),
(4, 'FACTOR UTILIDAD', '0.90');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_detalles_cotizaciones`
--

CREATE TABLE IF NOT EXISTS `tbl_detalles_cotizaciones` (
`id_detalle_cotizacion` int(11) NOT NULL,
  `id_producto_disponible` int(11) NOT NULL,
  `cantidad_cotizada` int(22) NOT NULL,
  `precio_unidad_cotizada` decimal(10,2) NOT NULL,
  `monto_iva` decimal(10,2) DEFAULT NULL,
  `precio_total` decimal(10,2) NOT NULL,
  `id_cotizacion` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Volcado de datos para la tabla `tbl_detalles_cotizaciones`
--

INSERT INTO `tbl_detalles_cotizaciones` (`id_detalle_cotizacion`, `id_producto_disponible`, `cantidad_cotizada`, `precio_unidad_cotizada`, `monto_iva`, `precio_total`, `id_cotizacion`) VALUES
(13, 7, 45, '105.19', '0.00', '50105.00', 10),
(14, 8, 5, '105.19', '0.00', '50105.00', 10),
(15, 9, 35, '50.00', '0.00', '6550.00', 10),
(16, 10, 30, '50.00', '0.00', '6550.00', 10),
(17, 12, 10, '35.26', '0.00', '1035.00', 10),
(18, 7, 15, '105.19', '0.00', '15105.00', 11),
(19, 12, 12, '35.26', '0.00', '1235.00', 11),
(20, 7, 45, '105.19', '0.00', '4733.55', 12),
(21, 8, 15, '105.19', '0.00', '1577.85', 12),
(30, 12, 25, '35.26', '0.00', '881.50', 12),
(31, 7, 25, '105.19', '0.00', '2629.75', 13),
(32, 12, 15, '35.26', '0.00', '528.90', 14),
(33, 7, 20, '105.19', '0.00', '2103.80', 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estatus_cotizaciones`
--

CREATE TABLE IF NOT EXISTS `tbl_estatus_cotizaciones` (
`id_estatu_cotizacion` int(11) NOT NULL,
  `estatu_cotizacion` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tbl_estatus_cotizaciones`
--

INSERT INTO `tbl_estatus_cotizaciones` (`id_estatu_cotizacion`, `estatu_cotizacion`) VALUES
(1, 'APROBADA'),
(2, 'ANULADA'),
(3, 'PROCESADA'),
(4, 'VENCIDA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estatus_productos`
--

CREATE TABLE IF NOT EXISTS `tbl_estatus_productos` (
`id_estatu_producto` int(11) NOT NULL,
  `estatu_producto` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tbl_estatus_productos`
--

INSERT INTO `tbl_estatus_productos` (`id_estatu_producto`, `estatu_producto`) VALUES
(1, 'ACTIVO'),
(2, 'INACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estatus_usuarios`
--

CREATE TABLE IF NOT EXISTS `tbl_estatus_usuarios` (
`id_estatu_usuario` int(11) NOT NULL,
  `estatu_usuario` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tbl_estatus_usuarios`
--

INSERT INTO `tbl_estatus_usuarios` (`id_estatu_usuario`, `estatu_usuario`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_ivas`
--

CREATE TABLE IF NOT EXISTS `tbl_ivas` (
`id_iva` int(11) NOT NULL,
  `concepto_iva` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `monto_iva` decimal(5,2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tbl_ivas`
--

INSERT INTO `tbl_ivas` (`id_iva`, `concepto_iva`, `monto_iva`) VALUES
(1, 'EXENTO', '0.00'),
(2, 'GRAVABLE', '0.12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_modulos`
--

CREATE TABLE IF NOT EXISTS `tbl_modulos` (
`id_modulo` int(11) NOT NULL,
  `modulo` varchar(80) NOT NULL,
  `descripcion_modulo` text NOT NULL,
  `posicion_modulo` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `tbl_modulos`
--

INSERT INTO `tbl_modulos` (`id_modulo`, `modulo`, `descripcion_modulo`, `posicion_modulo`) VALUES
(1, 'Configuracion del Sistema', 'Configuracion del Sistema', 1),
(2, 'Usuarios', 'permite agregar usuarios al sistema', 1),
(3, 'Ventas', 'permite controlar todo lo relacionado a las ventas', 2),
(4, 'Almacén', 'permite controlar lo referente al almacen inventario o otros', 3),
(5, 'Facturación', 'permite generar cotizaciones y todo tipo de facturas', 4),
(6, 'Administrador', 'permite generar todo lo referente a historicos', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_modulos_perfiles`
--

CREATE TABLE IF NOT EXISTS `tbl_modulos_perfiles` (
`id_modulo_perfil` int(11) NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `tbl_modulos_perfiles`
--

INSERT INTO `tbl_modulos_perfiles` (`id_modulo_perfil`, `id_modulo`, `id_perfil`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_perfiles`
--

CREATE TABLE IF NOT EXISTS `tbl_perfiles` (
`id_perfil` int(11) NOT NULL,
  `perfil` varchar(120) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `tbl_perfiles`
--

INSERT INTO `tbl_perfiles` (`id_perfil`, `perfil`) VALUES
(1, 'Super Administrador del sistema'),
(2, 'Almacen'),
(3, 'Facturador'),
(4, 'Vendedor'),
(5, 'SuperVendedor'),
(6, 'Administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_precios_productos`
--

CREATE TABLE IF NOT EXISTS `tbl_precios_productos` (
`id_precio_producto` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_tipo_cliente` int(11) NOT NULL,
  `fecha_registro_producto` date NOT NULL,
  `precio_producto` decimal(7,2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `tbl_precios_productos`
--

INSERT INTO `tbl_precios_productos` (`id_precio_producto`, `id_producto`, `id_tipo_cliente`, `fecha_registro_producto`, `precio_producto`) VALUES
(9, 1, 1, '2015-06-07', '35.00'),
(10, 1, 2, '2015-06-07', '50.45'),
(11, 1, 3, '2015-06-07', '75.15'),
(12, 1, 4, '2015-06-07', '95.25'),
(13, 1, 5, '2015-06-07', '105.20'),
(14, 2, 1, '2015-06-07', '45.25'),
(15, 2, 2, '2015-06-07', '105.19'),
(16, 2, 3, '2015-06-07', '201.00'),
(17, 2, 4, '2015-06-07', '305.00'),
(18, 2, 5, '2015-06-07', '425.15'),
(19, 3, 1, '2015-06-14', '12.59'),
(20, 3, 2, '2015-06-14', '35.26'),
(21, 3, 3, '2015-06-14', '65.26'),
(22, 3, 4, '2015-06-14', '87.29'),
(23, 3, 5, '2015-06-14', '25.59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_productos`
--

CREATE TABLE IF NOT EXISTS `tbl_productos` (
`id_producto` int(11) NOT NULL,
  `codigo_producto` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nombre_producto` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `minimo_disponible` int(11) NOT NULL,
  `existencia_producto` int(11) NOT NULL,
  `id_estatu_producto` int(11) NOT NULL,
  `id_iva` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `tbl_productos`
--

INSERT INTO `tbl_productos` (`id_producto`, `codigo_producto`, `nombre_producto`, `minimo_disponible`, `existencia_producto`, `id_estatu_producto`, `id_iva`) VALUES
(1, '65595262', 'Harina de Maiz', 10, 90, 1, 1),
(2, '655656', 'Azucar', 10, 71, 1, 2),
(3, 'tr34543', 'leche', 12, 84, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_productos_disponibles`
--

CREATE TABLE IF NOT EXISTS `tbl_productos_disponibles` (
`id_producto_disponible` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `existencia` int(11) NOT NULL,
  `cantidad_entrante` int(11) NOT NULL,
  `lote` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `fecha_registro` date NOT NULL,
  `costo_producto` decimal(7,2) NOT NULL,
  `remanente` int(11) DEFAULT NULL,
  `usuario` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `tbl_productos_disponibles`
--

INSERT INTO `tbl_productos_disponibles` (`id_producto_disponible`, `id_producto`, `id_proveedor`, `existencia`, `cantidad_entrante`, `lote`, `fecha_vencimiento`, `fecha_registro`, `costo_producto`, `remanente`, `usuario`) VALUES
(7, 2, 1, 45, 45, '565fgfdg5', '2015-06-30', '2015-06-14', '2450.00', NULL, 'admin'),
(8, 2, 1, 26, 26, 'gfhtfy4', '2015-07-14', '2015-06-14', '3200.00', NULL, 'admin'),
(9, 1, 1, 35, 35, 'fgh5645', '2015-06-18', '2015-06-14', '1500.00', NULL, 'admin'),
(10, 1, 1, 45, 45, '65r65re', '2015-06-26', '2015-06-14', '4500.00', NULL, 'admin'),
(11, 1, 1, 10, 10, 'f656fgh', '2015-06-30', '2015-06-14', '4500.00', NULL, 'admin'),
(12, 3, 1, 48, 48, 'fhgfh56', '2015-06-16', '2015-06-14', '4503.00', NULL, 'admin'),
(13, 3, 1, 36, 36, 'gfh5g54', '2015-06-30', '2015-06-14', '254.45', NULL, 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_proveedores`
--

CREATE TABLE IF NOT EXISTS `tbl_proveedores` (
`id_proveedor` int(11) NOT NULL,
  `nombre_proveedor` varchar(150) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `tbl_proveedores`
--

INSERT INTO `tbl_proveedores` (`id_proveedor`, `nombre_proveedor`) VALUES
(1, 'PROPIOS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_regiones`
--

CREATE TABLE IF NOT EXISTS `tbl_regiones` (
`id_region` int(11) NOT NULL,
  `region` varchar(250) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tbl_regiones`
--

INSERT INTO `tbl_regiones` (`id_region`, `region`) VALUES
(1, 'Distrito Capital'),
(2, 'Maturin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_sub_modulos`
--

CREATE TABLE IF NOT EXISTS `tbl_sub_modulos` (
`id_sub_modulo` int(11) NOT NULL,
  `sub_modulo` text NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `posicion_sub_modulo` int(11) NOT NULL,
  `descripcion_sub_modulo` text NOT NULL,
  `enlace` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `tbl_sub_modulos`
--

INSERT INTO `tbl_sub_modulos` (`id_sub_modulo`, `sub_modulo`, `id_modulo`, `posicion_sub_modulo`, `descripcion_sub_modulo`, `enlace`) VALUES
(1, 'Perfiles', 1, 1, 'Perfiles', '../tbl_perfiles/formulario.php?accion=insertar'),
(2, 'Modulos', 1, 2, 'Modulos', '../tbl_modulos/formulario.php?accion=insertar'),
(3, 'Modulos Perfiles', 1, 3, 'Modulos Perfiles', '../tbl_modulos_perfiles/formulario.php?accion=insertar'),
(4, 'Sub Modulos', 1, 4, 'sub_modulos', '../tbl_sub_modulos/formulario.php?accion=insertar'),
(5, 'Regiones', 1, 5, 'Regiones', '../tbl_regiones/formulario.php?accion=insertar'),
(6, 'Estatus Usuarios', 1, 6, 'Estatus Usuarios', '../tbl_estatus_usuarios/formulario.php?accion=insertar'),
(7, 'Usuarios', 1, 7, 'Usuarios', '../tbl_usuarios/formulario.php?accion=insertar'),
(8, 'Registro Usuario', 2, 1, 'permite registrar usuarios en el sistema', '../tbl_usuarios/formulario.php?accion=insertar'),
(9, 'Cambio de Clave', 2, 5, 'permite cambiar la clave', '../tbl_usuarios/formulario__1.php?accion=actualizar_clave'),
(10, 'Crear Clientes', 3, 4, 'permite crear los clientes', '../tbl_clientes/formulario.php?accion=insertar'),
(11, 'Crear Productos', 4, 1, 'permite registrar los productos', '../tbl_productos/formulario.php?accion=insertar'),
(12, 'Modificar Productos', 4, 2, 'permite modificar los productos', '../tbl_productos/formulario.php?accion=buscar'),
(13, 'Precios de Productos', 5, 1, 'permite crear los precios a los productos', '../tbl_precios_productos/formulario.php?accion=insertar'),
(14, 'Modificar Clientes', 3, 5, 'Permite modificar los clientes', '../tbl_clientes/formulario.php?accion=buscar'),
(15, 'Panel de Cotizaciones', 3, 1, 'permite controlar las cotizaciones', '../tbl_cotizaciones/formulario_2.php?accion=buscar'),
(16, 'Cotización', 3, 2, 'permite crear cotizaciones', '../tbl_cotizaciones/formulario.php?accion=insertar'),
(17, 'Cotización Vigente ', 3, 3, 'permite ver las cotizaciones vigentes', '../tbl_cotizaciones/listar_1.php'),
(18, 'Control de Administración', 6, 1, 'permite tareas multiples', '../tbl_datos_basicos/formulario.php?accion=actualizar'),
(19, 'Historico de Productos', 6, 2, 'permite ver el historico', '../tbl_productos/formulario_1.php?accion=historico'),
(20, 'Inventario', 4, 3, 'Permite registrar la mercancia que llega al almacen', '../tbl_productos_disponibles/formulario.php?accion=insertar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_tipos_clientes`
--

CREATE TABLE IF NOT EXISTS `tbl_tipos_clientes` (
`id_tipo_cliente` int(11) NOT NULL,
  `tipo_cliente` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `tbl_tipos_clientes`
--

INSERT INTO `tbl_tipos_clientes` (`id_tipo_cliente`, `tipo_cliente`) VALUES
(1, 'Tipo 1'),
(2, 'Tipo 2'),
(3, 'Tipo 3'),
(4, 'Tipo 4'),
(5, 'Tipo 5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuarios`
--

CREATE TABLE IF NOT EXISTS `tbl_usuarios` (
  `cedula` varchar(12) DEFAULT NULL,
  `usuario` varchar(50) NOT NULL,
  `clave` varchar(15) NOT NULL,
  `nombres` varchar(80) NOT NULL,
  `apellidos` varchar(80) NOT NULL,
  `correo_electronico` varchar(80) NOT NULL,
  `id_estatu_usuario` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  `id_region` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`cedula`, `usuario`, `clave`, `nombres`, `apellidos`, `correo_electronico`, `id_estatu_usuario`, `id_perfil`, `id_region`) VALUES
('1', 'admin', 'admin', 'super administrador', 'del sistema', 'super@gmail.com', 1, 1, 1),
('14259898', 'sjuan', '12345', 'Simon Juan', 'Rivas Rojas', 'no_tiene@gmail.com', 1, 4, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_clientes`
--
ALTER TABLE `tbl_clientes`
 ADD PRIMARY KEY (`id_cliente`), ADD KEY `id_tipo_cliente` (`id_tipo_cliente`);

--
-- Indices de la tabla `tbl_comisiones`
--
ALTER TABLE `tbl_comisiones`
 ADD PRIMARY KEY (`id_comision`), ADD KEY `id_cliente` (`id_cliente`);

--
-- Indices de la tabla `tbl_cotizaciones`
--
ALTER TABLE `tbl_cotizaciones`
 ADD PRIMARY KEY (`id_cotizacion`), ADD KEY `id_cliente` (`id_cliente`), ADD KEY `id_estatu_cotizacion` (`id_estatu_cotizacion`), ADD KEY `id_iva` (`id_iva`), ADD KEY `tbl_cotizaciones_usuario_fkey` (`usuario`);

--
-- Indices de la tabla `tbl_datos_basicos`
--
ALTER TABLE `tbl_datos_basicos`
 ADD PRIMARY KEY (`id_dato_basico`);

--
-- Indices de la tabla `tbl_detalles_cotizaciones`
--
ALTER TABLE `tbl_detalles_cotizaciones`
 ADD PRIMARY KEY (`id_detalle_cotizacion`), ADD KEY `id_producto_disponible` (`id_producto_disponible`), ADD KEY `id_cotizacion` (`id_cotizacion`);

--
-- Indices de la tabla `tbl_estatus_cotizaciones`
--
ALTER TABLE `tbl_estatus_cotizaciones`
 ADD PRIMARY KEY (`id_estatu_cotizacion`);

--
-- Indices de la tabla `tbl_estatus_productos`
--
ALTER TABLE `tbl_estatus_productos`
 ADD PRIMARY KEY (`id_estatu_producto`);

--
-- Indices de la tabla `tbl_estatus_usuarios`
--
ALTER TABLE `tbl_estatus_usuarios`
 ADD PRIMARY KEY (`id_estatu_usuario`);

--
-- Indices de la tabla `tbl_ivas`
--
ALTER TABLE `tbl_ivas`
 ADD PRIMARY KEY (`id_iva`);

--
-- Indices de la tabla `tbl_modulos`
--
ALTER TABLE `tbl_modulos`
 ADD PRIMARY KEY (`id_modulo`);

--
-- Indices de la tabla `tbl_modulos_perfiles`
--
ALTER TABLE `tbl_modulos_perfiles`
 ADD PRIMARY KEY (`id_modulo_perfil`), ADD KEY `FK_modulo_id_modulo` (`id_modulo`), ADD KEY `FK_modulo_id_perfil` (`id_perfil`);

--
-- Indices de la tabla `tbl_perfiles`
--
ALTER TABLE `tbl_perfiles`
 ADD PRIMARY KEY (`id_perfil`);

--
-- Indices de la tabla `tbl_precios_productos`
--
ALTER TABLE `tbl_precios_productos`
 ADD PRIMARY KEY (`id_precio_producto`), ADD KEY `id_tipo_cliente` (`id_tipo_cliente`), ADD KEY `id_producto` (`id_producto`);

--
-- Indices de la tabla `tbl_productos`
--
ALTER TABLE `tbl_productos`
 ADD PRIMARY KEY (`id_producto`), ADD KEY `id_estatu_producto` (`id_estatu_producto`), ADD KEY `id_iva` (`id_iva`);

--
-- Indices de la tabla `tbl_productos_disponibles`
--
ALTER TABLE `tbl_productos_disponibles`
 ADD PRIMARY KEY (`id_producto_disponible`), ADD KEY `id_producto` (`id_producto`), ADD KEY `id_proveedor` (`id_proveedor`), ADD KEY `tbl_productos_disponibles_usuario_fkey` (`usuario`);

--
-- Indices de la tabla `tbl_proveedores`
--
ALTER TABLE `tbl_proveedores`
 ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `tbl_regiones`
--
ALTER TABLE `tbl_regiones`
 ADD PRIMARY KEY (`id_region`);

--
-- Indices de la tabla `tbl_sub_modulos`
--
ALTER TABLE `tbl_sub_modulos`
 ADD PRIMARY KEY (`id_sub_modulo`), ADD KEY `fk_tbl_sub_modulos_id_modulo` (`id_modulo`);

--
-- Indices de la tabla `tbl_tipos_clientes`
--
ALTER TABLE `tbl_tipos_clientes`
 ADD PRIMARY KEY (`id_tipo_cliente`);

--
-- Indices de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
 ADD PRIMARY KEY (`usuario`), ADD KEY `fk_id_perfil` (`id_perfil`), ADD KEY `fk_id_region` (`id_region`), ADD KEY `fk_tbl_usuarios_id_estatu_usuario` (`id_estatu_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_clientes`
--
ALTER TABLE `tbl_clientes`
MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tbl_comisiones`
--
ALTER TABLE `tbl_comisiones`
MODIFY `id_comision` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tbl_cotizaciones`
--
ALTER TABLE `tbl_cotizaciones`
MODIFY `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `tbl_datos_basicos`
--
ALTER TABLE `tbl_datos_basicos`
MODIFY `id_dato_basico` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tbl_detalles_cotizaciones`
--
ALTER TABLE `tbl_detalles_cotizaciones`
MODIFY `id_detalle_cotizacion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `tbl_estatus_cotizaciones`
--
ALTER TABLE `tbl_estatus_cotizaciones`
MODIFY `id_estatu_cotizacion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tbl_estatus_productos`
--
ALTER TABLE `tbl_estatus_productos`
MODIFY `id_estatu_producto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tbl_estatus_usuarios`
--
ALTER TABLE `tbl_estatus_usuarios`
MODIFY `id_estatu_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tbl_ivas`
--
ALTER TABLE `tbl_ivas`
MODIFY `id_iva` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tbl_modulos`
--
ALTER TABLE `tbl_modulos`
MODIFY `id_modulo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tbl_modulos_perfiles`
--
ALTER TABLE `tbl_modulos_perfiles`
MODIFY `id_modulo_perfil` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tbl_perfiles`
--
ALTER TABLE `tbl_perfiles`
MODIFY `id_perfil` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tbl_precios_productos`
--
ALTER TABLE `tbl_precios_productos`
MODIFY `id_precio_producto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `tbl_productos`
--
ALTER TABLE `tbl_productos`
MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tbl_productos_disponibles`
--
ALTER TABLE `tbl_productos_disponibles`
MODIFY `id_producto_disponible` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `tbl_proveedores`
--
ALTER TABLE `tbl_proveedores`
MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tbl_regiones`
--
ALTER TABLE `tbl_regiones`
MODIFY `id_region` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tbl_sub_modulos`
--
ALTER TABLE `tbl_sub_modulos`
MODIFY `id_sub_modulo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `tbl_tipos_clientes`
--
ALTER TABLE `tbl_tipos_clientes`
MODIFY `id_tipo_cliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_clientes`
--
ALTER TABLE `tbl_clientes`
ADD CONSTRAINT `fk_tbl_clientes_id_tipo_cliente` FOREIGN KEY (`id_tipo_cliente`) REFERENCES `tbl_tipos_clientes` (`id_tipo_cliente`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_comisiones`
--
ALTER TABLE `tbl_comisiones`
ADD CONSTRAINT `tbl_comisiones_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbl_clientes` (`id_cliente`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_cotizaciones`
--
ALTER TABLE `tbl_cotizaciones`
ADD CONSTRAINT `fk_tbl_cotizaciones_id_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `tbl_clientes` (`id_cliente`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tbl_cotizaciones_id_estatu_cotizacion` FOREIGN KEY (`id_estatu_cotizacion`) REFERENCES `tbl_estatus_cotizaciones` (`id_estatu_cotizacion`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tbl_ivas_id_iva` FOREIGN KEY (`id_iva`) REFERENCES `tbl_ivas` (`id_iva`) ON UPDATE CASCADE,
ADD CONSTRAINT `tbl_cotizaciones_usuario_fkey` FOREIGN KEY (`usuario`) REFERENCES `tbl_usuarios` (`usuario`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_detalles_cotizaciones`
--
ALTER TABLE `tbl_detalles_cotizaciones`
ADD CONSTRAINT `fk_tbl_detalle_cotiz_id_cotizacion` FOREIGN KEY (`id_cotizacion`) REFERENCES `tbl_cotizaciones` (`id_cotizacion`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tbl_produc_dispo_id_producto_disp` FOREIGN KEY (`id_producto_disponible`) REFERENCES `tbl_productos_disponibles` (`id_producto_disponible`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_modulos_perfiles`
--
ALTER TABLE `tbl_modulos_perfiles`
ADD CONSTRAINT `FK_modulo_id_modulo` FOREIGN KEY (`id_modulo`) REFERENCES `tbl_modulos` (`id_modulo`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_modulo_id_perfil` FOREIGN KEY (`id_perfil`) REFERENCES `tbl_perfiles` (`id_perfil`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_precios_productos`
--
ALTER TABLE `tbl_precios_productos`
ADD CONSTRAINT `fk_tbl_precios_tipo_cliente` FOREIGN KEY (`id_tipo_cliente`) REFERENCES `tbl_tipos_clientes` (`id_tipo_cliente`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tbl_produc_id_pro` FOREIGN KEY (`id_producto`) REFERENCES `tbl_productos` (`id_producto`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_productos`
--
ALTER TABLE `tbl_productos`
ADD CONSTRAINT `fk_tbl_estatus_productos_id_estatu_pr` FOREIGN KEY (`id_estatu_producto`) REFERENCES `tbl_estatus_productos` (`id_estatu_producto`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tbl_produc_id_iva` FOREIGN KEY (`id_iva`) REFERENCES `tbl_ivas` (`id_iva`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_productos_disponibles`
--
ALTER TABLE `tbl_productos_disponibles`
ADD CONSTRAINT `fk_tbl_almacen_id_pro` FOREIGN KEY (`id_producto`) REFERENCES `tbl_productos` (`id_producto`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tbl_proveedores_id_prove` FOREIGN KEY (`id_proveedor`) REFERENCES `tbl_proveedores` (`id_proveedor`) ON UPDATE CASCADE,
ADD CONSTRAINT `tbl_productos_disponibles_usuario_fkey` FOREIGN KEY (`usuario`) REFERENCES `tbl_usuarios` (`usuario`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_sub_modulos`
--
ALTER TABLE `tbl_sub_modulos`
ADD CONSTRAINT `fk_tbl_sub_modulos_id_modulo` FOREIGN KEY (`id_modulo`) REFERENCES `tbl_modulos` (`id_modulo`);

--
-- Filtros para la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
ADD CONSTRAINT `fk_id_perfil` FOREIGN KEY (`id_perfil`) REFERENCES `tbl_perfiles` (`id_perfil`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_id_region` FOREIGN KEY (`id_region`) REFERENCES `tbl_regiones` (`id_region`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tbl_usuarios_id_estatu_usuario` FOREIGN KEY (`id_estatu_usuario`) REFERENCES `tbl_estatus_usuarios` (`id_estatu_usuario`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
