<?php session_start();
	
class reportes extends general {

	private $bd;
	public $where;
	private $ObjetSQL;
	public $tabla;
	public $campos=" * ";
	public $propiedades = array();

	public function __construct() {

		$main = main::getInstance();
		$MotorSQL = $main->motor."_sql";
		include_once ("abstraccion/db/{$main->motor}-sql.lang.php");
		$this->ObjetSQL = new $MotorSQL();
		$this->bd = new db();
		}

	public function __set($nombre, $valor) {
		$this->propiedades[$nombre] = $valor;
	}

	public function __get($nombre) {
		return $this->propiedades[$nombre];
	}

        public function __call($metodo, $args) {
       	if ($args) {
       		 $method = array(&$this->bd, $metodo);
       		 $a = call_user_func_array($method, $args);
       	} else {
       		$a = $this->bd->$metodo();
       	}
    	return $a;
	}

	
        

} //fin de la clase reportes
?>