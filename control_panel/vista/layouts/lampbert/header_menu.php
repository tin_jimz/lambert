<!-- Preloader Start -->
<div id="preloader">
    <div class="colorlib-load"></div>
</div>

<!-- ***** Header Area Start ***** -->
    <header class="header_area animated" id="menu_bar">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                
                    <?php if($_SESSION['session_usuario']['usuario']){ ?>
                        <div class="col-12 col-lg-10">
                            <div class="menu_area">
                                <nav class="navbar navbar-expand-lg navbar-light">
                                    <!-- Logo -->
                                    <a class="navbar-brand" href="#" id="inicio_link" style="font-size: 60px;">La.</a>
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                                    <!-- Menu Area -->
                                    <div class="collapse navbar-collapse" id="ca-navbar">
                                    <ul class="navbar-nav ml-auto" id="nav">
                                        <?php if($_SESSION['session_usuario']['id_perfil']==2){ ?>
                                            <li class="nav-item"><a class="nav-link" href="ahora_prof.php">Solicitud de Urgencia</a></li>
                                            <li class="nav-item"><a class="nav-link" href="reservas_prof.php">Reservas de Clases</a></li>                               
                                            <li class="nav-item"><a class="nav-link" href="calendario_prof.php">Mi Calendario</a></li>
                                        <?php }elseif($_SESSION['session_usuario']['id_perfil']==3){ ?>

                                            <li class="nav-item"><a class="nav-link" href="clase_ahora.php">Clase Urgente</a></li>
                                            <li class="nav-item"><a class="nav-link" href="reservar_clase.php">Reservar Clase</a></li>
                                            <li class="nav-item"><a class="nav-link" href="mis_reservas.php">Mis Reservas</a></li>
                                            <li class="nav-item"><a class="nav-link" href="calendario_alumno.php">Mi calendario</a></li>

                                        <?php } ?>
                                    
                                <?php }else{?>  
                                <div class="col-12 col-lg-8">
                            <div class="menu_area">
                                <nav class="navbar navbar-expand-lg navbar-light">
                                    <!-- Logo -->
                                    <a class="navbar-brand" href="#" style="font-size: 60px;">La.</a>
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                                    <!-- Menu Area -->
                                    <div class="collapse navbar-collapse" id="ca-navbar">
                                        <ul class="navbar-nav ml-auto" id="nav">                                                                                            
                                    <li class="nav-item active"><a class="nav-link" href="#home">Inicio</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#about">Información</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#features">Preguntas</a></li>
                                    <!-- <li class="nav-item"><a class="nav-link" href="#screenshot">Screenshot</a></li> -->
                                    <li class="nav-item"><a class="nav-link" href="#pricing">Planes</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#testimonials">Opiniones</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#team">Equipo</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#contact">Contacto</a></li>
                                <?php }?>
                                </ul>
                                <div class="sing-up-button d-lg-none">
                                    <a href="register.php">Registro Gratis</a>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Signup btn -->
                <?php if($_SESSION['session_usuario']['usuario']){ ?>
                <div class="col-12 col-lg-2">
                    <ul class="nav nav-pills">
                    <?php if($_SESSION['session_usuario']['id_perfil']==2){ ?>
                    <li class="dropdown" style="color: white;">
                        <span style="position: absolute;left: 23%;top: 0%;background-color: green;padding: 1px 7px;font-size: 12px;border-radius: 10px; display: none;" id="notificacion"></span>
                        <a href="#" title="" style="color: white;" data-toggle="dropdown"><i class="fa fa-bell" style="margin-top: 12px; margin-left: -10px;"></i></a>

                        <ul class="dropdown-menu">
                            
                        </ul>
                    </li>
                    <?php } ?>
                      <li class="nav-item dropdown" style="right: -30%;">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white;"><i class="fa fa-user-circle-o fa-lg"></i> <?php echo $_SESSION['session_usuario']['nombres']; ?></a>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="editar_perfil.php">Mi Perfil</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="#" onclick="salir();">Salir</a>
                        </div>
                      </li>
                    </ul>
                </div>
                <?php }else{ ?>
                <div class="col-12 col-lg-2">
                    <div class="sing-up-button d-none d-lg-block">
                        <a href="login.php">Iniciar Sesión</a>
                    </div>
                </div>
                <div class="col-12 col-lg-2">
                    <div class="sing-up-button d-none d-lg-block">
                        <a href="register.php">Registro Gratis</a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </header>