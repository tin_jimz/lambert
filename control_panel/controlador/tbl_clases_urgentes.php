<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_clases_urgentes.php"); # clase del modelo
	$Obj_tbl_clases_urgentes = new tbl_clases_urgentes($_REQUEST["id_clase_urgente"],$_REQUEST["id_curso"],$_REQUEST["id_perfil_alumno"],$_REQUEST["fecha_hora_inicio"],$_REQUEST["fecha_hora_fin"],$_REQUEST["status"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_clases_urgentes->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_clase_urgente"]=$Obj_tbl_clases_urgentes->insertar();
			if (is_numeric($_REQUEST["id_clase_urgente"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_clase_urgente"]=$_REQUEST["id_clase_urgente"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_clase_urgente"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_clase_urgente"]=$Obj_tbl_clases_urgentes->actualizar();
			if (is_numeric($_REQUEST["id_clase_urgente"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_clase_urgente"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_clases_urgentes->where=" id_clase_urgente in (".$_REQUEST["id_clase_urgente"].")";
		$_REQUEST["id_clase_urgente"]=$Obj_tbl_clases_urgentes->eliminar();
			if (is_numeric($_REQUEST["id_clase_urgente"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_clase_urgente"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_clases_urgentes->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_clase_urgente"];
				$options[$index+1]["text"] =$valor["perfil"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "combo_dependiente":
			
			$_SESSION["where"]="";	
			
			$Obj_tbl_clases_urgentes->where=$_REQUEST["where"];
			
			$combo=$Obj_tbl_clases_urgentes->listar(true);
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_clase_urgente"];
				$options[$index+1]["text"] =$valor["perfil"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		case "completar":
			$minutos = (60/1*floatval($_REQUEST['duracion']))+60;
			$hora = date($_REQUEST['hora']);
			$nuevafecha = strtotime ( '+'.intval($minutos).' minute' , strtotime ( $fecha ) ) ;
			$nuevafecha2 = date ( 'H:i:s' , $nuevafecha );

			$Obj_tbl_clases_urgentes = new tbl_clases_urgentes('', $_REQUEST['id_curso'], $_SESSION['session_usuario']['id_perfil_alumno'], date('Y-m-d '.$_REQUEST['hora']), date('Y-m-d '.$nuevafecha2), 2);
			$_REQUEST["id_clase_urgente"]=$Obj_tbl_clases_urgentes->insertar();
			if (is_numeric($_REQUEST["id_clase_urgente"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_clase_urgente"]=$_REQUEST["id_clase_urgente"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_clase_urgente"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}

			echo json_encode($retorna);

		break;

		case "aceptar_urgencia":
		$Obj_tbl_clases_urgentes->where=" id_clase_urgente=".$_REQUEST['id_clase_urgente']." ";
		$act = $Obj_tbl_clases_urgentes->actualizar_campo('status=1');
			if (is_numeric($act))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="actualizado";
			$retorna["id_clase_urgente"]=$_REQUEST["id_clase_urgente"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_clase_urgente"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;

		case "rechazar_urgencia":
		$Obj_tbl_clases_urgentes->where=" id_clase_urgente=".$_REQUEST['id_clase_urgente']." ";
		$act = $Obj_tbl_clases_urgentes->actualizar_campo('status=3');
			if (is_numeric($act))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="actualizado";
			$retorna["id_clase_urgente"]=$_REQUEST["id_clase_urgente"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_clase_urgente"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
	}	
	
	?>
	