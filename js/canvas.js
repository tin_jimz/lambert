var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
context.lineWidth = 5;
var down = false;

canvas.addEventListener('mousemove', draw);

canvas.addEventListener('mousedown', function(){
	down = true;
	context.beginPath();
	context.moveTo(xPos, yPos);
	canvas.addEventListener('mousemove', draw);
})

canvas.addEventListener('mouseup', function(){ down = false ; });

function draw(e){
	xPos = e.clientX - canvas.offsetLeft;
	yPos = e.clientY - canvas.offsetTop;

	if(down == true){
		context.lineTo(xPos, yPos);
		context.stroke();
	}
}

function cambiarColor(color){
	context.strokeStyle = color;
}

function limpiarCanvas(){
	context.clearRect(0, 0, canvas.width, canvas.height);
}

function tamanioPincel(tam){
	context.lineWidth = tam;
}

function fondoCanvas(color){
	context.fillStyle = color;
	context.fillRect(0, 0, canvas.width, canvas.height);
}

function cambiarEstilo(estilo){
	context.lineCap = estilo;
}

function triggerClick(){
	$('#file').click();
}

document.getElementById('file').addEventListener('change', function(e){
	var temp = URL.createObjectURL(e.target.files[0]);
	var image = new Image();
	image.src = temp;

	image.addEventListener('load', function(){
		context.drawImage(image, 0, 0);
	});
})