<?php session_start();
include("control_panel/inc/config.sistema.php");
require_once("control_panel/modelo/config.modelo.php"); # configuracion del modelo      
require_once("control_panel/modelo/class_tbl_estatus_usuarios.php"); # clase del modelo
$Obj_tbl_estatus_usuarios = new tbl_estatus_usuarios;   
require_once("control_panel/modelo/class_tbl_perfiles.php"); # clase del modelo
$Obj_tbl_perfiles = new tbl_perfiles;   
require_once("control_panel/modelo/class_tbl_regiones.php"); # clase del modelo
$Obj_tbl_regiones = new tbl_regiones;
$_SESSION["where"]="";  
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
   <?php include("control_panel/vista/layouts/lampbert/header.php");?>
    <?php include("control_panel/vista/layouts/lampbert/header_js.php");?>
  
</head>

<body>
    <!-- ***** Header Area  ***** -->
    <?php include("control_panel/vista/layouts/lampbert/header_menu.php");?>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading Area -->
                    <a href="calendario_prof.php" class="pull-right"><i class="fa fa-calendar"></i> Ver en Calendario</a>
                    <div class="section-heading text-left">
                        <h2 style="font-size: 26px;">Solicitudes de Reservación</h2><hr>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-10">
                    <div class="tabbable-panel">
                        <div class="tabbable-line tabs-below">
                            
                            <ul class="nav nav-tabs">
                                <li class="active" id="link1">
                                    <a href="#res1" data-toggle="tab" title="Reservas Aceptadas">Aceptadas</a>
                                </li>
                                <li id="link2">
                                    <a href="#res2" data-toggle="tab" title="Reservas Pendientes">Pendientes</a>
                                </li>
                                <li id="link3">
                                    <a href="#res3" data-toggle="tab" title="Reservas Caducadas">Caducadas</a>
                                </li>
                                <li id="link4">
                                    <a href="#res4" data-toggle="tab" title="Reservas Rechazadas">Rechazadas</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <ul class="event-list tab-pane active" id="res1">
                                    
                                </ul>
                                <ul class="event-list tab-pane" id="res2">
                                   
                                </ul>
                                <ul class="event-list tab-pane" id="res3">
                                    
                                </ul>
                                <ul class="event-list tab-pane" id="res4">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <ul class="event-list" id="mis_reservas_prof">
                        
                    </ul>
                </div>
            </div>
        </div>

    </section>


    
    <!-- ***** Footer Area Start ***** -->
    <?php include("control_panel/vista/layouts/lampbert/footer.php");?>
    <script src="js/reserva.js"></script>
    <script>
        $(mis_reservas_prof);
    </script>
    <style type="text/css">
        @import url("http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,400italic");
    </style>

</html>
