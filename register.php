<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Lambert - Registro</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Core Stylesheet -->
    <link href="style.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

</head>

<body>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area animated">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-12 col-lg-8">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="index.php">La.</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <li class="nav-item active"><a class="nav-link" href="#home">Home</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#about">About</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#features">Features</a></li>
                                    <!-- <li class="nav-item"><a class="nav-link" href="#screenshot">Screenshot</a></li> -->
                                    <li class="nav-item"><a class="nav-link" href="#pricing">Planes</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#testimonials">Opiniones</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#team">Equipo</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#contact">Contacto</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
    <section class="wellcome_area clearfix" id="home">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="card" style="width: 30%; margin-left: 35%; top: -5%;">
                  <div class="card-block">
                    <h4 class="card-title">REGISTRO DE USUARIOS</h4>
                    <hr>
                    <form id="form_registro" method="POST">
                    <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-primary active" id="lab1" style="width: 145px;">
                        <input type="radio" name="id_perfil" id="radio1" autocomplete="off"  checked=""> Profesor
                      </label>
                      <label class="btn btn-info" id="lab2" style="width: 145px;">
                        <input type="radio" name="id_perfil" id="radio2" autocomplete="off" > Alumno
                      </label>
                    </div>
                    <hr>
                    <div id="profesor">
                        <div class="form-group">
                            <label for="" id="titulo">Perfil</label>
                            <select type="text" name="id_perfil" id="id_perfil" class="form-control" required readonly>
                                <option value="2">Profesor</option>
                            </select>
                        </div>
                        <hr>
                    </div>
                        
                      <div class="form-group">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                            <input type="text" name="usuario" class="form-control" placeholder="Usuario" required>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon">@</div>
                            <input type="email" name="correo_electronico" class="form-control" placeholder="Email" required>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                            <input type="password" name="clave" class="form-control" placeholder="Contraseña" required>
                          </div>
                      </div>
                      <hr>
                      <input type="hidden" name="accion" value="insertar">
                      <input type="hidden" name="id_estatu_usuario" value="1">
                      <input type="hidden" name="id_region" value="1">
                      <input type="hidden" name="online" value="2">
                      <input type="hidden" name="imagen_perfil" value="img/user.png">
                    <button type="submit" class="btn btn-primary btn-md btn-block" style="border-radius: 0px;">Registrar</button>
                    <button type="reset" class="btn btn-default btn-md btn-block" style="border-radius: 0px;">Limpiar</button>
                    <p style="font-size: 12px; text-align: center;">Al registrarte aceptas nuestros <a target="_blank" href="#" style="text-decoration: underline;color: #8977de;">Terminos y Condiciones</a></p>
                    </form>
                    
                  </div>
                </div>
            </div>
        </div>
        
    </section>
    <!-- ***** Wellcome Area End ***** -->

    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-social-icon text-center section_padding_70 clearfix">
        <!-- footer logo -->
        <div class="footer-text">
            <h2>Ca.</h2>
        </div>
        <!-- social icon-->
        <div class="footer-social-icon">
            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="#"><i class="active fa fa-twitter" aria-hidden="true"></i></a>
            <a href="#"> <i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
        </div>
        <div class="footer-menu">
            <nav>
                <ul>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Terms &amp; Conditions</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </nav>
        </div>
        <!-- Foooter Text-->
        <div class="copyright-text">
            <!-- ***** Removing this text is now allowed! This template is licensed under CC BY 3.0 ***** -->
            <p>Copyright ©2017 Ca. Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
        </div>
    </footer>

    <!-- ***** Footer Area Start ***** -->

    <!-- Jquery-2.2.4 JS -->
    <script src="js/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap-4 Beta JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins JS -->
    <script src="js/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="js/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="js/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="js/active.js"></script>
    <!-- Funciones JS -->
    <script src="js/usuario.js"></script>
    <script>
        $(function(){registrar_usuario();});
    </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

</html>
