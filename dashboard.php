<?php session_start();
include("control_panel/inc/config.sistema.php");
require_once("control_panel/modelo/config.modelo.php"); # configuracion del modelo      
require_once("control_panel/modelo/class_tbl_estatus_usuarios.php"); # clase del modelo
$Obj_tbl_estatus_usuarios = new tbl_estatus_usuarios;   
require_once("control_panel/modelo/class_tbl_perfiles.php"); # clase del modelo
$Obj_tbl_perfiles = new tbl_perfiles;   
require_once("control_panel/modelo/class_tbl_regiones.php"); # clase del modelo
$Obj_tbl_regiones = new tbl_regiones;
$_SESSION["where"]="";  
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
   <?php include("control_panel/vista/layouts/lampbert/header.php");?>
    <?php include("control_panel/vista/layouts/lampbert/header_js.php");?>

</head>

<body>

    <!-- ***** Header Area  ***** -->
    <?php include("control_panel/vista/layouts/lampbert/header_menu.php"); ?>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading Area -->
                    <div class="section-heading text-left">
                        <h2 style="font-size: 22px;">Bienvenido(a) <?php echo $_SESSION["session_usuario"]['nombres']; ?></h2><hr>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <!-- Single Special Area -->
                    
                        <!-- <div class="col-sm-6 text-center">
                           <label class="label label-success">Line Chart</label>
                          <div id="line-chart"></div>
                        </div> -->
                        <?php if($_SESSION['session_usuario']['id_perfil']==2){ ?>
                        <div  class="col-sm-6">
                           <h5 >Clases Mensuales</h5><hr>
                          <div id="bar-chart" ></div>
                        </div>
                        <?php } ?>
                        
                        <!-- <div class="col-sm-6 col-sm-offset-3 text-center">
                           <label class="label label-success">Pie Chart</label>
                          <div id="pie-chart" ></div>
                        </div> -->

            </div>
        </div>

    </section>

    
    <!-- ***** Footer Area Start ***** -->
    <?php include("control_panel/vista/layouts/lampbert/footer.php");?>
    <link rel="stylesheet" href="css/morris.css">
    <script src="js/raphael-min.js"></script>
    <script src="js/morris.min.js"></script>
    <script src="js/dasboard.js"></script>
    <script>
        $(function(){
            $('#inicio_link').click(function(){ location='dashboard.php' })
            $('#menu_bar').css('background-color', '#6654e7')
        });
        /*Morris.Donut({
          element: 'pie-chart',
          data: [
            {label: "Friends", value: 30},
            {label: "Allies", value: 15},
            {label: "Enemies", value: 45},
            {label: "Neutral", value: 10}
          ]
        });*/
    </script>
    <?php if($_SESSION['session_usuario']['id_perfil']==2){ ?>
      <script>
         clases_mensual();
      </script>
    <?php } ?>
    <style type="text/css" media="screen">
        #area-chart,
        #line-chart,
        #bar-chart,
        #stacked,
        #pie-chart{
          min-height: 250px;
          background-color: #F6F6F6;
          margin: 2px;
          box-shadow: 1px 1px 3px 0px #a8a8a8;
        }
        
    </style>

</html>
