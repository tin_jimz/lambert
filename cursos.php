<?php session_start();
include("control_panel/inc/config.sistema.php");
require_once("control_panel/modelo/config.modelo.php"); # configuracion del modelo      
require_once("control_panel/modelo/class_tbl_estatus_usuarios.php"); # clase del modelo
$Obj_tbl_estatus_usuarios = new tbl_estatus_usuarios;   
require_once("control_panel/modelo/class_tbl_perfiles.php"); # clase del modelo
$Obj_tbl_perfiles = new tbl_perfiles;   
require_once("control_panel/modelo/class_tbl_regiones.php"); # clase del modelo
$Obj_tbl_regiones = new tbl_regiones;
$_SESSION["where"]="";  
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
   <?php include("control_panel/vista/layouts/lampbert/header.php");?>
    <?php include("control_panel/vista/layouts/lampbert/header_js.php");?>
</head>

<body>
    <!-- ***** Header Area  ***** -->
    <?php include("control_panel/vista/layouts/lampbert/header_menu.php");?>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="mis_cursos.php" class="pull-right"><i class="fa fa-list"></i> Mis Cursos</a>
                    <!-- Section Heading Area -->
                    <div class="section-heading text-left">
                        <h2 style="font-size: 22px;"><i class="fa fa-plus"></i> Nuevo Curso</h2>
                    </div>
                    
                    <hr>
                </div>
            </div>
            
            <div class="row">
                <!-- Single Special Area -->
                <div class="col-md-12">
                    <div class="alert alert-info" role="alert">
                      <i class="fa fa-info-circle fa-lg"></i> Registre los cursos en los cuales quiere participar, una vez registrado debe esperar a que el profesor acepte su petición para confirmar su entrada. 
                    </div>
                </div>
                <div class="col-md-12">
                        
                        <div class="row form-group product-chooser">
                            <div class="col-md-8">
                            <h6>Seleccione el Nivel</h6>
                            <hr>
                        </div>
                        <div class="col-md-4">
                            <h6>Seleccione el Grado</h6>
                            <hr>
                        </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">

                                <div alt="1" id="product1" class="product-chooser-item" style="margin-left: 0; border-radius: 0;">
                                    <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                        <span class="title"><i class="fa fa-pencil"></i> PRIMARIA</span>
                                        <span class="description">Selecccione esta opción si desea registrar un curso de nivel primario, así encontrarás contenido acorde a las necesidades pertinentes.</span>
                                        <input type="radio" name="nivel" id="sel1"><hr>
                                        <button type="button" id="btn1" class="btn btn-primary btn-sm" style="border-radius: 0px;">Seleccionar <i class="fa fa-check"></i></button>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div alt="2" id="product2" class="product-chooser-item" style="margin-left: 0; border-radius: 0;">
                                    <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                        <span class="title"><i class="fa fa-book"></i> SECUNDARIA</span>
                                        <span class="description">Seleccciona esta opción si desea registrar un curso de nivel secundario, así encontrarás contenido más avanzado, de acuerdo a su nivel.</span>
                                        <input type="radio" name="nivel" id="sel2"><hr>
                                        <button type="button" id="btn2" class="btn btn-primary btn-sm" style="border-radius: 0px;">Seleccionar <i class="fa fa-check"></i></button>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="wrap-select">
                                    <div id="dd" class="wrapper-dropdown-3" style="left: -21%;" >
                                        <span id="selector">Seleccione...</span>
                                    </div>
                                </div>
                            <input type="hidden" name="grado" id="grado">
                            </div>
                    </div>
                
                </div>
                <div class="col-md-8">
                    <h6>Seleccione Curso</h6><hr>
                    
                    <div class="notice notice-lg notice-info" id="notice">
                        <strong><a href="#" title="Ver Cursos">Realice una petición</a></strong> <span>para obtener resultados.</span>
                        <button disabled type="button" class="btn btn-circle btn-primary pull-right" style="border-radius: 30px; padding: 10px;" title="Ver Cursos"><i class="fa fa-arrows-alt"></i></button>
                    </div>

                </div>
                
            </div>
        </div>

    </section>

    
    <!-- ***** Footer Area Start ***** -->
    <?php include("control_panel/vista/layouts/lampbert/footer.php");?>

 <script src="js/cursos.js"></script>

</html>
