<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_sub_modulos.php"); # clase del modelo
	$Obj_tbl_sub_modulos = new tbl_sub_modulos($_REQUEST["id_sub_modulo"],$_REQUEST["sub_modulo"],$_REQUEST["id_modulo"],$_REQUEST["posicion_sub_modulo"],$_REQUEST["descripcion_sub_modulo"],$_REQUEST["enlace"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_sub_modulos->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_sub_modulo"]=$Obj_tbl_sub_modulos->insertar();
			if (is_numeric($_REQUEST["id_sub_modulo"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_sub_modulo"]=$_REQUEST["id_sub_modulo"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_sub_modulo"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_sub_modulo"]=$Obj_tbl_sub_modulos->actualizar();
			if (is_numeric($_REQUEST["id_sub_modulo"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_sub_modulo"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_sub_modulos->where=" id_sub_modulo in (".$_REQUEST["id_sub_modulo"].")";
		$_REQUEST["id_sub_modulo"]=$Obj_tbl_sub_modulos->eliminar();
			if (is_numeric($_REQUEST["id_sub_modulo"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_sub_modulo"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_sub_modulos->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_sub_modulo"];
				$options[$index+1]["text"] =$valor["sub_modulo"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "combo_dependiente":
			
			$_SESSION["where"]="";	
			
			$Obj_tbl_sub_modulos->where=$_REQUEST["where"];
			
			$combo=$Obj_tbl_sub_modulos->listar(true);
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_sub_modulo"];
				$options[$index+1]["text"] =$valor["sub_modulo"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
	}	
	
	?>
	