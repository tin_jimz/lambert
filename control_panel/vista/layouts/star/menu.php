<ul class="nav">
		
<?php include(RUTA_SISTEMA."/vista/menu/menu.php"); ?>		
		
<?php foreach($menu as $modulo => $submenus) { ?>
<li class="nav-item">
		<a class="nav-link" href="#">
            <img src="../../images/icons/1.png" alt="">
            <span class="menu-title">
		 	<?php echo $modulo; ?>
		 </span>
		</a>
	<div class="collapse" id="sample-pages">
        <ul class="nav flex-column sub-menu">
	        <?php foreach($submenus as $texto => $link) { ?>
	        <li class="nav-item">
	            <a class="nav-link" href="<?php echo $link; ?>">
	            	<?php echo $texto; ?>
	            </a>
	        </li>
	        <?php } ?>
		</ul>
	</div>
</li>
</ul>

<?php } ?>			
<li class="nav-item">
	<a href="<?php echo "/".$sistema ."/" ?>" class="nav-link" data-toggle="dropdown">Salir del Sistema</a>
</li>			
</ul>