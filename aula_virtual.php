<?php session_start();
include("control_panel/inc/config.sistema.php");
require_once("control_panel/modelo/config.modelo.php"); # configuracion del modelo      
require_once("control_panel/modelo/class_tbl_estatus_usuarios.php"); # clase del modelo
$Obj_tbl_estatus_usuarios = new tbl_estatus_usuarios;   
require_once("control_panel/modelo/class_tbl_perfiles.php"); # clase del modelo
$Obj_tbl_perfiles = new tbl_perfiles;   
require_once("control_panel/modelo/class_tbl_regiones.php"); # clase del modelo
$Obj_tbl_regiones = new tbl_regiones;
$_SESSION["where"]="";  

if($_SESSION['session_usuario']['id_perfil']==3){
    require_once("control_panel/modelo/class_tbl_clases.php"); # clase del modelo
    $Obj_tbl_clases = new tbl_clases;
    $Obj_tbl_clases->where=" id_clase=".$_REQUEST['id_clase'];
    $clase = $Obj_tbl_clases->listar(true);

    require_once("control_panel/modelo/class_tbl_perfil_profesor.php"); # clase del modelo
    $Obj_tbl_perfil_profesor = new tbl_perfil_profesor;
    $Obj_tbl_perfil_profesor->where=" id_perfil_profesor=".$clase[0]['id_perfil_profesor'];
    $prof = $Obj_tbl_perfil_profesor->listar(true);

    $destinatario = $prof[0]['usuario'];
}elseif($_SESSION['session_usuario']['id_perfil']==2){
    require_once("control_panel/modelo/class_tbl_asistencias.php"); # clase del modelo
    $Obj_tbl_asistencias = new tbl_asistencias;
    $Obj_tbl_asistencias->where=" tbl_asistencias.id_clase=".$_REQUEST['id_clase'];
    $asistencia = $Obj_tbl_asistencias->listar(true);
    $destinatario = $asistencia[0]['usuario'];
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
   <?php include("control_panel/vista/layouts/lampbert/header.php");?>
    <?php include("control_panel/vista/layouts/lampbert/header_js.php");?>
    <link rel="stylesheet" type="text/css" href="css/draw.css">
  
</head>

<body>
    <!-- ***** Header Area  ***** -->
    <?php include("control_panel/vista/layouts/lampbert/header_menu.php");?>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading Area -->
                    <div class="section-heading text-left">
                        <h2 style="font-size: 22px;">Aula Virtual</h2><hr>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="tabbable-panel" style="width: 80%;">
                    <div class="tabbable-line tabs-below">
                        
                        <ul class="nav nav-tabs">
                            <li class="active" id="link1">
                                <a href="#res1" data-toggle="tab" title="Reservas Aceptadas">Pizarra</a>
                            </li>
                            <li id="link2">
                                <a href="#res2" data-toggle="tab" title="Reservas Pendientes">Biblioteca</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="res1">
                                <div class="col-md-10">
                                    <div class="top-bar">
                                        <button id="save-btn" class="btn btn-sm btn-primary"><i class="fa fa-save"></i></button>
                                        <button id="undo-btn" class="btn btn-sm btn-success"><i class="fa fa-rotate-left"></i></button>
                                        <button id="clear-btn" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                        <input type="color" id="color-picker">
                                        <input type="range" class="form-control" id="brush-size" min="1" max="50" value="10" style="width: 20%; display: inline;">
                                        <span id="tamanio"></span>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <canvas id="draw" class="col-md-12"></canvas>
                                </div>
                            </div>
                            <div class="tab-pane" id="res2">
                                <form id="form_archivo" method="post" enctype="multipart/form-data">
                                <div class="col-md-8">
                                    <div class="input-group image-preview">
                                        
                                            <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                            <span class="input-group-btn">
                                                <!-- image-preview-clear button -->
                                                <button type="button" class="btn btn-secondary image-preview-clear" style="display:none;">
                                                    <span class="fa fa-trash"></span> Limpiar
                                                </button>
                                                <button type="submit" id="enviar" class="btn btn-success image-preview-send" style="display:none;">
                                                    <span class="fa fa-upload"></span> Cargar
                                                </button>
                                                <!-- image-preview-input -->
                                                <div class="btn btn-primary image-preview-input">
                                                    <span class="fa fa-folder-open"></span>
                                                    <span class="image-preview-input-title">Buscar Archivo</span>
                                                    <input type="file" accept="image/png, image/jpeg" name="input-file-preview"/> <!-- rename it -->
                                                    <input type="hidden" name="id_clase" value="<?php echo $_REQUEST['id_clase']; ?>">
                                                    <input type="hidden" name="accion" value="subir_archivo">
                                                </div>
                                            </span>
                                    </div>
                                </div>
                                </form>
                                <hr>
                                <div class="container-fluid">
                                    <div class="row" id="imagen-mult" style="box-shadow: 0px 0px 5px 0em gray; padding: 15px; height: 520px; overflow-y: auto;"></div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <div class="container">

        <div id="style_switcher" class="">
            <a class="switcher_toggle" onclick="Colapse_Switcher();">
                <i id="icono" class="fa fa-comments"></i>
            </a>
            <div>
                <div class="section-heading text-left">
                    <h2 style="font-size: 18px;">Chat</h2><hr style="margin-top: 0.5em; margin-bottom: 0;">
                </div>
                
                <ul id="ul"></ul>
                <hr>
                    <div class="msj-rta macro" style="margin:auto">                        
                        <div class="text text-r" style="background:whitesmoke !important">
                            <input class="mytext" placeholder="Type a message"/>
                        </div> 
                    </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="remitente" value="<?php echo $_SESSION['session_usuario']['usuario']; ?>">
    <input type="hidden" id="destinatario" value="<?php echo $destinatario; ?>">
    <input type="hidden" id="id_clase" value="<?php echo $_REQUEST['id_clase']; ?>">
    
    
    <!-- ***** Footer Area Start ***** -->
    <?php include("control_panel/vista/layouts/lampbert/footer.php");?>
    <script src="js/aula_virtual.js"></script>
    <script src="js/draw.js"></script>
    <script>
        $(chat('<?php echo $_REQUEST['id_clase'] ?>'), cargar_archivo(), list_img());

        /*setInterval(function(){
            chat('<?php echo $_REQUEST['id_clase'] ?>')
        }, 5000)*/
    </script>

    <style>
        .mytext{
            border:0;padding:5px;background:whitesmoke;
        }
        .text{
            width:75%;display:flex;flex-direction:column;
        }
        .text > p:first-of-type{
            width:100%;margin-top:0;margin-bottom:auto;line-height: 13px;font-size: 12px;
        }
        .text > p:last-of-type{
            width:100%;text-align:right;color:silver;margin-bottom:-7px;margin-top:auto;
        }
        .text-l{
            float:left;padding-right:10px;
        }        
        .text-r{
            float:right;padding-left:10px;
        }
        .avatar{
            display:flex;
            justify-content:center;
            align-items:center;
            width:25%;
            float:left;
            padding-right:10px;
        }
        .macro{
            margin-top:5px;width:85%;border-radius:5px;padding:5px;display:flex;
        }
        .msj-rta{
            float:right;background:whitesmoke;
        }
        .msj{
            float:left;background:white;
        }
        .frame{
            background:#e0e0de;
            height:450px;
            overflow:hidden;
            padding:0;
        }
        .frame > div:last-of-type{
            position:absolute;bottom:5px;width:100%;display:flex;
        }
        #ul {
            width:100%;
            list-style-type: none;
            padding:10px;
            bottom:32px;
            display:flex;
            flex-direction: column;
            height: 380px;
            overflow: auto;

        }
        .msj:before{
            width: 0;
            height: 0;
            content:"";
            top:-5px;
            left:-14px;
            position:relative;
            border-style: solid;
            border-width: 0 13px 13px 0;
            border-color: transparent #ffffff transparent transparent;            
        }
        .msj-rta:after{
            width: 0;
            height: 0;
            content:"";
            top:-5px;
            left:14px;
            position:relative;
            border-style: solid;
            border-width: 13px 13px 0 0;
            border-color: whitesmoke transparent transparent transparent;           
        }  
        input:focus{
            outline: none;
        }        
        ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
            color: #d4d4d4;
        }
        ::-moz-placeholder { /* Firefox 19+ */
            color: #d4d4d4;
        }
        :-ms-input-placeholder { /* IE 10+ */
            color: #d4d4d4;
        }
        :-moz-placeholder { /* Firefox 18- */
            color: #d4d4d4;
        } 

        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;    
            color: #333;
            background-color: #fff;
            border-color: #ccc;    
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        } 

        

</html>
