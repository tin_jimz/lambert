<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_usuarios.php"); # clase del modelo
	$Obj_tbl_usuarios = new tbl_usuarios();
	require_once("../modelo/class_tbl_clases_urgentes.php"); # clase del modelo
	$Obj_tbl_clases_urgentes = new tbl_clases_urgentes();
	require_once("../modelo/class_tbl_clases.php"); # clase del modelo
	$Obj_tbl_clases = new tbl_clases();
	require_once("../modelo/class_tbl_cursos_estudiantes.php"); # clase del modelo
	$Obj_tbl_cursos_estudiantes = new tbl_cursos_estudiantes();
	require_once("../modelo/class_tbl_cursos.php"); # clase del modelo
	$Obj_tbl_cursos = new tbl_cursos($_REQUEST["id_curso"],$_REQUEST["id_perfil_profesor"],$_REQUEST["tema"],$_REQUEST["descripcion"],$_REQUEST["fecha_inicio"],$_REQUEST["fecha_fin"],$_REQUEST["grado"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_cursos->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_curso"]=$Obj_tbl_cursos->insertar();
			if (is_numeric($_REQUEST["id_curso"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_curso"]=$_REQUEST["id_curso"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_curso"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_curso"]=$Obj_tbl_cursos->actualizar();
			if (is_numeric($_REQUEST["id_curso"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_curso"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_cursos->where=" id_curso in (".$_REQUEST["id_curso"].")";
		$_REQUEST["id_curso"]=$Obj_tbl_cursos->eliminar();
			if (is_numeric($_REQUEST["id_curso"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_curso"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_cursos->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_curso"];
				$options[$index+1]["text"] =$valor["curso"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "combo_dependiente":
			
			$_SESSION["where"]="";	
			
			$Obj_tbl_cursos->where=$_REQUEST["where"];
			
			$combo=$Obj_tbl_cursos->listar(true);
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_curso"];
				$options[$index+1]["text"] =$valor["curso"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		case 'contar':
			$Obj_tbl_cursos->where= " grado=".$_REQUEST['grado']." ";
			
			$combo=$Obj_tbl_cursos->listar(true);

			$cuenta = count($combo);
			
			$retorna['cuenta']=$cuenta;

			echo json_encode($retorna);
		break;

		case "buscar_cursos":
			$Obj_tbl_cursos->where= " grado=".$_REQUEST['grado']." ";
			
			$combo=$Obj_tbl_cursos->listar(true);
			$options=array();
			
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["id_curso"]=$valor["id_curso"];
				$options[$index+1]["tema"] =$valor["tema"];
				$options[$index+1]["descripcion"] =$valor["descripcion"];
				$options[$index+1]["fecha_inicio"] =$valor["fecha_inicio"];
				$options[$index+1]["fecha_fin"] =$valor["fecha_fin"];
				$options[$index+1]["usuario"] =$valor["usuario"];

				$Obj_tbl_usuarios->where=" tbl_usuarios.usuario='".$valor["usuario"]."' ";
				$user = $Obj_tbl_usuarios->listar(true);
				$options[$index+1]["nombres"] =$user[0]["nombres"];
				$options[$index+1]["apellidos"] =$user[0]["apellidos"];
				
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		case "solicitudes":
			$Obj_tbl_cursos->where= " tbl_perfil_profesor.id_perfil_profesor=".$_SESSION['session_usuario']['id_perfil_profesor']." ";
			$combo=$Obj_tbl_cursos->listar(true);
			$options=array();
			
			foreach($combo as $index => $valor)
			{
				$Obj_tbl_cursos_estudiantes->where= " tbl_cursos_estudiantes.id_curso=".$valor["id_curso"]." AND status=2 ";
				$cursos=$Obj_tbl_cursos_estudiantes->listar(true);

				if ($cursos[0]['status']) {
					$options[$index+1]["fecha_registro"] =$cursos[0]["fecha_registro"];
					$options[$index+1]["status"] =$cursos[0]["status"];
					$options[$index+1]["id_curso_estudiante"] =$cursos[0]["id_curso_estudiante"];

					$options[$index+1]["id_curso"]=$valor["id_curso"];
					$options[$index+1]["tema"] =$valor["tema"];
					$options[$index+1]["descripcion"] =$valor["descripcion"];
					$options[$index+1]["fecha_inicio"] =$valor["fecha_inicio"];
					$options[$index+1]["fecha_fin"] =$valor["fecha_fin"];
				}
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		case 'listar_materias':
			$Obj_tbl_cursos->where= " grado=".$_REQUEST['grado']." GROUP BY id_materia ";
			
			$combo=$Obj_tbl_cursos->listar(true);
			$options=array();
			
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["id_curso"]=$valor["id_curso"];
				$options[$index+1]["descripcion"] =$valor["descripcion"];
				$options[$index+1]["fecha_inicio"] =$valor["fecha_inicio"];
				$options[$index+1]["fecha_fin"] =$valor["fecha_fin"];
				$options[$index+1]["id_materia"] =$valor["id_materia"];
				$options[$index+1]["materia"] =$valor["materia"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
		break;

		case 'selec_prof':
			$Obj_tbl_cursos->where= " grado=".$_REQUEST['grado']." AND tbl_cursos.id_materia=".$_REQUEST['id_materia']." GROUP BY tbl_perfil_profesor.usuario ";
			
			$combo=$Obj_tbl_cursos->listar(true);
			$options=array();
			
			foreach($combo as $index => $valor)
			{
				$Obj_tbl_usuarios->where=" tbl_usuarios.usuario='".$valor["usuario"]."' AND online=1 ";
				$user = $Obj_tbl_usuarios->listar(true);
				if(count($user)>0){
					$options[$index+1]["id_perfil_profesor"] =$valor["id_perfil_profesor"];
					$options[$index+1]["nombres"] =$user[0]["nombres"];
					$options[$index+1]["apellidos"] =$user[0]["apellidos"];
					$options[$index+1]["id_curso"]=$valor["id_curso"];
				}
			}
			
			$retorna=$options;
			echo json_encode($retorna);
		break;

		case 'contar_prof':
			$Obj_tbl_cursos->where= " grado=".$_REQUEST['grado']." GROUP BY tbl_perfil_profesor.usuario ";
			$combo=$Obj_tbl_cursos->listar(true);
			$options=array();
			$contador=0;

			foreach($combo as $index => $valor)
			{
				$Obj_tbl_usuarios->where=" tbl_usuarios.usuario='".$valor["usuario"]."' AND online=1 ";
				$user = $Obj_tbl_usuarios->listar(true);
				if (count($user)>0) {
					$options[$index+1]["nombres"] =$user[0]["nombres"];
					$options[$index+1]["apellidos"] =$user[0]["apellidos"];
					$contador++;
				}
			}

			$options['cuenta']=$contador;
			
			$retorna=$options;
			echo json_encode($retorna);
		break;

		case "urgencia":
			$Obj_tbl_cursos->where= " tbl_cursos.id_perfil_profesor=".$_SESSION['session_usuario']['id_perfil_profesor']." ";
			$combo=$Obj_tbl_cursos->listar(true);
			$options=array();
			
			foreach($combo as $index => $valor)
			{
				$Obj_tbl_clases_urgentes->where= " tbl_clases_urgentes.id_curso=".$valor["id_curso"]." AND status=2 ";
				$urgente=$Obj_tbl_clases_urgentes->listar(true);

				if ($urgente[0]['status']) {
					$options[$index+1]["id_curso"]=$valor["id_curso"];
					$options[$index+1]["materia"] =$valor["materia"];

					$options[$index+1]["id_clase_urgente"] =$urgente[0]["id_clase_urgente"];
					$options[$index+1]["id_perfil_alumno"] =$urgente[0]["id_perfil_alumno"];
					$options[$index+1]["fecha_hora_inicio"] =$urgente[0]["fecha_hora_inicio"];
					$options[$index+1]["fecha_hora_fin"] =$urgente[0]["fecha_hora_fin"];

					$Obj_tbl_usuarios->where=" tbl_usuarios.usuario='".$urgente[0]["usuario"]."' ";
					$user = $Obj_tbl_usuarios->listar(true);
					$options[$index+1]["nombres"] =$user[0]["nombres"];
					$options[$index+1]["apellidos"] =$user[0]["apellidos"];
				}
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		case "notificacion":
			$Obj_tbl_cursos->where= " tbl_cursos.id_perfil_profesor=".$_SESSION['session_usuario']['id_perfil_profesor']." ";
			$combo=$Obj_tbl_cursos->listar(true);
			$options=array();
			$contador=0;
			foreach($combo as $index => $valor)
			{
				$Obj_tbl_clases_urgentes->where= " tbl_clases_urgentes.id_curso=".$valor["id_curso"]." AND status=2 ";
				$urgente=$Obj_tbl_clases_urgentes->listar(true);

				if ($urgente[0]['status']) {
					$contador++;
					$options[$index+1]["id_curso"]=$valor["id_curso"];
					$options[$index+1]["materia"] =$valor["materia"];

					$options[$index+1]["id_clase_urgente"] =$urgente[0]["id_clase_urgente"];
					$options[$index+1]["id_perfil_alumno"] =$urgente[0]["id_perfil_alumno"];
					$options[$index+1]["fecha_hora_inicio"] =$urgente[0]["fecha_hora_inicio"];
					$options[$index+1]["fecha_hora_fin"] =$urgente[0]["fecha_hora_fin"];

					$Obj_tbl_usuarios->where=" tbl_usuarios.usuario='".$urgente[0]["usuario"]."' ";
					$user = $Obj_tbl_usuarios->listar(true);
					$options[$index+1]["nombres"] =$user[0]["nombres"];
					$options[$index+1]["apellidos"] =$user[0]["apellidos"];
				}
			}


			$options["contador"] =$contador;
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		case 'selec_prof2':
			$Obj_tbl_cursos->where= " grado=".$_REQUEST['grado']." AND tbl_cursos.id_materia=".$_REQUEST['id_materia']." GROUP BY tbl_perfil_profesor.usuario ";
			
			$combo=$Obj_tbl_cursos->listar(true);
			$options=array();
			
			foreach($combo as $index => $valor)
			{
				$Obj_tbl_usuarios->where=" tbl_usuarios.usuario='".$valor["usuario"]."' ";
				$user = $Obj_tbl_usuarios->listar(true);
				if(count($user)>0){
					$options[$index+1]["id_perfil_profesor"] =$valor["id_perfil_profesor"];
					$options[$index+1]["nombres"] =$user[0]["nombres"];
					$options[$index+1]["apellidos"] =$user[0]["apellidos"];
					$options[$index+1]["id_curso"]=$valor["id_curso"];
				}
			}
			
			$retorna=$options;
			echo json_encode($retorna);
		break;

		case 'mis_reservas_prof':
			$Obj_tbl_cursos->where= " tbl_cursos.id_perfil_profesor=".$_SESSION['session_usuario']['id_perfil_profesor']." ";
			
			$combo=$Obj_tbl_cursos->listar(true);
			$options=array();
			
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["id_curso"] =$valor["id_curso"];
				$options[$index+1]["materia"] =$valor["materia"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
		break;

	}	
	
	?>
	