<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_chats.php"); # clase del modelo
	$Obj_tbl_chats = new tbl_chats($_REQUEST["id_mensaje"],$_REQUEST["id_clase"],$_REQUEST["remitente"],$_REQUEST["destinatario"],$_REQUEST["mensaje"],$_REQUEST["fecha_hora"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_chats->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_mensaje"]=$Obj_tbl_chats->insertar();
			if (is_numeric($_REQUEST["id_mensaje"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_mensaje"]=$_REQUEST["id_mensaje"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_mensaje"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_mensaje"]=$Obj_tbl_chats->actualizar();
			if (is_numeric($_REQUEST["id_mensaje"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_mensaje"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_chats->where=" id_mensaje in (".$_REQUEST["id_mensaje"].")";
		$_REQUEST["id_mensaje"]=$Obj_tbl_chats->eliminar();
			if (is_numeric($_REQUEST["id_mensaje"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_mensaje"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_chats->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_mensaje"];
				$options[$index+1]["text"] =$valor["archivo"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "chat":
			$Obj_tbl_chats->where=" tbl_chats.id_clase=".$_REQUEST['id_clase']." ";
			
			$combo=$Obj_tbl_chats->listar(true);
			$options=array();
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["id_mensaje"]=$valor["id_mensaje"];
				$options[$index+1]["remitente"]=$valor["remitente"];
				$options[$index+1]["destinatario"]=$valor["destinatario"];
				$options[$index+1]["mensaje"]=$valor["mensaje"];
				$options[$index+1]["fecha_hora"]=date($valor["fecha_hora"]);

			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
	}	
	
	?>
	