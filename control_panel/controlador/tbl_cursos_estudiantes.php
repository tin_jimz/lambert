<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_perfil_profesor.php"); # clase del modelo
	$Obj_tbl_perfil_profesor = new tbl_perfil_profesor();
	require_once("../modelo/class_tbl_usuarios.php"); # clase del modelo
	$Obj_tbl_usuarios = new tbl_usuarios();
	require_once("../modelo/class_tbl_cursos_estudiantes.php"); # clase del modelo
	$Obj_tbl_cursos_estudiantes = new tbl_cursos_estudiantes($_REQUEST["id_curso_estudiante"],$_REQUEST["id_perfil_alumno"],$_REQUEST["id_curso"],$_REQUEST["fecha_registro"],$_REQUEST["status"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_cursos_estudiantes->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_curso_estudiante"]=$Obj_tbl_cursos_estudiantes->insertar();
			if (is_numeric($_REQUEST["id_curso_estudiante"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_curso_estudiante"]=$_REQUEST["id_curso_estudiante"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_curso_estudiante"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_curso_estudiante"]=$Obj_tbl_cursos_estudiantes->actualizar();
			if (is_numeric($_REQUEST["id_curso_estudiante"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_curso_estudiante"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_cursos_estudiantes->where=" id_curso_estudiante in (".$_REQUEST["id_curso_estudiante"].")";
		$_REQUEST["id_curso_estudiante"]=$Obj_tbl_cursos_estudiantes->eliminar();
			if (is_numeric($_REQUEST["id_curso_estudiante"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_curso_estudiante"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_cursos_estudiantes->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_curso_estudiante"];
				$options[$index+1]["text"] =$valor["curso_estudiante"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "combo_dependiente":
			
			$_SESSION["where"]="";	
			
			$Obj_tbl_cursos_estudiantes->where=$_REQUEST["where"];
			
			$combo=$Obj_tbl_cursos_estudiantes->listar(true);
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_curso_estudiante"];
				$options[$index+1]["text"] =$valor["curso_estudiante"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		case "registrar_curso":
		$Obj_tbl_cursos_estudiantes = new tbl_cursos_estudiantes('',$_SESSION['session_usuario']["id_perfil_alumno"],$_REQUEST["id_curso"],date('Y-m-d'),2);
		$_REQUEST["id_curso_estudiante"]=$Obj_tbl_cursos_estudiantes->insertar();
			if (is_numeric($_REQUEST["id_curso_estudiante"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_curso_estudiante"]=$_REQUEST["id_curso_estudiante"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_curso_estudiante"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;

		case "mis_cursos":
			$Obj_tbl_cursos_estudiantes->where= " tbl_cursos_estudiantes.id_perfil_alumno=".$_SESSION['session_usuario']['id_perfil_alumno']." ORDER BY fecha_registro ";
			
			$combo=$Obj_tbl_cursos_estudiantes->listar(true);
			$options=array();
			
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["id_curso_estudiante"]=$valor["id_curso_estudiante"];
				$options[$index+1]["fecha_registro"] =$valor["fecha_registro"];
				$options[$index+1]["status"] =$valor["status"];

				$options[$index+1]["tema"] =$valor["tema"];
				$options[$index+1]["descripcion"] =$valor["descripcion"];
				$options[$index+1]["fecha_inicio"] =$valor["fecha_inicio"];
				$options[$index+1]["fecha_fin"] =$valor["fecha_fin"];

				$Obj_tbl_perfil_profesor->where=" tbl_perfil_profesor.id_perfil_profesor='".$valor["id_perfil_profesor"]."' ";
				$prof = $Obj_tbl_perfil_profesor->valor_campo('usuario');

				$Obj_tbl_usuarios->where=" tbl_usuarios.usuario='".$prof."' ";
				$user = $Obj_tbl_usuarios->listar(true);
				$options[$index+1]["nombres"] =$user[0]["nombres"];
				$options[$index+1]["apellidos"] =$user[0]["apellidos"];
				
				
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;

		case "aceptar_solicitud":
		$Obj_tbl_cursos_estudiantes->where=" id_curso_estudiante=".$_REQUEST['id_solicitud']." ";
		$act = $Obj_tbl_cursos_estudiantes->actualizar_campo('status=1');
			if (is_numeric($act))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="actualizado";
			$retorna["id_curso_estudiante"]=$_REQUEST["id_curso_estudiante"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_curso_estudiante"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;

		case "rechazar_solicitud":
		$Obj_tbl_cursos_estudiantes->where=" id_curso_estudiante=".$_REQUEST['id_solicitud']." ";
		$act = $Obj_tbl_cursos_estudiantes->actualizar_campo('status=3');
			if (is_numeric($act))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="actualizado";
			$retorna["id_curso_estudiante"]=$_REQUEST["id_curso_estudiante"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_curso_estudiante"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
	}	
	
	?>
	