<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_modulos_perfiles.php"); # clase del modelo
	$Obj_tbl_modulos_perfiles = new tbl_modulos_perfiles($_REQUEST["id_modulo_perfil"],$_REQUEST["id_modulo"],$_REQUEST["id_perfil"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_modulos_perfiles->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_modulo_perfil"]=$Obj_tbl_modulos_perfiles->insertar();
			if (is_numeric($_REQUEST["id_modulo_perfil"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_modulo_perfil"]=$_REQUEST["id_modulo_perfil"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_modulo_perfil"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_modulo_perfil"]=$Obj_tbl_modulos_perfiles->actualizar();
			if (is_numeric($_REQUEST["id_modulo_perfil"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_modulo_perfil"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_modulos_perfiles->where=" id_modulo_perfil in (".$_REQUEST["id_modulo_perfil"].")";
		$_REQUEST["id_modulo_perfil"]=$Obj_tbl_modulos_perfiles->eliminar();
			if (is_numeric($_REQUEST["id_modulo_perfil"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_modulo_perfil"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_modulos_perfiles->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_modulo_perfil"];
				$options[$index+1]["text"] =$valor["modulo_perfil"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "combo_dependiente":
			
			$_SESSION["where"]="";	
			
			$Obj_tbl_modulos_perfiles->where=$_REQUEST["where"];
			
			$combo=$Obj_tbl_modulos_perfiles->listar(true);
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_modulo_perfil"];
				$options[$index+1]["text"] =$valor["modulo_perfil"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
	}	
	
	?>
	