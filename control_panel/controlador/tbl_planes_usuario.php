<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_planes_usuarios.php"); # clase del modelo
	$Obj_tbl_planes_usuarios = new tbl_planes_usuarios($_REQUEST["id_plan_usuario"],$_REQUEST["id_plan"],$_REQUEST["usuario"],$_REQUEST["fecha_inicio"],$_REQUEST["fecha_fin"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_planes_usuarios->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
			$_REQUEST["id_plan_usuario"]=$Obj_tbl_planes_usuarios->insertar();
			if (is_numeric($_REQUEST["id_plan_usuario"]))
			{  
			$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
			$retorna["datos"]="";
			$retorna["estado"]="insertado";
			$retorna["id_plan_usuario"]=$_REQUEST["id_plan_usuario"];
			}
			else
			{ 
			$retorna["mensaje"]=$_REQUEST["id_plan_usuario"];//"NO se agrego el registro a la Base de Datos";
			$retorna["datos"]="";
			$retorna["estado"]="false";
			}
			echo json_encode($retorna);

		break;
		
		case "actualizar":
			$_REQUEST["id_plan_usuario"]=$Obj_tbl_planes_usuarios->actualizar();
			if (is_numeric($_REQUEST["id_plan_usuario"]))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$_REQUEST["id_plan_usuario"];//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_planes_usuarios->where=" id_plan_usuario in (".$_REQUEST["id_plan_usuario"].")";
		$_REQUEST["id_plan_usuario"]=$Obj_tbl_planes_usuarios->eliminar();
			if (is_numeric($_REQUEST["id_plan_usuario"]))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$_REQUEST["id_plan_usuario"];
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_planes_usuarios->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_plan_usuario"];
				$options[$index+1]["text"] =$valor["nombre"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "combo_dependiente":
			
			$_SESSION["where"]="";	
			
			$Obj_tbl_planes_usuarios->where=$_REQUEST["where"];
			
			$combo=$Obj_tbl_planes_usuarios->listar(true);
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["id_plan_usuario"];
				$options[$index+1]["text"] =$valor["plan_usuario"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
	}	
	
	?>
	