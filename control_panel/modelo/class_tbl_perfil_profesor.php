<?php session_start();
	
/**
 * Clase tbl_perfil_profesor extendida de class general <br>
 * con un metodo __construct que solicita los valores de los campos de la tabla de base de datos
 * NOTE: requiere PHP version 5 o la ultima
 * @package lambert
 * @author framework semilla
 * @author programador: 
 * @copyright 2014 
 * @version estable
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 *
 * @method Objet __construct()
 * @method void __set($nombre, $valor)
 * @method string __get($nombre)
 * @version 1.0
 */
 
class tbl_perfil_profesor extends general {
/**
* Objeto de Conexión y Gestión de la Base de Datos implementando Abstracción con la librería ADOdb
* @var Objetc
* @access public
*/
	public $bd;
/**
* Variable que indica en la construcción del SQL los campos necesarios, por defecto (*) todos
* @var string
* @access public
*/
	public $campos="*";
/**
* Variable que condiciona en la construcción del SQL, por defecto iguala la clave primaria al valor de la variable de construcción del Objeto.
* @var string
* @access public
*/
	public $where;
	private $ObjetSQL;
	private $tabla;
	private $propiedades = array();

/**
* función que se ejecuta al momento de instanciar o crear el Objeto
 * que la misma tiene el nombre de los campos de la base de datos, todos por defecto en NULL
 * esto con la finalidad de crear objetos vacíos
 * $main toma la instancia con el Patrón Singleton de las variables de conexión de base de datos <br>
 * $MotorSQL toma el motor de trabajo (PostgreSQL o MySQL) <br>
 * $this->ObjetSQL contiene todas las instrucciones SQL necesarias para todos las clases <br>
 * $this->bd toma una instancia con el Patrón Singleton <br>
 * $this->propiedades tiene un índice que es la relación de los campos de la base de datos, mediante el Patron ORM <br>
 * $this->where por defecto iguala la PK de la tabla al valor de la variable correspondiente a la PK de la clase <br>
 * $this->tabla es el nombre de la tabla con su schema <br>
 * 
* @access public
*/
	public function __construct($id_perfil_profesor=NULL,$dni_profesor=NULL,$direccion_profesor=NULL,$nac_profesor=NULL,$telefono_profesor=NULL,$email_profesor=NULL,$usuario=NULL) {

		$main = main::getInstance();
		$MotorSQL = $main->motor."_sql";
		include_once(MODELO."abstraccion".DS."db".DS."{$main->motor}-sql.lang.php");
		$this->ObjetSQL = new $MotorSQL();
		//$this->bd = new db();
		  $this->bd = db::getInstance();

		$this->propiedades["id_perfil_profesor"]=$id_perfil_profesor;
		$this->propiedades["dni_profesor"]=$dni_profesor;
		$this->propiedades["direccion_profesor"]=$direccion_profesor;
		$this->propiedades["nac_profesor"]=$nac_profesor;
		$this->propiedades["telefono_profesor"]=$telefono_profesor;
		$this->propiedades["email_profesor"]=$email_profesor;
		$this->propiedades["usuario"]=$usuario;
		$this->where=" id_perfil_profesor = ".$this->bd->qstr($this->propiedades["id_perfil_profesor"]);
		$this->tabla="tbl_perfil_profesor";
		}
/**
* Función de métodos mágicos de PHP (php magic methods) <br>
 * con la finalidad de asignarles valores a los índice / valor existentes en $this->propiedades<br>
 * por el contrario si no existe crea un nuevo índice en $this->propiedades y le asigna un valor <br>
 * esto con la finalidad de tener un Objeto mas dinámico. <br>
 * Parámetros: $nombre, $valor <br>
 * 
* @param $nombre índice existentes en $this->propiedades
* @param $valor valor existentes en $this->propiedades
* @property string $this->propiedades[$nombre];
* @access public
*/
	public function __set($nombre, $valor) {
		$this->propiedades[$nombre] = $valor;
	}
	
/**
* Función de métodos mágicos de PHP (php magic methods) <br>
 * Parámetros: $nombre <br>
 * Retorna: el valor del índice de la variable $this->propiedades <br>
 * si no existe retorna NULL o vacío <br>
 * 
* @param $nombre índice de la variable $this->propiedades
* @return string valor del índice de la variable $this->propiedades
* @property string $this->propiedades[$nombre];
* @access public
*/

	public function __get($nombre) {
		return $this->propiedades[$nombre];
	}

    public function __call($metodo, $args) {
       	if ($args) {
       		 $method = array(&$this->bd, $metodo);
       		 $a = call_user_func_array($method, $args);
       	} else {
       		$a = $this->bd->$metodo();
       	}
    	return $a;
	}
/**
* Función para insertar en la base de datos <br>
 * Parámetros: la variable $this->propiedades <br>
 * Retorna: integer según la clave primaria si la inserción es correcta <br>
 * en caso contrario un String de Error <br>
 * 
* @return integer según la clave primaria si la inserción es correcta en caso contrario un String de Error
* @param string $this->propiedades del Objeto Creado según los campos de la tabla
*/

	public function insertar(){
		try {
			$record=$this->AutoExecute($this->tabla,$this->propiedades,'INSERT');
			$retorno=$this->Insert_ID();
		} catch (exception $e) {
			$retorno=$this->ErrorMsg();
		}
	return $retorno;
	}

/**
* Función para actualizar en la base de datos <br>
 * Parámetros: la variable $this->propiedades y $this->wher <br>
 * Retorna: integer de la clave primaria si la actualización es correcta <br>
 * en caso contrario un String de Error <br>
 * 
* @return integer de la clave primaria si la actualizaron es correcta en caso contrario un String de Error
* @param string $this->propiedades del Objeto Creado según los campos de la tabla
*/
	public function actualizar(){
		try {
			$record=$this->AutoExecute($this->tabla,$this->propiedades,'UPDATE',$this->where);
			if ($record){
				$retorno=$this->id_perfil_profesor;
			}
			else{
				$retorno=false;
			}
		} catch (exception $e) {
			$retorno=$this->ErrorMsg();
		}
	return $retorno;
	}

/**
* Función para eliminar en la base de datos <br>
 * Parámetros: la variable $this->where <br>
 * Retorna: integer (1) si la eliminación es correcta <br>
 * en caso contrario un String de Error <br>
 * 
* @return integer(1) si la eliminación es correcta, en caso contrario un String de Error
* @param string $this->where del Objeto Creado según los campos de la tabla
*/
	public function eliminar(){
	
		$ObjetSQL=&$this->ObjetSQL;
		
		$sql.=sprintf($ObjetSQL->delete,$this->tabla);
		
		$sql.=sprintf($ObjetSQL->where,$this->where);

		try {	
			$record=$this->Execute($sql);
			if ($record){
				$retorno=1;
			}
			else{
				$retorno=false;
			}
		} catch (exception $e) {
			$retorno=$this->ErrorMsg();
		}	
	return $retorno;
	}
        
        public function actualizar_campo($set){
	
		$ObjetSQL=&$this->ObjetSQL;
		
		$sql.=sprintf($ObjetSQL->update,$this->tabla,$set);
		
		$sql.=sprintf($ObjetSQL->where,$this->where);

		try {	
			$record=$this->Execute($sql);
			if ($record){
				$retorno=1;
			}
			else{
				$retorno=false;
			}
		} catch (exception $e) {
			$retorno=$this->ErrorMsg();
		}	
            return $retorno;
	}

/**
* Función para consultar en la base de datos <br>
 * Parámetros: $campos con la finalidad de que utilice la $this->where <br>
 * Parámetros: $this->where del Objeto Creado según los campos de la tabla
 * Retorna: los registros de la consulta si es correcta <br>
 * en caso contrario un String de Error <br>
 * 
* @return Array con los datos de la consulta, en caso contrario un String de Error
* @param boolean $campos con la finalidad de que utilice la $this->where
* @param string $this->where del Objeto Creado según los campos de la tabla
*/

	public function listar($campos=false){

		$ObjetSQL=&$this->ObjetSQL;

		$this->tabla_l=$this->tabla;
		$this->tabla_l.=" join tbl_usuarios using(usuario)";
		$sql.=sprintf($ObjetSQL->select,$this->campos,$this->tabla_l);

		if ($campos){
			$sql.=sprintf($ObjetSQL->where,$this->where);
		}
		$sql.=$this->v_limit;
		//echo $sql; exit;
		$record = $this->Execute($sql);

		if ($record){
			$retorno=$record->GetRows();
		}
		else{
			$retorno=false;
		}

		return $retorno;
	}



/**
* Función para determinar el total de los registros de la tabla <br>
 * Parámetros: la variable \$this->where <br>
 * Retorna: integer, total_registros si la ejecución es correcta <br>
 * en caso contrario un String de Error <br>
 * <b>Descripción:</b><br>
 * $ObjetSQL=&$this->ObjetSQL; hace un enlace a la variable <br>
 * $this->campos_t=" count(*) as total_registros "; crea un índice en la variable $this->propiedades, llamado campos_t y le asigna el valor [count(*) as total_registros] <br>
 * $sql=sprintf($ObjetSQL->select,$this->campos_t,$this->tabla);  a la variable $sql le escribe en un formato pasándole los valores $ObjetSQL->select,$this->campos_t,$this->tabla <br>
 * if($this->where) si tiene valor la variable <br>
 * $sql.=sprintf($ObjetSQL->where,$this->where);  a la variable $sql le escribe en un formato pasándole los valores $this->where <br>
 * 
 * 
* @return integer, total_registros si la ejecución es correcta, en caso contrario un String de Error
* @param string \$this->where del Objeto Creado según los campos de la tabla
*/

	public function total_registros(){
	
		$ObjetSQL=&$this->ObjetSQL;
		
		$this->campos_t=" count(*) as total_registros ";
		
		$sql=sprintf($ObjetSQL->select,$this->campos_t,$this->tabla);
		
		if($this->where){
		$sql.=sprintf($ObjetSQL->where,$this->where);
		}
		
		//$sql="select count(*) as total_registros from ".$this->tabla." where ".$this->where; 					
		//sprintf($ObjetSQL->delete,$this->tabla);
		
		//$sql.=sprintf($ObjetSQL->where,$this->where);

		try {	
			$record = $this->Execute($sql);

			if ($record){
				$retorno=$record->GetRows();
				$retorno=$retorno[0]["total_registros"];
				
			}
			else{
				$retorno=false;
			}
		} catch (exception $e) {
			$retorno=$this->ErrorMsg();
		}	
	return $retorno;
	}
	

/**
* Función para paginar las consultas de los registros de la tabla <br>
 * Parámetros: integer  donde se define el inicio de la paginación <br>
 * Parámetros: integer  donde se define el fin de la paginación <br>
 * Retorna: Retorna: array, de la consulta de la función $this->listar, si la ejecución es correcta <br>
 * en caso contrario un String de Error <br>
 * 
 * 
* @return array de la función $this->listar
* @param integer $inicio donde se define el inicio de la paginación
* @param integer $fin donde se define el fin de la paginación 
*/	

	public function limit($inicio,$fin){

		$ObjetSQL=&$this->ObjetSQL;
		
		if ($_SESSION["where"]!="") {$sql=true;} else {$sql=false;}

		$this->v_limit=sprintf($ObjetSQL->limit,$inicio,$fin);
		
		//echo $sql; exit;
		
		$retorno = $this->listar($sql);

		return $retorno;
	}

/**
* Función para hacer una búsqueda en la consulta de los registros de la tabla
 * 
 * 
* @return array, de la consulta de la funcion $this->listar, si la ejecución es correcta,  en caso contrario un String de Error
* @param booleam Array para evaluar en conjunto para crear la búsqueda
* @param session $_SESSION["where"] si ya esta creado no la crea nuevamente
*/  

    public function buscar($campos=true){

		if (($campos) && ($_SESSION["where"]=="")) {
			$no_buscar = array("id_usuario","usuario","computadora","fecha_hora_sis");
			foreach ($this->propiedades as $key => $valor)
			{
				if(($valor) && !(in_array($key, $no_buscar)))
				{
				
					if (substr_count($key,"id_"))
					{
					$where.="$key in ($valor) AND ";
					}
					else
					{
					$where.="$key LIKE'%$valor%' AND ";
					}
					
				}
			}
			$where.="false";
			$where=str_replace(" AND false","",$where);
			$this->where=$where;
			$_SESSION["where"]=$this->where;
		}
		else
		{
			$this->where=$_SESSION["where"];
		}

		//echo $this->where; exit;

		$retorno = $this->listar(true);
		return $retorno;
	}
/**
* Función que retorna las <option> de los registros de la tabla
 * 
 * 
* @return string html para se interpretado en las etiquetas <select>
* @param integer  para determinar si el valor es igual al pk de la tabla para colocarlo como selected<br>
*/ 
	//public function ver_opciones_tbl_perfil_profesor($ver=NULL,$where=false)
	public function ver_opciones($ver=NULL,$where=false)
	{

		$campos=$this->listar($where);

		if ($campos) {

			foreach($campos as $index => $valor)
			{

				if ($valor["id_perfil_profesor"]==$ver)
				{
				$option.="<option value='".$valor["id_perfil_profesor"]."' selected>".$valor["dni_profesor"]."</option>";
				}
				else
				{
				$option.="<option value='".$valor["id_perfil_profesor"]."' >".$valor["dni_profesor"]."</option>";
				}
			}
		}
		else {$option = false;}

		return $option;

	}


/**
* Función que retorna las <checkbox> de los registros de la tabla
 * 
* @return string, html en etiquetas checkbox 
* @param integer $buscado para determinar si el valor es igual al pk de la tabla para colocarlo como checked
*/
	//public function ver_checkbox_tbl_perfil_profesor($buscado)
	public function ver_checkbox($buscado,$where=false)
	{

		$campos=$this->listar($where);

		if ($campos) {

			foreach($campos as $index => $valor)
			{
				if (in_array($valor["id_perfil_profesor"], $buscado))
				{
				$option.="<input type='checkbox' value='".$valor["id_perfil_profesor"]."' checked='checked' />".$valor["dni_profesor"]."<br />";
				}
				else
				{
				$option.="<input type='checkbox' value='".$valor["id_perfil_profesor"]."' />".$valor["dni_profesor"]."<br />";
				}
			}
		}
		else {$option = false;}

		return $option;

	}



/**
* Función para retornar el valor del campo de la tabla <br>
*
 * <b>Descripción:</b> lo primero que se tiene que tener en cuenta de esta función es que el objeto creado <br>
 * necesita la variable $this->where para ser utilizado en la función $this->listar(true), que obliga a utilizarlo <br>
 * 
* @return  string $campo, el valor del campo en su primera posición del array
* @param string $campo para determinar cual es el campo de la tabla
*/

	public function valor_campo($campo)
	{
		$campos=$this->listar(true);
		return $campos[0][$campo];
	}


} //fin de la clase tbl_perfil_profesor
?>