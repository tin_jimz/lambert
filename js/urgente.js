$(function(){
            $('#inicio_link').click(function(){ location='dashboard.php' })
            $('#menu_bar').css('background-color', '#6654e7')
            var dd = new DropDown( $('#dd') );
            $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true); 
            });

            $('#product1').on( "click",function(){
                //alert($('#product1').attr('alt'))
                $('#btn1').attr('disabled', 'disabled')
                $('#btn2').removeAttr('disabled')
                $('#grado').html('<option value="">Seleccione...</option>'+
                        '<option value="1">1</option>'+
                        '<option value="2">2</option>'+
                        '<option value="3">3</option>'+
                        '<option value="4">4</option>'+
                        '<option value="5">5</option>'+
                        '<option value="6">6</option>'+
                        '<option value="7">7</option>');
                $('#grado').change(function(){
                    listar_materias($('#grado').val());

                });
                $('#boton_urg').hide()
                $('#notice').html('<strong><a href="#" title="Ver Cursos">Realice una petición &nbsp;</a></strong><span>para obtener resultados.</span>')
                $('#grado').val('').focus();
            })
            $('#product2').on( "click",function(){
                //alert($('#product2').attr('alt'))
                $('#btn2').attr('disabled', 'disabled')
                $('#btn1').removeAttr('disabled')
                $('#grado').html('<option value="">Seleccione...</option>'+
                        '<option value="8">1</option>'+
                        '<option value="9">2</option>'+
                        '<option value="10">3</option>'+
                        '<option value="11">4</option>'+
                        '<option value="12">5</option>'+
                        '<option value="13">6</option>');
                $('#grado').change(function(){
                    listar_materias($('#grado').val());

                });
                $('#boton_urg').hide()
                $('#notice').html('<strong><a href="#" title="Ver Cursos">Realice una petición &nbsp;</a></strong><span>para obtener resultados.</span>')
                $('#grado').val('').focus();
            })

            

        });

        
        function DropDown(el) {
                this.dd = el;
                this.placeholder = this.dd.children('span');
                this.opts = this.dd.find('.dropdown a');
                this.val = '';
                this.index = -1;
                this.initEvents();
            }
            DropDown.prototype = {
                initEvents : function() {
                    var obj = this;

                    obj.dd.on('click', function(event){
                        $(this).toggleClass('active');
                        return false;
                    });

                    obj.opts.on('click',function(){
                        var opt = $(this);
                        obj.val = opt.text();
                        obj.index = opt.index();
                        obj.placeholder.text(obj.val);
                    });
                },
                getValue : function() {
                    return this.val;
                },
                getIndex : function() {
                    return this.index;
                }
            }

function listar_materias(grado){
	$.ajax({
		type: "POST", //The type of HTTP verb (post, get, etc)
		url: "control_panel/controlador/tbl_cursos.php", //The URL from the form element where the AJAX request will be sent
		data: {"accion": "listar_materias", "grado": grado}, //All the data from the form serialized
		dataType: "json", //The type of response to expect from the server
		success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
			if (data){
                contador=0;
                $('#notice').html('<div class="col-md-4"><select id="select_materia" class="form-control"><option value="">Elegí Matería...</option></select></div><div class="col-md-4" id="span_sel"></div><div class="col-md-4" id="div_desc"></div>');
                $.each(data,function(index_data, registros){
                    contador++;
                    $('#select_materia').append(new Option(registros.materia, registros.id_materia))
                });
                if (contador==0) {
                    $('#notice').html('<strong><a href="#" title="Ver Cursos">No hay materias&nbsp;</a></strong><span>registradas para este grado.</span>')
                }
            }

            $('#select_materia').change(function(){
                selec_prof($(this).val(), grado);
            })
		}
	});
}

function selec_prof(id_materia, grado){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_cursos.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "selec_prof", "grado": grado, "id_materia": id_materia}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data){
                contador=0;
                $('#boton_urg').show()
                $('#head6').show()
                $('#span_sel').html('<select id="selec_pro" class="form-control"><option value="">Elegí Profesor...</option></select>');
                $('#div_desc').html('<textarea id="descripcion" placeholder="Describa su urgencia" class="form-control"></textarea>');
                $.each(data,function(index_data, registros){
                    contador++;
                    $('#selec_pro').append(new Option(registros.nombres+' '+registros.apellidos, registros.id_curso))
                });
                if (contador==0) {
                    $('#span_sel').html('<strong><a href="#" title="Ver Cursos">No hay profesores</a></strong> <span>&nbsp;disponibles ahora.</span>')
                    $('#boton_urg').hide()
                }
            }
        }
    });
}

function completar(){
    var hora = $('#timepick').val()
    var duracion = $('#duracion').val()
    var id_curso = $('#selec_pro').val()
    var descripcion = $('#descripcion').val()

    if(hora!="" && id_curso!="" && descripcion!="" && duracion!=""){
        $.ajax({
            type: "POST", //The type of HTTP verb (post, get, etc)
            url: "control_panel/controlador/tbl_clases_urgentes.php", //The URL from the form element where the AJAX request will be sent
            data: {"accion": "completar", "hora": hora, "duracion": duracion, "id_curso": id_curso, "descripcion": descripcion}, //All the data from the form serialized
            dataType: "json", //The type of response to expect from the server
            success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
                if (data.estado=="insertado"){
                    $.confirm({
                            title: 'Exitoso!',
                            content: 'Su solicitud ha sido enviada, Por favor espere su respuesta!',
                            theme: 'supervan', // 'material', 'bootstrap'
                            buttons: {
                                somethingElse: {
                                    text: 'Aceptar',
                                    btnClass: 'btn-blue',
                                    action: function(){
                                        location ='dashboard.php';
                                    }
                                }
                            }
                        });
                }
            }
        });
    }else{
        $.confirm({
            title: 'Aviso!',
            content: 'Todos los campos deben estar llenos.',
            theme: 'light', // 'material', 'bootstrap'
            buttons: {
                somethingElse: {
                    text: 'Aceptar',
                    btnClass: 'btn-blue',
                    action: function(){
                        
                    }
                }
            }
        });
    }

}


///////////////////PROFE URGENCIA/////////////////////
function urgencia(){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_cursos.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "urgencia"}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data){
                contador = 0;
                $('#urgencia').html('');
                $.each(data,function(index_data, registros){
                contador++;
                if(registros.status==1){   
                        $('#urgencia').append('<li>'+
                                            '<time datetime="'+registros.fecha_hora+'" style="background-color: #007bff;">'+
                                                '<span class="day" style="margin-top: 10px;">'+registros.fecha_sola.substr(8, 2)+'</span>'+
                                                '<span class="month">'+registros.mes+'</span>'+
                                            '</time>'+
                                            '<div class="info">'+
                                                '<h2 class="title">'+registros.materia+'</h2>'+
                                                '<p class="desc"><span><img width="32" style="border-radius: 50px;" src="'+registros.imagen_perfil+'"/></span><span> '+registros.nombres+' '+registros.apellidos+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-clock-o fa-lg" ></i></span><span> De'+registros.hora_inicio+' a '+registros.hora_fin+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-ellipsis-v fa-lg" ></i></span><span> '+registros.descripcion+'</span></p>'+
                                                '<ul>'+
                                                    '<li style="width:50%;" id="acceso_aula"></li>'+
                                                '</ul>'+
                                            '</div>'+
                                        '</li>');
                        setTimeout(function(){
                            if(registros.flag==1){
                                $('#acceso_aula').html('<a href="aula_virtual_urgente.php?id_clase_urgente='+registros.id_clase_urgente+'" style="color: #546BF9;">Ir al Aula <i class="fa fa-arrow-right"></i></a>')
                            }
                        }, 200)
                        
                    }else if(registros.status==2){ 
                        $('#urgencia').append('<li>'+
                                            '<time datetime="'+registros.fecha_hora+'" style="background-color: orange;">'+
                                                '<span class="day" style="margin-top: 10px;">'+registros.fecha_sola.substr(8, 2)+'</span>'+
                                                '<span class="month">'+registros.mes+'</span>'+
                                            '</time>'+
                                            '<div class="info">'+
                                                '<h2 class="title">'+registros.materia+'</h2>'+
                                                '<p class="desc"><span><img width="32" style="border-radius: 50px;" src="'+registros.imagen_perfil+'"/></span><span> '+registros.nombres+' '+registros.apellidos+'</span></p>'+
                                               '<p class="desc"><span><i class="fa fa-clock-o fa-lg" ></i></span><span> De'+registros.hora_inicio+' a '+registros.hora_fin+'</span></p>'+
                                                '<p class="desc"><span><i class="fa fa-ellipsis-v fa-lg" ></i></span><span> '+registros.descripcion+'</span></p>'+
                                                
                                            '</div>'+
                                            '<div class="social">'+
                                                '<ul>'+
                                                    '<li class="facebook" style="width:33%;"><a href="javascript:void(0);" onclick="confirmar_urgente('+registros.id_clase_urgente+')" ><span class="fa fa-check fa-lg"></span></a></li>'+
                                                    '<li class="google-plus" style="width:34%;" ><a href="javascript:void(0);" onclick="rechazar_urgente('+registros.id_clase_urgente+')" ><span class="fa fa-ban fa-lg"></span></a></li>'+
                                                '</ul>'+
                                            '</div>'+
                                        '</li>');
                    }
                });

                if (contador==0) {
                    $('#urgencia').html('<div class="col-md-12"><hr /><i class="fa fa-info-circle"></i> No existen reservas de clases urgentes.</div>')
                }
            }
        }
    });
}

function confirmar_urgente(id_clase_urgente){
    $.confirm({
        title: '¿Seguro?',
        content: 'Realmente desea aceptar esta solicitud de Urgencia?',
        theme: 'light', // 'material', 'bootstrap'
        buttons: {
            confirm: function () {
                aceptar_urgencia(id_clase_urgente)
            },
            somethingElse: {
                text: 'Cancel',
                btnClass: 'btn-red',
                action: function(){
                    
                }
            }
        }
    });
}

function aceptar_urgencia(id_clase_urgente){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_clases_urgentes.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "aceptar_urgencia", "id_clase_urgente": id_clase_urgente}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data.estado=='actualizado'){
                    $.confirm({
                        title: 'Exitoso!',
                        content: 'La solicitud fue aceptada satisfactoriamente, Acepte para ser redirigido al Aula Virtual.',
                        theme: 'supervan', // 'material', 'bootstrap'
                        buttons: {
                            somethingElse: {
                                text: 'Aceptar',
                                btnClass: 'btn-blue',
                                action: function(){
                                    urgencia();
                                }
                            }
                        }
                    });
            }
        }
    });
}

function rechazar_urgente(id_clase_urgente){
    $.confirm({
        title: '¿Seguro?',
        content: 'Realmente desea cancelar esta solicitud de Urgencia?',
        theme: 'light', // 'material', 'bootstrap'
        buttons: {
            confirm: function () {
                rechazar_urgencia(id_clase_urgente)
            },
            somethingElse: {
                text: 'Cancel',
                btnClass: 'btn-red',
                action: function(){
                    
                }
            }
        }
    });
}

function rechazar_urgencia(id_clase_urgente){
    $.ajax({
        type: "POST", //The type of HTTP verb (post, get, etc)
        url: "control_panel/controlador/tbl_clases_urgentes.php", //The URL from the form element where the AJAX request will be sent
        data: {"accion": "rechazar_urgencia", "id_clase_urgente": id_clase_urgente}, //All the data from the form serialized
        dataType: "json", //The type of response to expect from the server
        success: function ( data, statusCode, xhr ) { //Triggered after a successful response from server
            if (data.estado=='actualizado'){
                    $.confirm({
                        title: 'Exitoso!',
                        content: 'La solicitud fue aceptada satisfactoriamente, Acepte para ser redirigido al Aula Virtual.',
                        theme: 'supervan', // 'material', 'bootstrap'
                        buttons: {
                            somethingElse: {
                                text: 'Aceptar',
                                btnClass: 'btn-blue',
                                action: function(){
                                    urgencia();
                                }
                            }
                        }
                    });
            }
        }
    });
}