<?php session_start();
/*
Código necesario para la ejecución
*/
include("../../inc/config.sistema.php");
require_once("../../modelo/config.modelo.php"); # configuracion del modelo		
require_once("../../modelo/class_tbl_estatus_usuarios.php"); # clase del modelo
$Obj_tbl_estatus_usuarios = new tbl_estatus_usuarios;	
require_once("../../modelo/class_tbl_perfiles.php"); # clase del modelo
$Obj_tbl_perfiles = new tbl_perfiles;	
require_once("../../modelo/class_tbl_regiones.php"); # clase del modelo
$Obj_tbl_regiones = new tbl_regiones;
$_SESSION["where"]="";	
			
?>
<?php include(RUTA_SISTEMA."/vista/layouts/{$defaul_layouts}/header_autenticacion.php"); ?>
<script type="text/javascript" src="../../js/validaciones/autenticar.js"></script>


<div class="content-wrapper full-page-wrapper d-flex align-items-center auth-pages">
  <div class="card col-lg-4 mx-auto">
    <div class="card-body px-5 py-5">
      <h3 class="card-title text-left mb-3">Login</h3>
      <form accept-charset="UTF-8" role="form" id="frm_formulario" name="frm_formulario">
      	<input name="accion" id="accion" type="hidden" value="<?php echo $_REQUEST["accion"]="autenticar"?>" />
		<input name="quien" id="quien" type="hidden" value="<?php echo $_REQUEST["quien"]?>" />
        <div class="form-group">
          <input type="text" class="form-control p_input" name="usuario" id="usuario" placeholder="Username">
        </div>
        <div class="form-group">
          <input type="password" class="form-control p_input" name="clave" id="clave" placeholder="Password">
        </div>
        <div class="form-group d-flex align-items-center justify-content-between">
          <div class="form-check"><label><input type="checkbox" class="form-check-input">Remember me</label></div>
          <a href="#" class="forgot-pass">Forgot password</a>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-primary btn-block enter-btn"><?php echo strtoupper($_REQUEST["accion"]); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
	
		
		<!--fin viata de plantilla -->
<?php include(RUTA_SISTEMA."/vista/layouts/{$defaul_layouts}/footer.php"); ?>