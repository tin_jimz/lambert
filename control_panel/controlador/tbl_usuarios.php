<?php
	session_start();
	require_once("../inc/config.sistema.php"); # configuracion del sistema
	require_once("../modelo/config.modelo.php"); # configuracion del modelo
	require_once("../modelo/class_tbl_perfil_profesor.php"); # clase del modelo
	$Obj_tbl_perfil_profesor = new tbl_perfil_profesor;
	require_once("../modelo/class_tbl_perfil_alumno.php"); # clase del modelo
	$Obj_tbl_perfil_alumno = new tbl_perfil_alumno;
	require_once("../modelo/class_tbl_perfil_pariente.php"); # clase del modelo
	$Obj_tbl_perfil_pariente = new tbl_perfil_pariente;
	require_once("../modelo/class_tbl_usuarios.php"); # clase del modelo
	$Obj_tbl_usuarios = new tbl_usuarios($_REQUEST["cedula"],$_REQUEST["usuario"],$_REQUEST["clave"],$_REQUEST["nombres"],$_REQUEST["apellidos"],$_REQUEST["correo_electronico"],$_REQUEST["id_estatu_usuario"],$_REQUEST["id_perfil"],$_REQUEST["id_region"],$_REQUEST["online"]);
		
	switch ($_REQUEST["accion"])
	{
		case "buscar":
			$_SESSION["where"]="";	
			if ($Obj_tbl_usuarios->buscar())
			{
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="encontrado"; 
			}
			else
			{ 
			
				$retorna["mensaje"]="NO se encuentra registro";
				$retorna["datos"]="";
				$retorna["estado"]="no_encontrado";
			}
			echo json_encode($retorna);
		break;
		
		case "insertar":
		$Obj_tbl_usuarios->where=" usuario='".$_REQUEST['usuario']."' ";
		$user = $Obj_tbl_usuarios->listar(true);
		if (count($user)<=0) {
			$Obj_tbl_usuarios->where=" correo_electronico='".$_REQUEST['correo_electronico']."' ";
			$mail = $Obj_tbl_usuarios->listar(true);
			if (count($mail)<=0) {
				$insertado=$Obj_tbl_usuarios->insertar();
				if (is_numeric($insertado))
				{  
				$retorna["mensaje"]="se agrego el registro a la Base de Datos"; 
				$retorna["datos"]="";
				$retorna["estado"]="insertado";
				}
				else
				{ 
				$retorna["mensaje"]=$insertado;//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
				}
			}else{
				$retorna["estado"]="ce";
			}	
		}else{
			$retorna["estado"]="ue";
		}
			
		echo json_encode($retorna);

		break;
		
		case "actualizar":
			$actualizado=$Obj_tbl_usuarios->actualizar();
			if (is_numeric($actualizado))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$actualizado;//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "eliminar":
		$Obj_tbl_usuarios->where=" USUARIO = '".$_REQUEST["usuario"]."'";
		$eliminado=$Obj_tbl_usuarios->eliminar();
			if (is_numeric($eliminado))
			{  
				$retorna["mensaje"]="se elimino..."; 
				$retorna["datos"]="";
				$retorna["estado"]="eliminado";
			}
			else
			{ 
				$retorna["mensaje"]="No se elimino...".$eliminado;
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
		
		case "combo":
			
			$_SESSION["where"]="";	
			$combo=$Obj_tbl_usuarios->listar();
			$options=array();
			
			$options[0]["value"]="";
			$options[0]["text"]="Seleccione...";
			
				
			foreach($combo as $index => $valor)
			{
				$options[$index+1]["value"]=$valor["usuario"];
				$options[$index+1]["text"] =$valor["usuario"];
			}
			
			$retorna=$options;
			echo json_encode($retorna);
			 
		break;
		
		case "autenticar":
		$Obj_tbl_usuarios->where=" usuario = '".$_REQUEST["usuario"]."' and clave='".$Obj_tbl_usuarios->clave."'";
		$autenticado=$Obj_tbl_usuarios->listar(true);
		#echo "<pre>"; print_r($autenticado); exit;
			if (count($autenticado)==1)
			{  
				$_SESSION["session_usuario"]=$autenticado[0];
				$retorna["mensaje"]="Autenmticado el Usuario  [".$_REQUEST["usuario"]."]"; 
				$retorna["reenvio"]="../principal/index.php";
				$retorna["estado"]="autenticado";

				if ($autenticado[0]['id_perfil']==2) {
					$Obj_tbl_perfil_profesor->where=" tbl_perfil_profesor.usuario='".$_REQUEST["usuario"]."' ";
					$result = $Obj_tbl_perfil_profesor->listar(true);
					if (count($result)>0) {
						$_SESSION["session_usuario"] = array_merge($autenticado[0], $result[0]);
					}else $_SESSION["session_usuario"] = $autenticado[0];
				}else if($autenticado[0]['id_perfil']==3){
					$Obj_tbl_perfil_alumno->where=" tbl_perfil_alumno.usuario='".$_REQUEST["usuario"]."' ";
					$result = $Obj_tbl_perfil_alumno->listar(true);
					if (count($result)>0) {
						$_SESSION["session_usuario"] = array_merge($autenticado[0], $result[0]);
					}else $_SESSION["session_usuario"] = $autenticado[0];
				}else if($autenticado[0]['id_perfil']==4){
					$Obj_tbl_perfil_pariente->where=" tbl_perfil_pariente.usuario='".$_REQUEST["usuario"]."' ";
					$result = $Obj_tbl_perfil_pariente->listar(true);
					if (count($result)>0) {
						$_SESSION["session_usuario"] = array_merge($autenticado[0], $result[0]);
					}else $_SESSION["session_usuario"] = $autenticado[0];
				}


			}
			else
			{ 
				$retorna["mensaje"]="No se encuentra el usuario [".$_REQUEST["usuario"]."]";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;

		case "salir":
			session_destroy();

			$retorna["estado"]="hecho";
			
			echo json_encode($retorna); 
		break;

		case 'editar_perfil':
			$Obj_tbl_usuarios->where=" usuario='".$_SESSION["session_usuario"]['usuario']."' ";
			$actualizado=$Obj_tbl_usuarios->actualizar_campo("apellidos='".$_REQUEST["apellidos"]."'");
			if (is_numeric($actualizado))
			{  
				$retorna["mensaje"]="se actualizo..."; 
				$retorna["datos"]="";
				$retorna["estado"]="actualizado";
				$_SESSION["session_usuario"]['apellidos']=$_REQUEST["apellidos"];
			}
			else
			{ 
				$retorna["mensaje"]="NO se actualizo ".$actualizado;//"NO se agrego el registro a la Base de Datos";
				$retorna["datos"]="";
				$retorna["estado"]="false";
			}
			echo json_encode($retorna); 
		break;
	}	
	
	?>
	